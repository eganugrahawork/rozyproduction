var form = document.getElementById('updateSewing');
$("#tanggal").flatpickr();
getDataPartner()
countTotal()
var validator;
var declareValidator;
var cuttingDetailIdValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
        numeric: {
            message: "Data harus diisi !",
        },
    },
}
var priceValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}


validation()
itemValidation()
declareSomeLibraryOnRows();

function getDataPartner() {
    var idPartner = $('#partner_id').val();
    $.ajax({
        url: '/admin/procurement/pengadaan/get-partner/' + idPartner,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $("#alamat").val(response.partner.alamat)
            $("#no_telp").val(response.partner.no_telp)

            validation()
            itemValidation()

        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}


function declareSomeLibraryOnRows() {
    $('.select-2-id').each(function (index) {
        $(this).attr('data-select2-id', 'select2[' + index + ']')
        $(this).select2()
    })

    $('.detail-form').each(function (index) {
        var elementHarga = $(this).parent().parent().find('#price')
        var elementTotal = $(this).parent().parent().find('#jumlah')
        Inputmask({
            alias: "currency",
            prefix: ''
        }).mask(elementHarga);
        Inputmask({
            alias: "currency",
            prefix: ''
        }).mask(elementTotal);
    })

    var grandTotal = $('#total')

    Inputmask({
        alias: "currency",
        prefix: ''
    }).mask(grandTotal);
}


function validation() {
    validator = [];
    declareValidator = FormValidation.formValidation(form, {
        fields: {

            kode: {
                validators: {
                    notEmpty: {
                        message: "Kode harus diisi !",
                    },
                },
            },
            partner_id: {
                validators: {
                    notEmpty: {
                        message: "Partner harus dipilih !",
                    },
                    numeric: {
                        message: "Partner harus dipilih !",
                    },
                },
            },
            pengantar_id: {
                validators: {
                    notEmpty: {
                        message: "Pengantar harus dipilih !",
                    },
                    numeric: {
                        message: "Pengantar harus dipilih !",
                    },
                },
            },
            tanggal: {
                validators: {
                    notEmpty: {
                        message: "Tanggal harus diisi !",
                    },
                },
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
            }),
        },
    })

    validator.push(declareValidator);


}

function itemValidation() {
    $('.detail-form').each(function (index) {
        $(this).parent().parent().find('#sewing_detail_id').attr('name', 'sewing_detail_id[' + index + ']');
        $(this).parent().parent().find('#cutting_detail_id').attr('name', 'cutting_detail_id[' + index + ']');
        $(this).parent().parent().find('#price').attr('name', 'price[' + index + ']');
        $(this).parent().parent().find('#qty').attr('name', 'qty[' + index + ']');
        $(this).parent().parent().find('#jumlah').attr('name', 'jumlah[' + index + ']');

        validator[0]
            .addField('cutting_detail_id[' + index + ']', cuttingDetailIdValidator)
            .addField('price[' + index + ']', priceValidator)
    })
}



function update() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Simpan data ?',
                    text: "Pastikan Semua Kolom Diisi dengan benar",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, simpan!',
                    cancelButtonText: 'Tidak, batalkan!',
                    reverseButtons: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: '/admin/produksi/jahit/'+ $('#id').val(),
                            type: 'PUT',
                            data: $('#updateSewing').serialize(),
                            dataType: 'json',
                            success: function (response) {
                                if (response.success) {
                                    Swal.fire({
                                        text: "Berhasil mengubah Jahit!",
                                        icon: "success",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Lanjutkan !",
                                        customClass: {
                                            confirmButton: "btn btn-primary",
                                        },
                                    }).then(function (t) {
                                        t.isConfirmed &&
                                            window.location.replace("/admin/produksi/jahit");
                                        cuttingTable.ajax.reload();
                                    });
                                } else {
                                    Swal.fire(
                                        response.errors,
                                        'Informasi lebih lanjut hubungi team.',
                                        'error'
                                    )
                                }
                            },
                            error: function (xhr, status, error) {
                                if (xhr.status === 422) {
                                    var errors = xhr.responseJSON.errors;
                                    Swal.fire(
                                        'Peringatan',
                                        errors[0],
                                        'warning'
                                    )
                                } else {
                                    Swal.fire(
                                        'Error',
                                        'Terjadi kesalahan: ' + status + ' ' + error,
                                        'error'
                                    )
                                }
                            }
                        });
                    } else if (
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Dibatalkan',
                            '',
                            'success'
                        )
                    }
                })

            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}


function getDetail() {
    var templates = `
                <tr>
                <td class="fv-row">
                    <select class="form-select form-select-solid fs-7 select-2-id detail-form"
                        data-control="select2" data-placeholder="Pilih Cutting Detail" id="cutting_detail_id"
                        onchange="getCuttingDetail(this)">
                        <option></option>
                    </select>
                </td>
                <td class="fv-row">
                    <input class="form-control form-control-white text-center fw-bolder fs-7" type="text"
                        id="qty" autocomplete="off" value="0" readonly />
                </td>
                <td class="fv-row">
                    <input class="form-control form-control-solid text-end fs-7" type="text"
                        id="price" autocomplete="off" value="0" onkeyup="countTotal()" />
                </td>
                <td class="fv-row">
                    <input class="form-control form-control-white text-end fs-7" type="text"
                        id="jumlah" autocomplete="off" value="0" readonly />
                </td>
                <td class="fv-row text-center"> <button class="btn btn-sm btn-danger"
                        data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus detail"
                        type="button" onclick="removeRowVariant(this)">-</button></td>
                </tr>
                       `;

    $.ajax({
        url: '/admin/produksi/jahit/get-detail-form',
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $('#tableItems').append(templates);
            var option = `<option></option>`
            var inputBahanJadi = '';
            $.each(response.cuttingDetail, function (index, data) {
                option += `<option value="` + data.id + `">`+ data.partner + ` | `+ data.bahan_jadi + ` : ` + data.nama + `-` + data.nama_detail + `</option>`;

                inputBahanJadi += `<input type='hidden' id='qty_bahan_jadi` + data.id + `' value='` + data.bahan_jadi + `' />`
            })

            $('.detail-form').each(function () {
                if ($.isNumeric($(this).val()) === false) {
                    $(this).parent().find('#cutting_detail_id').html(option)
                    $(this).parent().find('#cutting_detail_id').parent().append(inputBahanJadi)
                }
            })

            validation()
            itemValidation()
            declareSomeLibraryOnRows();
        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}

function getCuttingDetail(e) {
    var id = $(e).val()
    var qty = $(e).parent().find('#qty_bahan_jadi' + id).val()
    console.log(qty);

    $(e).parent().parent().find('#qty').val(qty);
}

function addRowVariant() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                var templates = `
                <tr>
                <td class="fv-row">
                    <select class="form-select form-select-solid fs-7 select-2-id detail-form"
                        data-control="select2" data-placeholder="Pilih Cutting Detail" id="cutting_detail_id"
                        onchange="getCuttingDetail(this)">
                        <option></option>
                    </select>
                </td>
                <td class="fv-row">
                    <input class="form-control form-control-white text-center fw-bolder fs-7" type="text"
                        id="qty" autocomplete="off" value="0" readonly />
                </td>
                <td class="fv-row">
                    <input class="form-control form-control-solid text-end fs-7" type="text"
                        id="price" autocomplete="off" value="0" onkeyup="countTotal()" />
                </td>
                <td class="fv-row">
                    <input class="form-control form-control-white text-end fs-7" type="text"
                        id="jumlah" autocomplete="off" value="0" readonly />
                </td>
                <td class="fv-row text-center"> <button class="btn btn-sm btn-danger"
                        data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus detail"
                        type="button" onclick="removeRowVariant(this)">-</button></td>
                </tr>
                       `;

                $.ajax({
                    url: '/admin/produksi/jahit/get-detail-form',
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        $('#tableItems').append(templates);
                        var option = `<option></option>`
                        var inputBahanJadi = '';
                        $.each(response.cuttingDetail, function (index, data) {
                            option += `<option value="` + data.id + `">`+ data.partner + ` | `+ data.bahan_jadi + ` : ` + data.nama + `-` + data.nama_detail + `</option>`;

                            inputBahanJadi += `<input type='hidden' id='qty_bahan_jadi` + data.id + `' value='` + data.bahan_jadi + `' />`
                        })

                        $('.detail-form').each(function () {
                            if ($.isNumeric($(this).val()) === false) {
                                $(this).parent().find('#cutting_detail_id').html(option)
                                $(this).parent().find('#cutting_detail_id').parent().append(inputBahanJadi)
                            }
                        })

                        validation()
                        itemValidation()
                        declareSomeLibraryOnRows();
                    },
                    error: function (xhr, status, error) {
                        if (xhr.status === 422) {
                            var errors = xhr.responseJSON.errors;
                            Swal.fire(
                                'Peringatan',
                                errors[0],
                                'warning'
                            )
                        } else {
                            Swal.fire(
                                'Error',
                                'Terjadi kesalahan: ' + status + ' ' + error,
                                'error'
                            )
                        }
                    }
                });
            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}

function removeRowVariant(e) {
    $(e).parent().parent().remove();
    validation()
    itemValidation()
    declareSomeLibraryOnRows();
    countTotal()
}

function countTotal() {
    var x;
    var grandTotal = parseFloat(0);
    $('.detail-form').each(function (index) {
       x = $(this).parent().parent().find('#qty').val()
        var jumlahBelumClean = $(this).parent().parent().find('#price').val();
        if (jumlahBelumClean) {
            var jumlahTanpaKarakter = jumlahBelumClean.replace(/,/g, '');
            $(this).parent().parent().find('#jumlah').val(parseFloat(x) * parseFloat(jumlahTanpaKarakter));
            grandTotal += parseFloat(x) * parseFloat(jumlahTanpaKarakter);
        }
    })

    $('#total').val(parseFloat(grandTotal))
}
