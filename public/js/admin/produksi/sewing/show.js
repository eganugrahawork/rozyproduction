var form = document.getElementById('processSewing');
$("#tanggal_selesai").flatpickr();


function proccess(e) {

   var value = $(e).parent().parent().parent().find('#input_selesai').val()

   if(!value){
        $(e).parent().parent().find('#input_selesai').focus()
        Swal.fire(
            'Peringatan',
            'Data harus diisi',
            'warning'
        )
    }else if(!tanggal_selesai) {
        Swal.fire(
            'Peringatan',
            'Tanggal selesai harus diisi',
            'warning'
        )


    }else{
    var dataValue = {
        sewing_detail_id : $(e).parent().parent().parent().find('#sewing_detail_id').val(),
        tanggal_selesai : $(e).parent().parent().parent().find('#tanggal_selesai').val(),
        jumlah_cutting : $(e).parent().parent().parent().find('#jumlah_cutting').val(),
        harga_jahit : $(e).parent().parent().parent().find('#harga_jahit').val(),
        qty_jadi : value,
        pembawa_id : $(e).parent().parent().parent().find('#pembawa_id').val(),
        tanggal_diserahkan : $(e).parent().parent().parent().find('#tanggal_diserahkan').val()
    }

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
        title: 'Data sudah benar ?',
        text: "Pastikan jumlah sesuai. ini tidak bisa diedit lagi !",
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'Ya, sudah benar!',
        cancelButtonText: 'Tidak, batalkan!',
        reverseButtons: false
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/admin/produksi/jahit/proccess',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  // CSRF Token dari meta tag
                },

                data: dataValue,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        Swal.fire({
                            text: "Berhasil menyelesaikan Jahit!",
                            icon: "success",
                            buttonsStyling: !1,
                            confirmButtonText: "Lanjutkan !",
                            customClass: {
                                confirmButton: "btn btn-primary",
                            },
                        }).then(function (t) {
                            t.isConfirmed &&
                                window.location.replace("/admin/produksi/jahit");
                            cuttingTable.ajax.reload();
                        });
                    } else {
                        Swal.fire(
                            response.errors,
                            'Informasi lebih lanjut hubungi team.',
                            'error'
                        )
                    }
                },
                error: function (xhr, status, error) {
                    if (xhr.status === 422) {
                        var errors = xhr.responseJSON.errors;
                        Swal.fire(
                            'Peringatan',
                            errors[0],
                            'warning'
                        )
                    } else {
                        Swal.fire(
                            'Error',
                            'Terjadi kesalahan: ' + status + ' ' + error,
                            'error'
                        )
                    }
                }
            });
        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
                'Dibatalkan',
                '',
                'success'
            )
        }
    })
   }
}
