var form = document.getElementById('storeCutting');
$("#tanggal").flatpickr();

var validator;
var declareValidator;
var pengadaanDetailIdValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
        numeric: {
            message: "Data harus diisi !",
        },
    },
}
var produkIdValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
        numeric: {
            message: "Data harus diisi !",
        },
    },
}
var varianIdValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}
var priceValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}


validation()

function getDataPartner() {
    var idPartner = $('#partner_id').val();
    $.ajax({
        url: '/admin/procurement/pengadaan/get-partner/' + idPartner,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $("#alamat").val(response.partner.alamat)
            $("#no_telp").val(response.partner.no_telp)

            validation()
            itemValidation()

        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}

function getPengadaan() {
    var pengadaanId = $('#pengadaan_id').val()

    var templates = `  <table class="table align-middle  gy-5">
    <thead>
        <tr class="fs-6 fw-bolder text-gray-600 text-center">
            <th class="min-w-200px">Pengadaan Detail</th>
            <th class="w-200px">Produk</th>
            <th class="mw-200px">Varian</th>
            <th class="min-w-100px">Harga</th>
            <th class="min-w-100px">Est. Jadi</th>
            <th class="min-w-100px">Est. Sisa</th>
            <th class="min-w-50px">Aksi</th>
        </tr>
    </thead>
    <tbody id="tableItems">
        <tr>
            <td class="fv-row">
            <input type="hidden" id="qty_yard" type="number"/>
                <select class="form-select form-select-solid fs-7 select-2-id detail-form"
                    data-control="select2" data-placeholder="Pilih Pengadaan Detail" id="pengadaan_detail_id"
                    onchange="getProduk(this)">
                    <option></option>
                </select>
            </td>
            <td class="fv-row">
                <select class="form-select form-select-solid fs-7 select-2-id detail-produk-form"
                    data-control="select2" data-placeholder="Pilih Produk Detail" id="produk_id"
                    onchange="getVarianDetail(this)">
                    <option></option>
                </select>
            </td>
            <td class="fv-row">
                <select class="form-select form-select-solid fs-7 select-2-id detail-varian-form"
                    data-control="select2" data-placeholder="Pilih Pengadaan Detail" id="varian_id">
                    <option></option>
                </select>
            </td>
            <td class="fv-row">
                <input class="form-control form-control-solid text-end fs-7" type="text"
                    id="price" autocomplete="off" value="0" />
            </td>
            <td class="fv-row">
                <input class="form-control form-control-white text-end fs-7" type="text"
                    id="est_jadi" autocomplete="off" value="0" readonly />
            </td>
            <td class="fv-row">
                <input class="form-control form-control-white text-end fs-7" type="text"
                    id="est_sisa" autocomplete="off" value="0" readonly />
            </td>
            <td class="fv-row text-center"> <button class="btn btn-sm btn-danger"
                    data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus detail"
                    type="button" onclick="removeRowVariant(this)">-</button></td>

        </tr>

    </tbody>
    <tfoot>
        <tr>
            <th class="text-end" colspan="7"> <button class="btn btn-sm btn-success"
                    type="button" onclick="addRowVariant()">+</button></th>
        </tr>
        <tr class="fw-bolder fs-7">
            <th class="text-end" colspan="5"> Est. Jumlah Jadi</th>
            <th colspan="2"><input type="text"
                    class="form-control text-end form-control-white fs-7 text-dark" readonly
                    id="jumlah_est_jadi" /></th>
        </tr>
    </tfoot>
</table>`;

    $.ajax({
        url: '/admin/produksi/cutting/get-detail-form/' + pengadaanId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $('#detailHere').html(templates);
            var option = `<option></option>`
            $.each(response.pengadaanDetail, function (index, data) {
                option += `<option value="` + data.id + `">` + data.qty + data.uom + ` : ` + data.nama + `-` + data.warna + `</option>`;
            })


            $('.detail-form').each(function () {
                if ($.isNumeric($(this).val()) === false) {
                    $(this).parent().find('#pengadaan_detail_id').html(option)
                }
            })


            validation()
            itemValidation()
            declareSomeLibraryOnRows();


        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}

function getProduk(e) {
    var pengadaanDetailId = $(e).val();

    $.ajax({
        url: '/admin/produksi/cutting/get-produk/' + pengadaanDetailId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            var option = `<option></option>`
            $.each(response.isProduk, function (index, data) {
                option += `<option value="` + data.id + `">` + data.nama + ` - `+ parseFloat(data.number_cutting_require).toFixed(2) + ` yr</option>`;
            })

            $(e).parent().parent().find('#qty_yard').val(response.isProduk[0].qty)
            $(e).parent().parent().find('#produk_id').html(option)

            validation()
            itemValidation()
            declareSomeLibraryOnRows();

        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}

function getVarianDetail(e) {
    var produkId = $(e).val();

    $.ajax({
        url: '/admin/produksi/cutting/get-varian/' + produkId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            var option = `<option></option>`
            $.each(response.isVarian, function (index, data) {
                option += `<option value="` + data.id + `">` + data.nama + `</option>`;
            })

            var est_jadi = parseFloat($(e).parent().parent().find('#qty_yard').val()) / parseFloat(response.isVarian[0].number_cutting_require)

            est_jadi = Math.floor(est_jadi)

            var est_sisa = parseFloat($(e).parent().parent().find('#qty_yard').val()) - (parseFloat(response.isVarian[0].number_cutting_require) * est_jadi) 
            $(e).parent().parent().find('#est_jadi').val(est_jadi)
            $(e).parent().parent().find('#est_sisa').val(est_sisa.toFixed(2))
            $(e).parent().parent().find('#varian_id').html(option)

            validation()
            itemValidation()
            declareSomeLibraryOnRows();
            countTotal()

        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}

function declareSomeLibraryOnRows() {
    $('.select-2-id').each(function (index) {
        $(this).attr('data-select2-id', 'select2[' + index + ']')
        $(this).select2()
    })

    $('.detail-form').each(function (index) {
        var elementHarga = $(this).parent().parent().find('#price')
        var elementTotal = $(this).parent().parent().find('#total')
        Inputmask({
            alias: "currency",
            prefix: ''
        }).mask(elementHarga);
        Inputmask({
            alias: "currency",
            prefix: ''
        }).mask(elementTotal);
    })

    var grandTotal = $('#grandTotal')

    Inputmask({
        alias: "currency",
        prefix: ''
    }).mask(grandTotal);
}


function countTotal() {
    var grandTotal = 0;


    $('.detail-form').each(function (index) {
        var qty = $(this).parent().parent().find('#qty').val();
        var total = $(this).parent().parent().find('#total');
        var hargaInputBelumClean = $(this).parent().parent().find('#price').val();

        // Periksa apakah hargaInputBelumClean telah didefinisikan
        if (hargaInputBelumClean) {
            var hargaTanpaKarakter = hargaInputBelumClean.replace(/,/g, '');
            total.val(parseFloat(qty) * parseFloat(hargaTanpaKarakter));
            grandTotal += parseFloat(qty) * parseFloat(hargaTanpaKarakter);
        }
    })

    $('#grandTotal').val(grandTotal)

}


function validation() {
    validator = [];
    declareValidator = FormValidation.formValidation(form, {
        fields: {

            kode: {
                validators: {
                    notEmpty: {
                        message: "Kode harus diisi !",
                    },
                },
            },
            kategori_id: {
                validators: {
                    notEmpty: {
                        message: "Kategori harus diisi !",
                    },
                    numeric: {
                        message: "Kategori harus diisi !",
                    },
                },
            },
            partner_id: {
                validators: {
                    notEmpty: {
                        message: "Partner harus diisi !",
                    },
                    numeric: {
                        message: "Partner harus diisi !",
                    },
                },
            },
            pengantar_id: {
                validators: {
                    notEmpty: {
                        message: "Pengantar harus diisi !",
                    },
                    numeric: {
                        message: "Pengantar harus diisi !",
                    },
                },
            },
            tanggal: {
                validators: {
                    notEmpty: {
                        message: "Tanggal harus diisi !",
                    },
                },
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
            }),
        },
    })

    validator.push(declareValidator);


}

function itemValidation() {
    $('.detail-form').each(function (index) {
        $(this).parent().parent().find('#pengadaan_detail_id').attr('name', 'pengadaan_detail_id[' + index + ']');
        $(this).parent().parent().find('#qty_yard').attr('name', 'qty_yard[' + index + ']');
        $(this).parent().parent().find('#produk_id').attr('name', 'produk_id[' + index + ']');
        $(this).parent().parent().find('#varian_id').attr('name', 'varian_id[' + index + ']');
        $(this).parent().parent().find('#price').attr('name', 'price[' + index + ']');
        $(this).parent().parent().find('#est_jadi').attr('name', 'est_jadi[' + index + ']');
        $(this).parent().parent().find('#est_sisa').attr('name', 'est_sisa[' + index + ']');

        validator[0]
            .addField('pengadaan_detail_id[' + index + ']', pengadaanDetailIdValidator)
            .addField('produk_id[' + index + ']', produkIdValidator)
            .addField('varian_id[' + index + ']', varianIdValidator)
            .addField('price[' + index + ']', priceValidator)
    })
}



function store() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Simpan data ?',
                    text: "Pastikan Semua Kolom Diisi dengan benar",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, simpan!',
                    cancelButtonText: 'Tidak, batalkan!',
                    reverseButtons: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: '/admin/produksi/cutting',
                            type: 'POST',
                            data: $('#storeCutting').serialize(),
                            dataType: 'json',
                            success: function (response) {
                                if (response.success) {
                                    Swal.fire({
                                        text: "Berhasil menambahkan cutting!",
                                        icon: "success",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Lanjutkan !",
                                        customClass: {
                                            confirmButton: "btn btn-primary",
                                        },
                                    }).then(function (t) {
                                        t.isConfirmed &&
                                            window.location.replace("/admin/produksi/cutting");
                                        cuttingTable.ajax.reload();
                                    });
                                } else {
                                    Swal.fire(
                                        response.errors,
                                        'Informasi lebih lanjut hubungi team.',
                                        'error'
                                    )
                                }
                            },
                            error: function (xhr, status, error) {
                                if (xhr.status === 422) {
                                    var errors = xhr.responseJSON.errors;
                                    Swal.fire(
                                        'Peringatan',
                                        errors[0],
                                        'warning'
                                    )
                                } else {
                                    Swal.fire(
                                        'Error',
                                        'Terjadi kesalahan: ' + status + ' ' + error,
                                        'error'
                                    )
                                }
                            }
                        });
                    } else if (
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Dibatalkan',
                            '',
                            'success'
                        )
                    }
                })

            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}

function addRowVariant() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                var pengadaanId = $('#pengadaan_id').val()

                var templates = `
                <tr>
                <td class="fv-row">
                <input type="hidden" id="qty_yard" type="number"/>
                    <select class="form-select form-select-solid fs-7 select-2-id detail-form"
                        data-control="select2" data-placeholder="Pilih Pengadaan Detail" id="pengadaan_detail_id"
                        onchange="getProduk(this)">
                        <option></option>
                    </select>
                </td>
                <td class="fv-row">
                    <select class="form-select form-select-solid fs-7 select-2-id detail-produk-form"
                        data-control="select2" data-placeholder="Pilih Produk Detail" id="produk_id"
                        onchange="getVarianDetail(this)">
                        <option></option>
                    </select>
                </td>
                <td class="fv-row">
                    <select class="form-select form-select-solid fs-7 select-2-id detail-varian-form"
                        data-control="select2" data-placeholder="Pilih Pengadaan Detail" id="varian_id">
                        <option></option>
                    </select>
                </td>
                <td class="fv-row">
                    <input class="form-control form-control-solid text-end fs-7" type="text"
                        id="price" autocomplete="off" value="0" />
                </td>
                <td class="fv-row">
                    <input class="form-control form-control-white text-end fs-7" type="text"
                        id="est_jadi" autocomplete="off" value="0" readonly />
                </td>
                <td class="fv-row">
                    <input class="form-control form-control-white text-end fs-7" type="text"
                        id="est_sisa" autocomplete="off" value="0" readonly />
                </td>
                <td class="fv-row text-center"> <button class="btn btn-sm btn-danger"
                        data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus detail"
                        type="button" onclick="removeRowVariant(this)">-</button></td>
    
            </tr>
                       `;

                $.ajax({
                    url: '/admin/produksi/cutting/get-detail-form/' + pengadaanId,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        $('#tableItems').append(templates);
                        var option = `<option></option>`
                        $.each(response.pengadaanDetail, function (index, data) {
                            option += `<option value="` + data.id + `">` + data.qty + data.uom + ` : ` + data.nama + `-` + data.warna + `</option>`;
                        })


                        $('.detail-form').each(function () {
                            if ($.isNumeric($(this).val()) === false) {
                                $(this).parent().find('#pengadaan_detail_id').html(option)
                            }
                        })

                        validation()
                        itemValidation()
                        declareSomeLibraryOnRows();
                    },
                    error: function (xhr, status, error) {
                        if (xhr.status === 422) {
                            var errors = xhr.responseJSON.errors;
                            Swal.fire(
                                'Peringatan',
                                errors[0],
                                'warning'
                            )
                        } else {
                            Swal.fire(
                                'Error',
                                'Terjadi kesalahan: ' + status + ' ' + error,
                                'error'
                            )
                        }
                    }
                });
            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}

function removeRowVariant(e) {
    $(e).parent().parent().remove();
    validation()
    itemValidation()
    declareSomeLibraryOnRows();
    countTotal()
}

function countTotal(){
    var x = parseFloat(0);
    var y; 
    $('.detail-form').each(function(index){
        var y = $(this).parent().parent().find('#est_jadi').val();
    if (!isNaN(parseFloat(y))) { 
        x += parseFloat(y);
    }
    })

    $('#jumlah_est_jadi').val(parseFloat(x))


}
