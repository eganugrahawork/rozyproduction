var form = document.getElementById('formModalCutting');
// $("#tanggal").flatpickr();
getDataPartner()
countTotal()

var validator;



function getDataPartner() {
    var idPartner = $('#partner_id').val();
    $.ajax({
        url: '/admin/procurement/pengadaan/get-partner/' + idPartner,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $("#alamat").val(response.partner.alamat)
            $("#no_telp").val(response.partner.no_telp)


        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}


function closeCuttingModal(e, id){
    $('#closeCuttingModal').modal('show')
    var pengadaanDetailAja = $(e).parent().parent().parent().parent().find('#pengadaanDetailAja').val()
    var hargaAja = $(e).parent().parent().parent().parent().find('#hargaAja').html()
    var judulAja = $(e).parent().parent().parent().parent().find('#judulAja').html()
    var estJadiAja = $(e).parent().parent().parent().parent().find('#estJadiAja').html()
    $('#pengadaan_detail_id').val(pengadaanDetailAja)
    $('#price').val(hargaAja)
    $('#judulModal').html(judulAja)
    $('#id_cutting_detail').val(id)
    $('#est_jadi_modal').val(estJadiAja)
    $('#tanggal_selesai').flatpickr()
    validation()
}

function storeHasilCutting(){
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Simpan Hasil Cutting ?',
                    text: "Hasil cutting tidak bisa diubah lagi !",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, simpan!',
                    cancelButtonText: 'Tidak, batalkan!',
                    reverseButtons: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: '/admin/produksi/storehasilcutting',
                            type: 'POST',
                            data: $('#formModalCutting').serialize(),
                            dataType: 'json',
                            success: function (response) {
                                if (response.success) {
                                    Swal.fire({
                                        text: "Berhasil menambahkan hasil!",
                                        icon: "success",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Lanjutkan !",
                                        customClass: {
                                            confirmButton: "btn btn-primary",
                                        },
                                    }).then(function (t) {
                                        t.isConfirmed &&
                                            location.reload();
                                    });
                                } else {
                                    Swal.fire(
                                        response.errors,
                                        'Informasi lebih lanjut hubungi team.',
                                        'error'
                                    )
                                }
                            },
                            error: function (xhr, status, error) {
                                if (xhr.status === 422) {
                                    var errors = xhr.responseJSON.errors;
                                    Swal.fire(
                                        'Peringatan',
                                        errors[0],
                                        'warning'
                                    )
                                } else {
                                    Swal.fire(
                                        'Error',
                                        'Terjadi kesalahan: ' + status + ' ' + error,
                                        'error'
                                    )
                                }
                            }
                        });
                    } else if (
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Dibatalkan',
                            '',
                            'success'
                        )
                    }
                })

            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}


function validation() {
    validator = [];
    declareValidator = FormValidation.formValidation(form, {
        fields: {
            jumlah_akhir: {
                validators: {
                    notEmpty: {
                        message: "Hasil harus diisi !",
                    },
                },
            },
            tanggal_selesai: {
                validators: {
                    notEmpty: {
                        message: "Tanggal selesai harus diisi !",
                    },
                },
            },
            pembawa_id: {
                validators: {
                    notEmpty: {
                        message: "Pembawa harus diisi !",
                    },
                    numeric: {
                        message: "Pembawa harus diisi !",
                    },
                },
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
            }),
        },
    })

    validator.push(declareValidator);


}

function countTotal(){
    var x = parseFloat(0);
    var y; 
    $('.detail-form').each(function(index){
        var y = $(this).parent().parent().find('#est_jadi').val();
    if (!isNaN(parseFloat(y))) { 
        x += parseFloat(y);
    }
    })

    $('#jumlah_est_jadi').val(parseFloat(x))


}
