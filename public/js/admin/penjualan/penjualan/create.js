var form = document.getElementById('storePenjualan');
$("#tanggal").flatpickr();

var validator;
var declareValidator;

var produkIdValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}
var varianIdValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
        numeric: {
            message: "Data harus diisi !",
        },
    },
}
var qtyValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}

validation()

function getVarian(e) {
    var produk_id = $(e).val().split(",");;

    $.ajax({
        url: '/admin/penjualan/get-varian/' + produk_id[0],
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $(e).parent().parent().find('#price').val(produk_id[1]);
            var option = `<option></option>`
            $.each(response.varian, function (index, data) {
                option += `<option value="` + data.id +`">` + data.nama + `</option>`;
            })

            $(e).parent().parent().find('#varian_id').html(option);


            declareSomeLibraryOnRows();
            validation()
            itemValidation()

        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}

function getStok(e) {
    var produk_id = $(e).parent().parent().find('#produk_id').val().split(",");;

    $.ajax({
        url: '/admin/penjualan/get-stok/' + produk_id[0] +'/'+$(e).val(),
        type: 'GET',
        dataType: 'json',
        success: function (response) {
           var vall = parseFloat(response.ps)
            $(e).parent().parent().find('#stok_qty').val(vall.toFixed(0))
            $(e).parent().parent().find('#stok_qty_parent').val(vall.toFixed(0))
            countTotal()
        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}

function declareSomeLibraryOnRows() {
    $('.select-2-id').each(function (index) {
        $(this).attr('data-select2-id', 'select2[' + index + ']')
        $(this).select2()
    })

    $('.detail-form').each(function (index) {
        var elementHarga = $(this).parent().parent().find('#price')
        var elementTotal = $(this).parent().parent().find('#total')
        Inputmask({
            alias: "currency",
            prefix: ''
        }).mask(elementHarga);
        Inputmask({
            alias: "currency",
            prefix: ''
        }).mask(elementTotal);
    })

    var grandTotal = $('#grandTotal')

    Inputmask({
        alias: "currency",
        prefix: ''
    }).mask(grandTotal);
}


function countTotal() {
    var grandTotal = 0;
    var totalQty = 0;

    $('.detail-form').each(function(index) {
        var stok_qty_parent = $(this).parent().parent().find('#stok_qty_parent').val();
        var qty = $(this).parent().parent().find('#qty').val();
        var total = $(this).parent().parent().find('#total');
        var hargaInputBelumClean = $(this).parent().parent().find('#price').val();

        if (qty) {
            var newStokQty = parseFloat(stok_qty_parent) - parseFloat(qty);
            $(this).parent().parent().find('#stok_qty').val(newStokQty);
            totalQty += parseFloat(qty);

            // Cek jika stok tidak cukup dan set qty ke 0 tanpa memanggil countTotal lagi
            if (newStokQty < 0) {
                Swal.fire(
                    'Quantity jual tidak boleh melebihi stok !',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                );

                $('#btnSubmit').attr('disabled', true);
                $(this).parent().parent().find('#qty').val(0);
                $(this).parent().parent().find('#stok_qty').val(stok_qty_parent); // Kembalikan stok asli
                return false; // Hentikan loop each ini
            }else{
                $('#btnSubmit').removeAttr('disabled');
            }
        }

        // Periksa apakah hargaInputBelumClean telah didefinisikan
        if (hargaInputBelumClean) {
            var hargaTanpaKarakter = hargaInputBelumClean.replace(/,/g, '');
            total.val(parseFloat(qty) * parseFloat(hargaTanpaKarakter));
            grandTotal += parseFloat(qty) * parseFloat(hargaTanpaKarakter);
        }
    });

    $('#totalQty').val(totalQty);
    $('#grandTotal').val(grandTotal);
}


function validation() {
    validator = [];
    declareValidator = FormValidation.formValidation(form, {
        fields: {

            kode: {
                validators: {
                    notEmpty: {
                        message: "Kode harus diisi !",
                    },
                },
            },
            media_id: {
                validators: {
                    notEmpty: {
                        message: "Media harus diisi !",
                    },
                },
            },
            shipper_id: {
                validators: {
                    notEmpty: {
                        message: "Shipper harus diisi !",
                    },
                    numeric: {
                        message: "Shipper harus diisi !",
                    },
                },
            },
            tanggal: {
                validators: {
                    notEmpty: {
                        message: "Tanggal harus diisi !",
                    },
                },
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
            }),
        },
    })

    validator.push(declareValidator);


}

function itemValidation() {
    $('.detail-form').each(function (index) {
        $(this).parent().parent().find('#produk_id').attr('name', 'produk_id[' + index + ']');
        $(this).parent().parent().find('#varian_id').attr('name', 'varian_id[' + index + ']');
        $(this).parent().parent().find('#qty').attr('name', 'qty[' + index + ']');
        $(this).parent().parent().find('#price').attr('name', 'price[' + index + ']');
        $(this).parent().parent().find('#total').attr('name', 'total[' + index + ']');
        $(this).parent().parent().find('#stok_qty_parent').attr('name', 'stok_qty_parent[' + index + ']');

        validator[0]
            .addField('produk_id[' + index + ']', produkIdValidator)
            .addField('varian_id[' + index + ']', varianIdValidator)
            .addField('qty[' + index + ']', qtyValidator)
    })
}



function store() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                if($('#produk_id').length){
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Simpan data ?',
                    text: "Pastikan Semua Kolom Diisi dengan benar",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, simpan!',
                    cancelButtonText: 'Tidak, batalkan!',
                    reverseButtons: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: '/admin/penjualan',
                            type: 'POST',
                            data: $('#storePenjualan').serialize(),
                            dataType: 'json',
                            success: function (response) {
                                if (response.success) {
                                    Swal.fire({
                                        text: "Berhasil membuat Penjualan!",
                                        icon: "success",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Lanjutkan !",
                                        customClass: {
                                            confirmButton: "btn btn-primary",
                                        },
                                    }).then(function (t) {
                                        t.isConfirmed &&
                                            window.location.replace("/admin/penjualan");
                                        pengadaanTable.ajax.reload();
                                    });
                                } else {
                                    Swal.fire(
                                        response.errors,
                                        'Informasi lebih lanjut hubungi team.',
                                        'error'
                                    )
                                }
                            },
                            error: function (xhr, status, error) {
                                if (xhr.status === 422) {
                                    var errors = xhr.responseJSON.errors;
                                    Swal.fire(
                                        'Peringatan',
                                        errors[0],
                                        'warning'
                                    )
                                } else {
                                    Swal.fire(
                                        'Error',
                                        'Terjadi kesalahan: ' + status + ' ' + error,
                                        'error'
                                    )
                                }
                            }
                        });
                    } else if (
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Dibatalkan',
                            '',
                            'success'
                        )
                    }
                })
    
            }else{
                Swal.fire(
                    'Tidak ada produk yang dijual !',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )
            }
            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}

function addRowVariant() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                var mediaId = $('#media_id').val()

                var templates = `
                             <tr>
                                <td class="fv-row">
                                    <select class="form-select form-select-solid fs-7 select-2-id" data-control="select2"
                                        data-placeholder="Pilih Produk" id="produk_id" onchange="getVarian(this)">
                                        <option></option>
                                    </select>
                                </td>
                                <td class="fv-row">
                                    <select class="form-select form-select-solid fs-7 select-2-id detail-form" data-control="select2"
                                        data-placeholder="Pilih Varian" id="varian_id" onchange="getStok(this)">
                                        <option></option>
                                    </select>
                                </td>
                                <td class="fv-row">
                                <input type="hidden" id="stok_qty_parent" />
                                    <input class="form-control form-control-white text-end fs-7" type="number" id="stok_qty" data-bs-toggle="tooltip" data-bs-placement="top" title="Sisa stok setelah dikurangi qty penjualan ini."
                                        autocomplete="off" readonly/>
                                </td>
                                <td class="fv-row">
                                    <input class="form-control form-control-solid text-end fs-7" type="number" id="qty"
                                        autocomplete="off" value="0" onkeyup="countTotal(this)"/>
                                </td>
                                <td class="fv-row">
                                    <input class="form-control form-control-white text-end fs-7" type="text" id="price"
                                        autocomplete="off" value="0" readonly />
                                </td>
                                <td class="fv-row">
                                <input class="form-control form-control-white text-end fs-7" type="text" id="total"
                                    autocomplete="off" readonly/>
                                </td>
                                <td class="fv-row text-center"> <button class="btn btn-sm btn-danger"
                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus detail"
                                        type="button" onclick="removeRowVariant(this)">-</button></td>

                            </tr>
                       `;

                $.ajax({
                    url: '/admin/penjualan/get-detail-form/' + mediaId,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        
                        $('#tableItems').append(templates);
                        var option = `<option></option>`
                        $.each(response.produk, function (index, data) {
                            option += `<option value="` + data.id +','+ data.harga_jual+ ','+data.harga_produk_id+`">` + data.nama + `</option>`;
                        })

                        $('.detail-form').each(function () {
                            if ($.isNumeric($(this).val()) === false) {
                                $(this).parent().parent().find('#produk_id').html(option)
                            }
                        })

                        validation()
                        itemValidation()
                        declareSomeLibraryOnRows();
                    },
                    error: function (xhr, status, error) {
                        if (xhr.status === 422) {
                            var errors = xhr.responseJSON.errors;
                            Swal.fire(
                                'Peringatan',
                                errors[0],
                                'warning'
                            )
                        } else {
                            Swal.fire(
                                'Error',
                                'Terjadi kesalahan: ' + status + ' ' + error,
                                'error'
                            )
                        }
                    }
                });
            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}

function removeRowVariant(e) {
    $(e).parent().parent().remove();
    validation()
    itemValidation()
    declareSomeLibraryOnRows();
    countTotal()
}
