var form = document.getElementById('storePricing');
var hpp = $('#hpp')
Inputmask({
    alias: "currency",
    prefix: ''
}).mask(hpp);
var harga_jual = $('#harga_jual')
Inputmask({
    alias: "currency",
    prefix: ''
}).mask(harga_jual);


var validator = FormValidation.formValidation(form, {
    fields: {
        hpp: {
            validators: {
                notEmpty: {
                    message: "HPP harus diisi !",
                },
            },
        },
        harga_jual: {
            validators: {
                notEmpty: {
                    message: "Harga Jual harus diisi !",
                },
            },
        },
        produk_id: {
            validators: {
                notEmpty: {
                    message: "Harap pilih produk.",
                },
                integer: {
                    message: "Harap pilih produk.",
                },
            },
        },
        kategori_media_penjualan_id: {
            validators: {
                notEmpty: {
                    message: "Harap pilih Kategori.",
                },
                integer: {
                    message: "Harap pilih Kategori.",
                },
            },
        },
        media_penjualan_id: {
            validators: {
                notEmpty: {
                    message: "Harap pilih Media.",
                },
                integer: {
                    message: "Harap pilih Media.",
                },
            },
        },
    },
    plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        bootstrap: new FormValidation.plugins.Bootstrap5({
            rowSelector: ".fv-row",
            eleInvalidClass: "",
            eleValidClass: "",
        }),
    },
})


function getMedia(e) {
    $.ajax({
        url: '/admin/masterdata/pricing/get-media/' + $(e).val(),
        type: 'get',
        success: function (response) {
            // console.log(response);
            var option = '<option></option>';
            $('#media_penjualan_id').html(option)
            if (response.data.length < 1) {
                Swal.fire(
                    'Tidak bisa dipilih karena tidak ada media penjualan pada kategori ini.',
                    'Harap ubah kategori atau tambahkan media penjualan terlebih dahulu pada kategori dipilih.',
                    'warning'
                )
            } else {
                option = '<option></option>';
                $.each(response.data, function (index, media) {
                    option += `<option value="` + media.id + `">` + media.nama + `</option>`;
                })

                $('#media_penjualan_id').html(option)
            }

        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}



function store() {
    if (validator) {
        validator.validate().then(function (status) {
            if (status == 'Valid') {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Simpan data ?',
                    text: "Pastikan semua kolom diisi dengan benar",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, simpan!',
                    cancelButtonText: 'Tidak, batalkan!',
                    reverseButtons: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: '/admin/masterdata/pricing',
                            type: 'POST',
                            data: $('#storePricing').serialize(),
                            dataType: 'json',
                            success: function (response) {
                                if (response.success) {
                                    Swal.fire({
                                        text: "Berhasil membuat Harga Baru!",
                                        icon: "success",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Lanjutkan !",
                                        customClass: {
                                            confirmButton:
                                                "btn btn-primary",
                                        },
                                    }).then(function (t) {
                                        t.isConfirmed &&
                                        window.location.replace("/admin/masterdata/pricing");
                                    });
                                } else {
                                    Swal.fire(
                                        response.errors,
                                        'Informasi lebih lanjut hubungi team.',
                                        'error'
                                    )
                                }
                            },
                            error: function (xhr, status, error) {
                                if (xhr.status === 422) {
                                    var errors = xhr.responseJSON.errors;
                                    Swal.fire(
                                        'Peringatan',
                                        errors[0],
                                        'warning'
                                    )
                                } else {
                                    Swal.fire(
                                        'Error',
                                        'Terjadi kesalahan: ' + status + ' ' + error,
                                        'error'
                                    )
                                }
                            }
                        });
                    } else if (
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Dibatalkan',
                            '',
                            'success'
                        )
                    }
                })
            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }

}