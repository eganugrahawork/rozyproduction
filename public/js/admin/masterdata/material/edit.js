var form = document.getElementById('updateMaterial');

var validator;
var declareValidator;
var varianValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}
var stokAwalVAlidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}
var hargaValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}

libraryOnRows()
validation()

function update() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                var nilaiDitemukan = {};
                var isBerbeda = false;
                var nilaiSama = '';
                $('.is-varian').each(function () {
                    var nilaiInput = $(this).val();
                    if (nilaiDitemukan[nilaiInput]) {
                        isBerbeda = true;
                        nilaiSama = nilaiInput;
                    } else {
                        nilaiDitemukan[nilaiInput] = true;
                    }
                })

                if (isBerbeda) {
                    Swal.fire(
                        'Peringatan',
                        'Varian ' + nilaiSama + ' tidak boleh sama !',
                        'warning'
                    )
                } else {
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    })

                    swalWithBootstrapButtons.fire({
                        title: 'Simpan data ?',
                        text: "Pastikan Semua Kolom Diisi dengan benar",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonText: 'Ya, simpan!',
                        cancelButtonText: 'Tidak, batalkan!',
                        reverseButtons: false
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: '/admin/masterdata/material/'+$('#id').val(),
                                type: 'PUT',
                                data: $('#updateMaterial').serialize(),
                                dataType: 'json',
                                success: function (response) {
                                    if (response.success) {
                                        Swal.fire({
                                            text: "Berhasil mengubah material!",
                                            icon: "success",
                                            buttonsStyling: !1,
                                            confirmButtonText: "Lanjutkan !",
                                            customClass: {
                                                confirmButton: "btn btn-primary",
                                            },
                                        }).then(function (t) {
                                            t.isConfirmed &&
                                            window.location.replace("/admin/masterdata/material");
                                            materialTable.ajax.reload();
                                        });
                                    } else {
                                        Swal.fire(
                                            response.errors,
                                            'Informasi lebih lanjut hubungi team.',
                                            'error'
                                        )
                                    }
                                },
                                error: function (xhr, status, error) {
                                    if (xhr.status === 422) {
                                        var errors = xhr.responseJSON.errors;
                                        Swal.fire(
                                            'Peringatan',
                                            errors[0],
                                            'warning'
                                        )
                                    } else {
                                        Swal.fire(
                                            'Error',
                                            'Terjadi kesalahan: ' + status + ' ' + error,
                                            'error'
                                        )
                                    }
                                }
                            });
                        } else if (
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            swalWithBootstrapButtons.fire(
                                'Dibatalkan',
                                '',
                                'success'
                            )
                        }
                    })
                }
            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}

function validation() {
    validator = [];
    declareValidator = FormValidation.formValidation(form, {
        fields: {
            jenis_material_id: {
                validators: {
                    notEmpty: {
                        message: "Jenis harus diisi !",
                    },
                    numeric: {
                        message: "Jenis harus diisi !",
                    },
                },
            },
            nama: {
                validators: {
                    notEmpty: {
                        message: "Nama harus diisi !",
                    },
                    remote: {
                        url: "/admin/masterdata/material/check-code",
                        method: 'GET',
                        data: {
                            nama: $('#nama').val(),
                            nama_sebelumnya: $('#nama_sebelumnya').val()
                        },
                        message: 'Nama sudah tersedia.'
                    }
                },
            },
            uom_id: {
                validators: {
                    notEmpty: {
                        message: "Uom harus diisi !",
                    },
                    numeric: {
                        message: "Uom harus diisi !",
                    },
                },
            },
            kode: {
                validators: {
                    notEmpty: {
                        message: "Kode harus diisi !",
                    },
                    remote: {
                        url: "/admin/masterdata/material/check-code",
                        method: 'GET',
                        data: {
                            kode: $('#kode').val(),
                            kode_sebelumnya: $('#kode_sebelumnya').val()
                        },
                        message: 'Kode sudah tersedia.'
                    }
                },
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
            }),
        },
    })

    validator.push(declareValidator);

    $('.is-varian').each(function (index) {
        $(this).parent().parent().parent().find('#material_detail_id').attr('name', 'material_detail_id[' + index + ']');
        $(this).parent().parent().parent().find('#varian').attr('name', 'warna[' + index + ']');
        $(this).parent().parent().parent().find('#stok_awal').attr('name', 'stok_awal[' + index + ']');
        $(this).parent().parent().parent().find('#harga_stok_awal').attr('name', 'harga_stok_awal[' + index + ']');
        $(this).parent().parent().parent().find('#id_history').attr('name', 'id_history[' + index + ']');

        validator[0].addField('warna[' + index + ']', varianValidator)
        validator[0].addField('stok_awal[' + index + ']', stokAwalVAlidator)
        validator[0].addField('harga_stok_awal[' + index + ']', hargaValidator)
    })
}


function addRowVariant() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            var nilaiDitemukan = {};
            var isBerbeda = false;
            var nilaiSama = '';
            if (status == 'Valid') {

                $('.is-varian').each(function () {
                    var nilaiInput = $(this).val();
                    if (nilaiDitemukan[nilaiInput]) {
                        isBerbeda = true;
                        nilaiSama = nilaiInput;
                    } else {
                        nilaiDitemukan[nilaiInput] = true;
                    }
                })

                if (isBerbeda) {
                    Swal.fire(
                        'Peringatan',
                        'Varian ' + nilaiSama + ' tidak boleh sama !',
                        'warning'
                    )
                } else {
                    var template = ` <div class="row mb-3">
                                <div class="col-lg-4">
                                    <div class="fv-row mb-2">
                                        <label class="form-label fs-7 fw-bolder text-dark">Varian</label>
                                        <input class="form-control form-control-solid fs-7 is-varian" type="text"
                                            autocomplete="off" id="varian" />
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="fv-row">
                                        <label class="form-label fs-7 fw-bolder text-dark">Stok Awal</label>
                                        <input class="form-control form-control-solid text-end fs-7 stok-awal" type="number"
                                            autocomplete="off" id="stok_awal" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="fv-row">
                                        <label class="form-label fs-7 fw-bolder text-dark">Harga</label>
                                        <input class="form-control form-control-solid text-end fs-7" type="text"
                                            autocomplete="off" id="harga_stok_awal" />
                                    </div>
                                </div>
                                <div class="col-lg-2 d-flex align-items-center justify-content-end">
                                    <button class="btn btn-sm btn-danger" data-bs-toggle="tooltip" data-bs-placement="top"
                                        title="Hapus Varian" type="button" onclick="removeRowVariant(this)">-</button>
                                </div>
                            </div>`;

                    $('#varianDisini').append(template)
                    libraryOnRows()
                    validation()
                }



            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data dan varian terisi semua sebelum menambah !',
                    'warning'
                )
            }

        });
    }
}

function removeRowVariant(e) {
    var ix = 0;
    $('.is-varian').each(function(index){
        ix += 1
    });

    if( ix <= 1){
        Swal.fire(
            'Peringatan',
            'Harap isi minimal 1 varian',
            'warning'
        )
    }else{
        $(e).parent().parent().remove()
        libraryOnRows()
        validation()
    }
}

function libraryOnRows(){
    $('.is-varian').each(function (index) {
        var elementHarga = $(this).parent().parent().parent().find('#harga_stok_awal')
        Inputmask({
            alias: "currency",
            prefix: ''
        }).mask(elementHarga);
       
    })
}