var form =document.getElementById('updateJenisMaterial');

var validator = FormValidation.formValidation(form, {
    fields: {

        nama: {
            validators: {
                notEmpty: {
                    message: "Nama Tipe harus diisi !",
                },
            },
        },
    },
    plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        bootstrap: new FormValidation.plugins.Bootstrap5({
            rowSelector: ".fv-row",
            eleInvalidClass: "",
            eleValidClass: "",
        }),
    },
})



function update(){
    if (validator) {
        validator.validate().then(function (status) {
            if (status == 'Valid') {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Simpan data ?',
                    text: "Pastikan Semua Kolom Diisi dengan benar",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, simpan!',
                    cancelButtonText: 'Tidak, batalkan!',
                    reverseButtons: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: '/admin/masterdata/jenis-material/'+$('#id').val(),
                            type: 'PUT',
                            data: $('#updateJenisMaterial').serialize(),
                            dataType: 'json',
                            success: function(response) {
                                if (response.success) {
                                    Swal.fire({
                                        text: "Berhasil mengubah jenis material!",
                                        icon: "success",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Lanjutkan !",
                                        customClass: {
                                            confirmButton:
                                                "btn btn-primary",
                                        },
                                    }).then(function (t) {
                                        t.isConfirmed &&
                                        $("#modalTipeBarang").modal('hide')
                                        jenisMaterialTable.ajax.reload();
                                    });
                                } else {
                                    Swal.fire(
                                        response.errors,
                                        'Informasi lebih lanjut hubungi team.',
                                        'error'
                                    )
                                }
                            },
                            error: function(xhr, status, error) {
                                if (xhr.status === 422) {
                                    var errors = xhr.responseJSON.errors;
                                    Swal.fire(
                                        'Peringatan',
                                        errors[0],
                                        'warning'
                                    )
                                } else {
                                    Swal.fire(
                                        'Error',
                                        'Terjadi kesalahan: ' + status + ' '+ error,
                                        'error'
                                    )
                                }
                            }
                        });
                    } else if (
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Dibatalkan',
                            '',
                            'success'
                        )
                    }
                })
            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }

}

function tutupModalEdit() {
    $('#modalTipeBarang').modal('hide');
}
