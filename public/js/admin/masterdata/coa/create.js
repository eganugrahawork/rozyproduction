var form = document.getElementById('storeCoa');


validation()

function store() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                var nilaiDitemukan = {};
                var isBerbeda = false;
                var nilaiSama = '';
                $('.is-varian').each(function () {
                    var nilaiInput = $(this).val();
                    if (nilaiDitemukan[nilaiInput]) {
                        isBerbeda = true;
                        nilaiSama = nilaiInput;
                    } else {
                        nilaiDitemukan[nilaiInput] = true;
                    }
                })

                if (isBerbeda) {
                    Swal.fire(
                        'Peringatan',
                        'Varian ' + nilaiSama + ' tidak boleh sama !',
                        'warning'
                    )
                } else {
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    })

                    swalWithBootstrapButtons.fire({
                        title: 'Simpan data ?',
                        text: "Pastikan semua formulir Diisi dengan benar",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonText: 'Ya, simpan!',
                        cancelButtonText: 'Tidak, batalkan!',
                        reverseButtons: false
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: '/admin/masterdata/coa',
                                type: 'POST',
                                data: $('#storeCoa').serialize(),
                                dataType: 'json',
                                success: function (response) {
                                    if (response.success) {
                                        Swal.fire({
                                            text: "Berhasil membuat Coa!",
                                            icon: "success",
                                            buttonsStyling: !1,
                                            confirmButtonText: "Lanjutkan !",
                                            customClass: {
                                                confirmButton: "btn btn-primary",
                                            },
                                        }).then(function (t) {
                                            t.isConfirmed &&
                                                window.location.replace("/admin/masterdata/coa");
                                            materialTable.ajax.reload();
                                        });
                                    } else {
                                        Swal.fire(
                                            response.errors,
                                            'Informasi lebih lanjut hubungi team.',
                                            'error'
                                        )
                                    }
                                },
                                error: function (xhr, status, error) {
                                    if (xhr.status === 422) {
                                        var errors = xhr.responseJSON.errors;
                                        Swal.fire(
                                            'Peringatan',
                                            errors[0],
                                            'warning'
                                        )
                                    } else {
                                        Swal.fire(
                                            'Error',
                                            'Terjadi kesalahan: ' + status + ' ' + error,
                                            'error'
                                        )
                                    }
                                }
                            });
                        } else if (
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            swalWithBootstrapButtons.fire(
                                'Dibatalkan',
                                '',
                                'success'
                            )
                        }
                    })
                }
            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}

function validation() {
    validator = [];
    declareValidator = FormValidation.formValidation(form, {
        fields: {

            parent_id: {
                validators: {
                    notEmpty: {
                        message: "Parent harus dipilih !",
                    },
                    numeric: {
                        message: "Parent harus dipilih !",
                    },
                },
            },
            adjustment: {
                validators: {
                    notEmpty: {
                        message: "Adjustment harus dipilih !",
                    },
                    numeric: {
                        message: "Adjustment harus dipilih !",
                    },
                },
            },
            kode: {
                validators: {
                    notEmpty: {
                        message: "Kode harus diisi !",
                    },
                    remote: {
                        url: "/admin/masterdata/coa/check-unique",
                        method: 'GET',
                        data: {
                            kode: $('#kode').val()
                        },
                        message: 'kode ' + $('#kode').val() + ' sudah tersedia.'
                    }
                },
            },
            nama: {
                validators: {
                    notEmpty: {
                        message: "Nama harus diisi !",
                    },
                    remote: {
                        url: "/admin/masterdata/coa/check-unique",
                        method: 'GET',
                        data: {
                            nama: $('#nama').val()
                        },
                        message: 'Nama ' + $('#nama').val() + ' sudah tersedia.'
                    }
                },
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
            }),
        },
    })

    validator.push(declareValidator);
}