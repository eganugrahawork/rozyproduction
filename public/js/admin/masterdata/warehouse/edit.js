var form = document.getElementById("updateWarehouse");

validation();

function update() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == "Valid") {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: "btn btn-success",
                        cancelButton: "btn btn-danger",
                    },
                    buttonsStyling: false,
                });

                swalWithBootstrapButtons
                    .fire({
                        title: "Simpan data ?",
                        text: "Pastikan semua formulir Diisi dengan benar",
                        icon: "question",
                        showCancelButton: true,
                        confirmButtonText: "Ya, simpan!",
                        cancelButtonText: "Tidak, batalkan!",
                        reverseButtons: false,
                    })
                    .then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: "/admin/masterdata/warehouse/"+ $('#id').val(),
                                type: "PUT",
                                data: $("#updateWarehouse").serialize(),
                                dataType: "json",
                                success: function (response) {
                                    if (response.success) {
                                        Swal.fire({
                                            text: "Berhasil mengubah Warehouse!",
                                            icon: "success",
                                            buttonsStyling: !1,
                                            confirmButtonText: "Lanjutkan !",
                                            customClass: {
                                                confirmButton:
                                                    "btn btn-primary",
                                            },
                                        }).then(function (t) {
                                            t.isConfirmed &&
                                                window.location.replace(
                                                    "/admin/masterdata/warehouse"
                                                );
                                            materialTable.ajax.reload();
                                        });
                                    } else {
                                        Swal.fire(
                                            response.errors,
                                            "Informasi lebih lanjut hubungi team.",
                                            "error"
                                        );
                                    }
                                },
                                error: function (xhr, status, error) {
                                    if (xhr.status === 422) {
                                        var errors = xhr.responseJSON.errors;
                                        Swal.fire(
                                            "Peringatan",
                                            errors[0],
                                            "warning"
                                        );
                                    } else {
                                        Swal.fire(
                                            "Error",
                                            "Terjadi kesalahan: " +
                                                status +
                                                " " +
                                                error,
                                            "error"
                                        );
                                    }
                                },
                            });
                        } else if (
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            swalWithBootstrapButtons.fire(
                                "Dibatalkan",
                                "",
                                "success"
                            );
                        }
                    });
            } else {
                Swal.fire(
                    "Peringatan",
                    "Pastikan data diisi dengan benar !",
                    "warning"
                );
            }
        });
    }
}

function validation() {
    validator = [];
    declareValidator = FormValidation.formValidation(form, {
        fields: {
            pic_id: {
                validators: {
                    notEmpty: {
                        message: "PIC harus dipilih !",
                    },
                    numeric: {
                        message: "PIC harus dipilih !",
                    },
                },
            },
            nama: {
                validators: {
                    notEmpty: {
                        message: "Nama harus diisi !",
                    },
                },
            },
            alamat: {
                validators: {
                    notEmpty: {
                        message: "Alamat harus diisi !",
                    },
                },
            },
            kode_pos: {
                validators: {
                    notEmpty: {
                        message: "Kode Pos harus diisi !",
                    },
                },
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
            }),
        },
    });

    validator.push(declareValidator);
}
