var form = document.getElementById('storeProduk');

var validator;
var declareValidator;
var varianValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}
var stokAwalVAlidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}

var elementHpp = $('#hpp')
Inputmask({
    alias: "currency",
    prefix: ''
}).mask(elementHpp);
validation()

function store() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                var nilaiDitemukan = {};
                var isBerbeda = false;
                var nilaiSama = '';
                $('.is-varian').each(function () {
                    var nilaiInput = $(this).val();
                    if (nilaiDitemukan[nilaiInput]) {
                        isBerbeda = true;
                        nilaiSama = nilaiInput;
                    } else {
                        nilaiDitemukan[nilaiInput] = true;
                    }
                })

                if (isBerbeda) {
                    Swal.fire(
                        'Peringatan',
                        'Varian ' + nilaiSama + ' tidak boleh sama !',
                        'warning'
                    )
                } else {
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    })

                    swalWithBootstrapButtons.fire({
                        title: 'Simpan data ?',
                        text: "Pastikan Semua Kolom Diisi dengan benar",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonText: 'Ya, simpan!',
                        cancelButtonText: 'Tidak, batalkan!',
                        reverseButtons: false
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: '/admin/masterdata/produk',
                                type: 'POST',
                                data: $('#storeProduk').serialize(),
                                dataType: 'json',
                                success: function (response) {
                                    if (response.success) {
                                        Swal.fire({
                                            text: "Berhasil membuat Produk!",
                                            icon: "success",
                                            buttonsStyling: !1,
                                            confirmButtonText: "Lanjutkan !",
                                            customClass: {
                                                confirmButton: "btn btn-primary",
                                            },
                                        }).then(function (t) {
                                            t.isConfirmed &&
                                                window.location.replace("/admin/masterdata/produk");
                                            produkTable.ajax.reload();
                                        });
                                    } else {
                                        Swal.fire(
                                            response.errors,
                                            'Informasi lebih lanjut hubungi team.',
                                            'error'
                                        )
                                    }
                                },
                                error: function (xhr, status, error) {
                                    if (xhr.status === 422) {
                                        var errors = xhr.responseJSON.errors;
                                        Swal.fire(
                                            'Peringatan',
                                            errors[0],
                                            'warning'
                                        )
                                    } else {
                                        Swal.fire(
                                            'Error',
                                            'Terjadi kesalahan: ' + status + ' ' + error,
                                            'error'
                                        )
                                    }
                                }
                            });
                        } else if (
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            swalWithBootstrapButtons.fire(
                                'Dibatalkan',
                                '',
                                'success'
                            )
                        }
                    })
                }
            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}

function validation() {
    validator = [];
    declareValidator = FormValidation.formValidation(form, {
        fields: {
            kategori_id: {
                validators: {
                    notEmpty: {
                        message: "Kategori harus diisi !",
                    },
                    integer: {
                        message: "Kategori harus diisi !",
                    },
                },
            },
            nama: {
                validators: {
                    notEmpty: {
                        message: "Nama harus diisi !",
                    },
                },
            },
            hpp: {
                validators: {
                    notEmpty: {
                        message: "Hpp harus diisi !",
                    },
                },
            },
            material_id: {
                validators: {
                    notEmpty: {
                        message: "Bahan harus diisi !",
                    },
                    integer: {
                        message: "Bahan harus diisi !",
                    },
                },
            },
            number_cutting_require: {
                validators: {
                    notEmpty: {
                        message: "Bagian ini harus diisi !",
                    },
                    number: {
                        message: "Isi dengan benar !",
                    },
                },
            },
            kode: {
                validators: {
                    notEmpty: {
                        message: "Kode harus diisi !",
                    },
                    remote: {
                        url: "/admin/masterdata/produk/check-code",
                        method: 'GET',
                        data: {
                            kode: $('#kode').val()
                        },
                        message: 'Kode ' + $('#kode').val() + ' sudah tersedia.'
                    }
                },
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
            }),
        },
    })

    validator.push(declareValidator);

    $('.is-varian').each(function (index) {
        
        $(this).parent().parent().parent().find('#varian').attr('name', 'warna[' + index + ']');
        $(this).parent().parent().parent().find('#stok_awal').attr('name', 'stok_awal[' + index + ']');

        validator[0].addField('warna[' + index + ']', varianValidator)
        validator[0].addField('stok_awal[' + index + ']', stokAwalVAlidator)
    })
}


function addRowVariant() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            var nilaiDitemukan = {};
            var isBerbeda = false;
            var nilaiSama = '';
            if (status == 'Valid') {

                $('.is-varian').each(function () {
                    var nilaiInput = $(this).val();
                    if (nilaiDitemukan[nilaiInput]) {
                        isBerbeda = true;
                        nilaiSama = nilaiInput;
                    } else {
                        nilaiDitemukan[nilaiInput] = true;
                    }
                })

                if (isBerbeda) {
                    Swal.fire(
                        'Peringatan',
                        'Varian ' + nilaiSama + ' tidak boleh sama !',
                        'warning'
                    )
                } else {
                    var template = `
                  <div class="row mb-3">
                                <div class="col-lg-6">
                                    <div class="fv-row mb-2">
                                        <label class="form-label fs-7 fw-bolder text-dark">Varian</label>
                                        <input class="form-control form-control-solid fs-7 is-varian" type="text"
                                            autocomplete="off" id="varian" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="fv-row">
                                        <label class="form-label fs-7 fw-bolder text-dark">Stok Awal</label>
                                        <input class="form-control form-control-solid text-end fs-7 stok-awal" type="number"
                                            autocomplete="off" id="stok_awal" />
                                    </div>
                                </div>
                                <div class="col-lg-2 d-flex align-items-center justify-content-end">
                                    <button class="btn btn-sm btn-danger" data-bs-toggle="tooltip" data-bs-placement="top"
                                        title="Hapus Varian" type="button" onclick="removeRowVariant(this)">-</button>
                                </div>
                            </div>
                    `;

                    $('#varianDisini').append(template)

                    validation()
                }



            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data dan varian terisi semua sebelum menambah !',
                    'warning'
                )
            }

        });
    }
}

function removeRowVariant(e) {
    var ix = 0;
    $('.is-varian').each(function (index) {
        ix += 1
    });

    if (ix <= 1) {
        Swal.fire(
            'Peringatan',
            'Harap isi minimal 1 varian',
            'warning'
        )
    } else {

        $(e).parent().parent().remove()
        validation()
    }
}
