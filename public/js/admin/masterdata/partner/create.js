var form = document.getElementById('storePartner');

var validator;
var declareValidator;

validation()

function store() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {

                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    })

                    swalWithBootstrapButtons.fire({
                        title: 'Simpan data ?',
                        text: "Pastikan Semua Kolom Diisi dengan benar",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonText: 'Ya, simpan!',
                        cancelButtonText: 'Tidak, batalkan!',
                        reverseButtons: false
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: '/admin/masterdata/partner',
                                type: 'POST',
                                data: $('#storePartner').serialize(),
                                dataType: 'json',
                                success: function (response) {
                                    if (response.success) {
                                        Swal.fire({
                                            text: "Berhasil membuat Partner!",
                                            icon: "success",
                                            buttonsStyling: !1,
                                            confirmButtonText: "Lanjutkan !",
                                            customClass: {
                                                confirmButton: "btn btn-primary",
                                            },
                                        }).then(function (t) {
                                            t.isConfirmed &&
                                            window.location.replace("/admin/masterdata/partner");
                                            partnerTable.ajax.reload();
                                        });
                                    } else {
                                        Swal.fire(
                                            response.errors,
                                            'Informasi lebih lanjut hubungi team.',
                                            'error'
                                        )
                                    }
                                },
                                error: function (xhr, status, error) {
                                    if (xhr.status === 422) {
                                        var errors = xhr.responseJSON.errors;
                                        Swal.fire(
                                            'Peringatan',
                                            errors[0],
                                            'warning'
                                        )
                                    } else {
                                        Swal.fire(
                                            'Error',
                                            'Terjadi kesalahan: ' + status + ' ' + error,
                                            'error'
                                        )
                                    }
                                }
                            });
                        } else if (
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            swalWithBootstrapButtons.fire(
                                'Dibatalkan',
                                '',
                                'success'
                            )
                        }
                    })
            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}

function validation() {
    validator = [];
    declareValidator = FormValidation.formValidation(form, {
        fields: {

            nama: {
                validators: {
                    notEmpty: {
                        message: "Nama harus diisi !",
                    },
                },
            },
            kode: {
                validators: {
                    notEmpty: {
                        message: "Kode harus diisi !",
                    },
                    remote: {
                        url: "/admin/masterdata/partner/check-code",
                        method: 'GET',
                        data: {
                            kode: $('#kode').val()
                        },
                        message: 'Nama sudah tersedia.'
                    }
                },
            },
            no_telp: {
                validators: {
                    notEmpty: {
                        message: "No Telepon/No Hp harus diisi !",
                    },
                },
            },
            kategori_id: {
                validators: {
                    notEmpty: {
                        message: "Kategori Tidak Boleh Kosong",
                    },
                    integer: {
                        message: "Kategori Tidak Boleh Kosong",
                    },
                },
            },
            alamat: {
                validators: {
                    notEmpty: {
                        message: "Alamat harus diisi !",
                    },
                },
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
            }),
        },
    })

    validator.push(declareValidator);
}

