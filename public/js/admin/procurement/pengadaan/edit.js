var form = document.getElementById('updatePengadaan');
$("#tanggal").flatpickr();

declareSomeLibraryOnRows()
countTotal()
getDataPartner()
var validator;
var declareValidator;
var materialIdValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
        numeric: {
            message: "Data harus diisi !",
        },
    },
}
var materialDetailIdValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
        numeric: {
            message: "Data harus diisi !",
        },
    },
}
var qtyValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}
var priceValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}
var totalValidator = {
    validators: {
        notEmpty: {
            message: 'Harap Isi Data ini !'
        },
    },
}

validation()

function getDataPartner() {
    var idPartner = $('#partner_id').val();
    $.ajax({
        url: '/admin/procurement/pengadaan/get-partner/' + idPartner,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $("#alamat").val(response.partner.alamat)
            $("#no_telp").val(response.partner.no_telp)

            validation()
            itemValidation()

        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}

function getMaterialVarian(e) {
    var materialId = $(e).val();

    $.ajax({
        url: '/admin/procurement/pengadaan/get-detail-material/' + materialId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            var option = `<option></option>`
            $.each(response.materialDetail, function (index, data) {
                option += `<option value="` + data.id + `">` + data.warna + `</option>`;
            })


            var isUom = '(' + response.uom.simbol + ')';
            $(e).parent().parent().find('#is_uom').val(isUom);
            $(e).parent().parent().find('#material_detail_id').html(option)


            declareSomeLibraryOnRows();
            validation()
            itemValidation()

        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}

function getCategory() {
    var kategoriId = $('#kategori_id').val()

    var templates = `<table class="table align-middle  gy-5">
                            <thead>
                                <tr class="fs-6 fw-bolder text-gray-600 text-center">
                                    <th class="min-w-125px">Material</th>
                                    <th class="min-w-125px">Detail</th>
                                    <th class="w-100px">Uom</th>
                                    <th class="w-100px">Quantity</th>
                                    <th class="mw-70px">Harga</th>
                                    <th class="mw-100px">Total</th>
                                    <th class="min-w-50px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="tableItems">
                                <tr>
                                    <td class="fv-row">
                                        <select class="form-select form-select-solid fs-7 select-2-id detail-form" data-control="select2"
                                            data-placeholder="Pilih Produk" id="material_id" onchange="getMaterialVarian(this)">
                                            <option></option>
                                        </select>
                                    </td>
                                    <td class="fv-row">
                                        <select class="form-select form-select-solid fs-7 select-2-id" id="material_detail_id" data-control="select2"
                                            data-placeholder="Pilih Varian">
                                            <option></option>
                                        </select>
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control text-gray-600 form-control-white text-center fs-7" type="text" id="is_uom"
                                            autocomplete="off"/>
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-solid text-end fs-7" type="number" id="qty"
                                            autocomplete="off" value="0" onkeyup="countTotal()"/>
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-solid text-end fs-7" type="text" id="price"
                                            autocomplete="off" onkeyup="countTotal()" value="0" />
                                    </td>
                                    <td class="fv-row">
                                    <input class="form-control form-control-white text-end fs-7" type="text" id="total"
                                        autocomplete="off" readonly/>
                                    </td>
                                    <td class="fv-row text-center"> <button class="btn btn-sm btn-danger"
                                            data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus detail"
                                            type="button" onclick="removeRowVariant(this)">-</button></td>

                                </tr>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-end" colspan="7"> <button class="btn btn-sm btn-success" type="button" onclick="addRowVariant()">+</button></th>
                                </tr>
                                <tr class="fw-bolder fs-7">
                                <th class="text-end" colspan="5"> Grand Total</th>
                                <th  colspan="2"><input type="text" class="form-control text-end form-control-white fs-7 text-dark" readonly id="grandTotal"/></th>
                                </tr>
                            </tfoot>
                        </table>`;

    $.ajax({
        url: '/admin/procurement/pengadaan/get-detail-form/' + kategoriId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $('#detailHere').html(templates);
            var option = `<option></option>`
            $.each(response.material, function (index, data) {
                option += `<option value="` + data.id + `">` + data.nama + `</option>`;
            })


            $('.detail-form').each(function () {
                if ($.isNumeric($(this).val()) === false) {
                    $(this).parent().find('#material_id').html(option)
                }
            })


            validation()
            itemValidation()
            declareSomeLibraryOnRows();


        },
        error: function (xhr, status, error) {
            if (xhr.status === 422) {
                var errors = xhr.responseJSON.errors;
                Swal.fire(
                    'Peringatan',
                    errors[0],
                    'warning'
                )
            } else {
                Swal.fire(
                    'Error',
                    'Terjadi kesalahan: ' + status + ' ' + error,
                    'error'
                )
            }
        }
    });
}

function declareSomeLibraryOnRows() {
    $('.select-2-id').each(function (index) {
        $(this).attr('data-select2-id', 'select2[' + index + ']')
        $(this).select2()
    })

    $('.detail-form').each(function (index) {
        var elementHarga = $(this).parent().parent().find('#price')
        var elementTotal = $(this).parent().parent().find('#total')
        Inputmask({
            alias: "currency",
            prefix: ''
        }).mask(elementHarga);
        Inputmask({
            alias: "currency",
            prefix: ''
        }).mask(elementTotal);
    })

    var grandTotal = $('#grandTotal')

    Inputmask({
        alias: "currency",
        prefix: ''
    }).mask(grandTotal);
}


function countTotal() {
    var grandTotal =0;


    $('.detail-form').each(function(index){
        var qty = $(this).parent().parent().find('#qty').val();
        var total = $(this).parent().parent().find('#total');
        var hargaInputBelumClean = $(this).parent().parent().find('#price').val();

        // Periksa apakah hargaInputBelumClean telah didefinisikan
        if (hargaInputBelumClean) {
            var hargaTanpaKarakter = hargaInputBelumClean.replace(/,/g, '');
            total.val(parseFloat(qty) * parseFloat(hargaTanpaKarakter));
            grandTotal += parseFloat(qty) * parseFloat(hargaTanpaKarakter);
        }
    })

    $('#grandTotal').val(grandTotal)

}


function validation() {
    validator = [];
    declareValidator = FormValidation.formValidation(form, {
        fields: {

            kode: {
                validators: {
                    notEmpty: {
                        message: "Kode harus diisi !",
                    },
                },
            },
            kategori_id: {
                validators: {
                    notEmpty: {
                        message: "Kategori harus diisi !",
                    },
                    numeric: {
                        message: "Kategori harus diisi !",
                    },
                },
            },
            partner_id: {
                validators: {
                    notEmpty: {
                        message: "Partner harus diisi !",
                    },
                    numeric: {
                        message: "Partner harus diisi !",
                    },
                },
            },
            warehouse_id: {
                validators: {
                    notEmpty: {
                        message: "Warehouse harus diisi !",
                    },
                    numeric: {
                        message: "Warehouse harus diisi !",
                    },
                },
            },
            pembawa_id: {
                validators: {
                    notEmpty: {
                        message: "Pembawa harus diisi !",
                    },
                    numeric: {
                        message: "Pembawa harus diisi !",
                    },
                },
            },
            tanggal: {
                validators: {
                    notEmpty: {
                        message: "Tanggal harus diisi !",
                    },
                },
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: ".fv-row",
                eleInvalidClass: "",
                eleValidClass: "",
            }),
        },
    })

    validator.push(declareValidator);


}

function itemValidation() {
    $('.detail-form').each(function (index) {
        $(this).parent().parent().find('#pengadaan_detail_id').attr('name', 'pengadaan_detail_id[' + index + ']');
        $(this).parent().parent().find('#material_id').attr('name', 'material_id[' + index + ']');
        $(this).parent().parent().find('#material_detail_id').attr('name', 'material_detail_id[' + index + ']');
        $(this).parent().parent().find('#qty').attr('name', 'qty[' + index + ']');
        $(this).parent().parent().find('#price').attr('name', 'price[' + index + ']');
        $(this).parent().parent().find('#total').attr('name', 'total[' + index + ']');

        validator[0]
            .addField('material_id[' + index + ']', materialIdValidator)
            .addField('material_detail_id[' + index + ']', materialDetailIdValidator)
            .addField('qty[' + index + ']', qtyValidator)
            .addField('price[' + index + ']', priceValidator)
            .addField('total[' + index + ']', totalValidator)
    })
}



function update() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Simpan data ?',
                    text: "Pastikan Semua Kolom Diisi dengan benar",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, simpan!',
                    cancelButtonText: 'Tidak, batalkan!',
                    reverseButtons: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: '/admin/procurement/pengadaan/'+ $('#id').val(),
                            type: 'PUT',
                            data: $('#updatePengadaan').serialize(),
                            dataType: 'json',
                            success: function (response) {
                                if (response.success) {
                                    Swal.fire({
                                        text: "Berhasil mengubah material!",
                                        icon: "success",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Lanjutkan !",
                                        customClass: {
                                            confirmButton: "btn btn-primary",
                                        },
                                    }).then(function (t) {
                                        t.isConfirmed &&
                                            window.location.replace("/admin/procurement/pengadaan");
                                        pengadaanTable.ajax.reload();
                                    });
                                } else {
                                    Swal.fire(
                                        response.errors,
                                        'Informasi lebih lanjut hubungi team.',
                                        'error'
                                    )
                                }
                            },
                            error: function (xhr, status, error) {
                                if (xhr.status === 422) {
                                    var errors = xhr.responseJSON.errors;
                                    Swal.fire(
                                        'Peringatan',
                                        errors[0],
                                        'warning'
                                    )
                                } else {
                                    Swal.fire(
                                        'Error',
                                        'Terjadi kesalahan: ' + status + ' ' + error,
                                        'error'
                                    )
                                }
                            }
                        });
                    } else if (
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Dibatalkan',
                            '',
                            'success'
                        )
                    }
                })

            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}

function addRowVariant() {
    if (validator[0]) {
        validator[0].validate().then(function (status) {
            if (status == 'Valid') {
                var kategoriId = $('#kategori_id').val()

                var templates = `
                            <tr>
                                <td class="fv-row">
                                    <select class="form-select form-select-solid fs-7 select-2-id detail-form" data-control="select2"
                                        data-placeholder="Pilih Produk" id="material_id" onchange="getMaterialVarian(this)">
                                        <option></option>
                                    </select>
                                </td>
                                <td class="fv-row">
                                    <select class="form-select form-select-solid fs-7 select-2-id" id="material_detail_id" data-control="select2"
                                        data-placeholder="Pilih Varian">
                                        <option></option>
                                    </select>
                                </td>
                                <td class="fv-row">
                                    <input class="form-control text-gray-600 form-control-white text-center fs-7" type="text" id="is_uom"
                                        autocomplete="off"/>
                                </td>
                                <td class="fv-row">
                                    <input class="form-control form-control-solid text-end fs-7" type="number" id="qty"
                                        autocomplete="off" value="0" onkeyup="countTotal(this)"/>
                                </td>
                                <td class="fv-row">
                                    <input class="form-control form-control-solid text-end fs-7" type="text" id="price"
                                        autocomplete="off" onkeyup="countTotal(this)" value="0" />
                                </td>
                                <td class="fv-row">
                                <input class="form-control form-control-white text-end fs-7" type="text" id="total"
                                    autocomplete="off" readonly/>
                                </td>
                                <td class="fv-row text-center"> <button class="btn btn-sm btn-danger"
                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus detail"
                                        type="button" onclick="removeRowVariant(this)">-</button></td>

                            </tr>
                       `;

                $.ajax({
                    url: '/admin/procurement/pengadaan/get-detail-form/' + kategoriId,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        $('#tableItems').append(templates);
                        var option = `<option></option>`
                        $.each(response.material, function (index, data) {
                            option += `<option value="` + data.id + `">` + data.nama + `</option>`;
                        })

                        $('.detail-form').each(function () {
                            if ($.isNumeric($(this).val()) === false) {
                                $(this).parent().find('#material_id').html(option)
                            }
                        })

                        validation()
                        itemValidation()
                        declareSomeLibraryOnRows();
                    },
                    error: function (xhr, status, error) {
                        if (xhr.status === 422) {
                            var errors = xhr.responseJSON.errors;
                            Swal.fire(
                                'Peringatan',
                                errors[0],
                                'warning'
                            )
                        } else {
                            Swal.fire(
                                'Error',
                                'Terjadi kesalahan: ' + status + ' ' + error,
                                'error'
                            )
                        }
                    }
                });
            } else {
                Swal.fire(
                    'Peringatan',
                    'Pastikan data diisi dengan benar !',
                    'warning'
                )

            }
        });
    }
}

function removeRowVariant(e) {
    $(e).parent().parent().remove();
    validation()
    itemValidation()
    declareSomeLibraryOnRows();
    countTotal()
}
