<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic - Bootstrap 5 HTML, VueJS, React, Angular & Laravel Admin Dashboard Theme
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <title id="titleBar"></title>
    <meta name="description"
        content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
    <meta name="keywords"
        content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="utf-8" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title"
        content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
    <meta property="og:url" content="https://keenthemes.com/metronic" />
    <meta property="og:site_name" content="Keenthemes | Metronic" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="shortcut icon" href="/assets/metronic/media/logos/thumbnail.png" />
	<link href="/assets/metronic/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="/assets/metronic/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/assets/metronic/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" class="header-tablet-and-mobile-fixed aside-enabled">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row flex-column-fluid">
            <!--begin::Aside-->
            @include('admin.layouts.aside')
            <!--end::Aside-->
            <!--begin::Wrapper-->
            <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <!--begin::Header-->
                @include('admin.layouts.header')
                <!--end::Header-->
                <!--begin::Content-->
                <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                    <!--begin::Post-->
                    <div class="post d-flex flex-column-fluid" id="kt_post">
                        <!--begin::Container-->
                        <div id="kt_content_container" class="container-xxl">
                            @yield('content')
                        </div>
                        <!--end::Container-->
                    </div>
                    <!--end::Post-->
                </div>
                <!--end::Content-->
                <!--begin::Footer-->
                @include('admin.layouts.footer')
                <!--end::Footer-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Root-->

    <!--end::Main-->
    <script>
        var hostUrl = "/assets/metronic/";
    </script>
    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="/assets/metronic/plugins/global/plugins.bundle.js"></script>
    <script src="/assets/metronic/js/scripts.bundle.js"></script>
	<script src="/assets/metronic/plugins/custom/datatables/datatables.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    {{--
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="/assets/metronic/js/custom/widgets.js"></script>
		<script src="/assets/metronic/js/custom/apps/chat/chat.js"></script>
		<script src="/assets/metronic/js/custom/modals/create-app.js"></script>
		<script src="/assets/metronic/js/custom/modals/upgrade-plan.js"></script>
		<!--end::Page Custom Javascript-->
 --}}
    <!--end::Javascript-->

    <script>
        var pathArray = window.location.pathname.split('/');
        var path1 = pathArray[1].charAt(0).toUpperCase() + pathArray[1].slice(1);
        var path2 = pathArray[2] ? pathArray[2].charAt(0).toUpperCase() + pathArray[2].slice(1) : "none";
        var path3 = pathArray[3] ? (pathArray[3].charAt(0).toUpperCase() + pathArray[3].slice(1)) : 'none';
        var path4 = pathArray[4] ? (pathArray[4].charAt(0).toUpperCase() + pathArray[4].slice(1)) : 'none';
        var breadCrumb = `<li class="breadcrumb-item text-muted">
                        <a href="#" class="text-muted text-hover-primary">` + path1 + `</a>
                    </li>`;
        if (path2 !== 'none') {
            breadCrumb += ` <li class="breadcrumb-item">
                        <span class="bullet bg-gray-200 w-5px h-2px"></span>
                    </li> <li class="breadcrumb-item text-dark">` + path2 + `</li>`
            $('#titleBar').html(path2 + "||Rozy")
            $('#titleBread').html(path2)
        }
        if (path3 !== "none") {
            if (path3.includes("-")) {
                var words = path3.split("-");

                words = words.map(function(word) {
                    return word.charAt(0).toUpperCase() + word.slice(1);
                });

                path3 = words.join(" ");
            }
            breadCrumb += ` <li class="breadcrumb-item">
                        <span class="bullet bg-gray-200 w-5px h-2px"></span>
						</li> <li class="breadcrumb-item text-dark">` + path3 + `</li>`
            $('#titleBar').html(path3 + " - Rozy")
            $('#titleBread').html(path3)
        }
        if (path4 !== "none") {
            if (path4.includes("-")) {
                var words = path4.split("-");

                words = words.map(function(word) {
                    return word.charAt(0).toUpperCase() + word.slice(1);
                });

                path4 = words.join(" ");
            }

            if($.isNumeric(path4)){
                path4 = "Detail";
            }

            breadCrumb += ` <li class="breadcrumb-item">
                        <span class="bullet bg-gray-200 w-5px h-2px"></span>
						</li> <li class="breadcrumb-item text-dark">` + path4 + `</li>`
            $('#titleBar').html(path4 + " - Rozy")
            $('#titleBread').html(path4)
        }

        $('#breadCrumbHere').html(breadCrumb)
    </script>

    @yield('jsOnPage')
</body>
<!--end::Body-->

</html>
