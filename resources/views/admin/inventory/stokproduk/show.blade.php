@extends('admin.layouts.main')
@section('content')
    <div class="mb-2">
        <a href="/admin/inventory/stok-produk" class="btn btn-secondary fw-bolder">Kembali
            <span class="svg-icon svg-icon-primary svg-icon-base"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                    height="24" viewBox="0 0 24 24" fill="none">
                    <path
                        d="M14 18H9.60001V16H14C15.1 16 16 15.1 16 14V3C16 2.4 16.4 2 17 2C17.6 2 18 2.4 18 3V14C18 16.2 16.2 18 14 18Z"
                        fill="black" />
                    <path opacity="0.3" d="M9.60002 12L5.3 16.3C4.9 16.7 4.9 17.3 5.3 17.7L9.60002 22V12Z"
                        fill="black" />
                </svg></span>
        </a>
    </div>
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
               Detail Stok
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="row g-5 g-xl-8">
                <!--begin::Col-->
                <div class="col-xl-12">
                    <!--begin::Mixed Widget 1-->
                    <div class="card card-xl-stretch mb-xl-8">
                        <!--begin::Body-->
                        <div class="card-body p-0">
                            <!--begin::Header-->
                            <div class="px-9 pt-7 card-rounded h-300px w-100 bg-primary">
                                <!--begin::Heading-->
                                <div class="d-flex flex-stack">
                                    <h3 class="m-0 text-white fw-bolder fs-3">Produk : {{ $jumlahStok->nama }}</h3>
                                    <h5 class="m-0 text-white fw-bolder fs-5">Jenis : {{ $jumlahStok->jenis }}</h5>
                                </div>
                                <!--end::Heading-->
                                <!--begin::Balance-->
                                <div class="d-flex text-center flex-column text-white pt-8">
                                    <span class="fw-bold fs-5">You Balance</span>
                                    <span class="fw-bolder fs-2x pt-1">{{ $jumlahStok->stok_akhir }}</span>
                                    <span class="fw-bold fs-7">{{ $jumlahStok->valuasi_produk }}</span>
                                    <a href="/admin/inventory/stok-produk/history/{{ $jumlahStok->id }}"
                                        class="text-white"><span class="svg-icon svg-icon-white svg-icon-1hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3" d="M22 4H5C4.4 4 4 4.4 4 5V22H13C13.6 22 14 21.6 14 21V18H17C17.6 18 18 17.6 18 17V14H21C21.6 14 22 13.6 22 13V4Z" fill="black"/>
                                            <path d="M13 18H10V15C10 14.4 10.4 14 11 14H14V17C14 17.6 13.6 18 13 18ZM3 18C2.4 18 2 18.4 2 19V21C2 21.6 2.4 22 3 22H9C9.6 22 10 21.6 10 21V18H3ZM18 13V10H15C14.4 10 14 10.4 14 11V14H17C17.6 14 18 13.6 18 13ZM21 2H19C18.4 2 18 2.4 18 3V10H21C21.6 10 22 9.6 22 9V3C22 2.4 21.6 2 21 2Z" fill="black"/>
                                            </svg></span> <span class="fw-bold fs-7">Lihat History</span></a>
                                </div>
                                <!--end::Balance-->
                            </div>
                            <div class="bg-body shadow-sm card-rounded mx-9 mb-9 px-6 py-9 position-relative z-index-1"
                                style="margin-top: -100px">
                                <div class="row mb-3">
                                    <div class="d-flex align-items-center position-relative my-1 col-lg">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                                        <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2"
                                                    rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                                <path
                                                    d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                                    fill="black" />
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                        <input type="text" id="searchmaterialTabel"
                                            class="form-control form-control-sm form-control-solid w-250px ps-15"
                                            placeholder="Cari" />
                                    </div>
                                    <div class="d-flex justify-content-end col-lg">
                                        <button type="button" class="btn btn-sm btn-light-primary me-3"
                                            data-bs-toggle="modal" data-bs-target="#kt_items_export_modal">
                                            <span class="svg-icon svg-icon-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none">
                                                    <rect opacity="0.3" x="12.75" y="4.25" width="12" height="2"
                                                        rx="1" transform="rotate(90 12.75 4.25)" fill="black" />
                                                    <path
                                                        d="M12.0573 6.11875L13.5203 7.87435C13.9121 8.34457 14.6232 8.37683 15.056 7.94401C15.4457 7.5543 15.4641 6.92836 15.0979 6.51643L12.4974 3.59084C12.0996 3.14332 11.4004 3.14332 11.0026 3.59084L8.40206 6.51643C8.0359 6.92836 8.0543 7.5543 8.44401 7.94401C8.87683 8.37683 9.58785 8.34458 9.9797 7.87435L11.4427 6.11875C11.6026 5.92684 11.8974 5.92684 12.0573 6.11875Z"
                                                        fill="black" />
                                                    <path
                                                        d="M18.75 8.25H17.75C17.1977 8.25 16.75 8.69772 16.75 9.25C16.75 9.80228 17.1977 10.25 17.75 10.25C18.3023 10.25 18.75 10.6977 18.75 11.25V18.25C18.75 18.8023 18.3023 19.25 17.75 19.25H5.75C5.19772 19.25 4.75 18.8023 4.75 18.25V11.25C4.75 10.6977 5.19771 10.25 5.75 10.25C6.30229 10.25 6.75 9.80228 6.75 9.25C6.75 8.69772 6.30229 8.25 5.75 8.25H4.75C3.64543 8.25 2.75 9.14543 2.75 10.25V19.25C2.75 20.3546 3.64543 21.25 4.75 21.25H18.75C19.8546 21.25 20.75 20.3546 20.75 19.25V10.25C20.75 9.14543 19.8546 8.25 18.75 8.25Z"
                                                        fill="#C4C4C4" />
                                                </svg>
                                            </span>Export</button>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table align-middle table-row-dashed fs-8 gy-5" id="materialTabel">
                                        <!--begin::Table head-->
                                        <thead>
                                            <!--begin::Table row-->
                                            <tr class="text-start text-gray-400 fw-bolder fs-8 text-uppercase gs-0">
                                                <th class="min-w-50px">No</th>
                                                <th class="min-w-75px">Detail
                                                </th>
                                                <th class="min-w-75px text-center">Stok Awal</th>
                                                <th class="min-w-75px text-center">Stok Masuk</th>
                                                <th class="min-w-75px text-center">Stok Keluar</th>
                                                <th class="min-w-75px text-center">Stok Akhir</th>
                                                <th class="min-w-100px text-center">Tanggal Diperbarui</th>
                                                <th class="-"></th>
                                            </tr>
                                            <!--end::Table row-->
                                        </thead>
                                        <!--end::Table head-->
                                        <!--begin::Table body-->
                                        <tbody class="fw-bold text-gray-600">
                                            @foreach ($stokDetail as $detail)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $detail->produk_detail }}</td>
                                                    <td class="text-center">{{ $detail->stok_awal }}</td>
                                                    <td class="text-center">{{ $detail->stok_masuk }}</td>
                                                    <td class="text-center">{{ $detail->stok_keluar }}</td>
                                                    <td class="text-center">{{ $detail->stok_akhir }}</td>
                                                    <td class="text-center ">{{ $detail->updated_at }}</td>
                                                    <td>
                                                        <a href="/admin/inventory/stok-produk/detail-history/{{ $detail->id }}"
                                                            class="btn btn-sm btn-light btn-active-light-primary">Cek History</a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                        <!--end::Table body-->
                                    </table>

                                </div>
                            </div>
                            <!--end::Header-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Mixed Widget 1-->
                </div>
                <!--end::Col-->
            </div>


            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->
    <!--begin::Modals-->
    <div class="modal fade" id="thisModalHere" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content" id="hereModalContent">
                <!--begin::Form-->

                <!--end::Form-->
            </div>
        </div>
    </div>
    <!--end::Modal - Customers - Add-->
    <!--begin::Modal - Adjust Balance-->
    <div class="modal fade" id="kt_items_export_modal" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Modal header-->
                <div class="modal-header">
                    <!--begin::Modal title-->
                    <h2 class="fw-bolder">Export Data</h2>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <button type="button" class="btn btn-icon btn-sm btn-active-icon-primary"
                        onclick="tutupExportModal()">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                    transform="rotate(-45 6 17.3137)" fill="black" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                                    transform="rotate(45 7.41422 6)" fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </button>
                    <!--end::Close-->
                </div>
                <!--end::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
                    <!--begin::Form-->
                    <form id="#" class="form" action="#">

                        <!--begin::Input group-->
                        <div class="fv-row mb-10">
                            <!--begin::Label-->
                            <label class="fs-5 fw-bold form-label mb-5">Pilih Format Export:</label>
                            <!--end::Label-->
                            <!--begin::Input-->
                            <select data-control="select2" data-placeholder="Select a format" data-hide-search="true"
                                name="format" id="formatExport" class="form-select form-select-solid">
                                <option value="excell">Excel</option>
                                <option value="pdf">PDF</option>
                                <option value="cvs">CVS</option>
                                <option value="zip">ZIP</option>
                            </select>
                            <!--end::Input-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Actions-->
                        <div class="text-center">
                            <button type="reset" onclick="tutupExportModal()" class="btn btn-light me-3">Batal</button>
                            <button type="button" onclick="exportList()" class="btn btn-primary">
                                Submit
                            </button>
                        </div>
                        <!--end::Actions-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Modal body-->
            </div>
            <!--end::Modal content-->
        </div>
        <!--end::Modal dialog-->
    </div>
@endsection


@section('jsOnPage')
    <script>
        var materialTabel = $('#materialTabel').DataTable({
            processing: true,
            // serverSide: true,
            searching: true,
            "language": {
                "processing": '<button class="btn btn-transparent" type="button" disabled><span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>Memuat...</button>'
            },
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "orderable": false
        });

        $('#searchmaterialTabel').on('keydown', function(e) {
            materialTabel.search($(this).val()).draw();
        });

        function tutupExportModal() {
            Swal.fire({
                text: "Yakin ingin menutup ?",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                confirmButtonText: "Ya, tutup!",
                cancelButtonText: "Tidak, batalkan.",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light",
                },
            }).then(function(t) {
                t.value ?
                    ($('#kt_items_export_modal').modal('hide')) :
                    "cancel" === t.dismiss &&
                    Swal.fire({
                        text: "Tidak jadi menutup!.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok!",
                        customClass: {
                            confirmButton: "btn btn-primary",
                        },
                    });
            });
        }

        function exportList() {
            Swal.fire({
                text: "Export data format " + $('#formatExport').text() + ' ?',
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                confirmButtonText: "Ya, Export!",
                cancelButtonText: "Tidak, batalkan.",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light",
                },
            }).then(function(t) {
                t.value ?
                    ($('#kt_items_export_modal').modal('hide')) :
                    "cancel" === t.dismiss &&
                    Swal.fire({
                        text: "Tidak jadi!.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok!",
                        customClass: {
                            confirmButton: "btn btn-primary",
                        },
                    });
            });
        }
    </script>
@endsection
