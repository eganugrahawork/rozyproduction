@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Tambah Penjualan
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="storePenjualan">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Penjualan" type="text" name="kode" id="kode"
                                autocomplete="off" value="{{ $isSequence }}" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Media Penjualan</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Media" id="media_id" name="media_id">
                                <option></option>
                                @foreach ($isMedia as $item)
                                    <option value="{{ $item->id }}">{{ $item->media }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Tanggal</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="tanggal" id="tanggal"
                                autocomplete="off" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Shipper</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Shipper" id="shipper_id" name="shipper_id" >
                                <option></option>
                                @foreach ($shipper as $item)
                                    <option value="{{ $item->id }}">{{ $item->kode }} - {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-solid fs-7" name="keterangan"></textarea>
                        </div>
                    </div>
                </div>
                <div class="pt-2">
                    <span class="text-gray-600 fw-bolder fs-4">Detail</span>
                    <br id="garisBaruDetail">
                    <table class="table align-middle  gy-5">
                        <thead>
                            <tr class="fs-6 fw-bolder text-gray-600 text-center">
                                <th class="min-w-200px">Produk</th>
                                <th class="min-w-200px">Varian</th>
                                <th class="min-w-25px">Stok</th>
                                <th class="min-w-25px">Qty</th>
                                <th class="min-w-100px">Harga</th>
                                <th class="min-w-100px">Jumlah</th>
                                <th class="min-w-50px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="tableItems">
                    
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-end" colspan="7"> <button class="btn btn-sm btn-success"
                                        type="button" onclick="addRowVariant()">+</button></th>
                            </tr>
                            <tr class="fw-bolder fs-7">
                                <th class="text-end" colspan="5">Jumlah Quantity</th>
                                <th colspan="2"><input type="text"
                                        class="form-control text-end form-control-white fs-7 text-dark" readonly
                                        id="totalQty"/></th>
                            </tr>
                            <tr class="fw-bolder fs-7">
                                <th class="text-end" colspan="5">Total</th>
                                <th colspan="2"><input type="text"
                                        class="form-control text-end form-control-white fs-7 text-dark" readonly
                                        id="grandTotal" /></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="table-responsive p-2" id="detailHere">
                  
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" id="btnSubmit" class="btn btn-sm btn-primary" onclick="store()">Tambah</button>
                    </div>
                    <div class="p-2">
                        <a href="/admin/penjualan" class="btn btn-sm btn-secondary">Batal</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/penjualan/penjualan/create.js"></script> 
@endsection
