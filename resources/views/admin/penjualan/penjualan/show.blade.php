@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                {{ $isConfirm == 1 ? 'Konfirmasi ' : 'Info ' }} Penjualan
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="confirmPenjualan">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <input type="hidden" name="id" id="id" value="{{ $isPenjualan[0]->id }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Penjualan" type="text" name="kode" id="kode"
                                autocomplete="off" value="{{ $isPenjualan[0]->kode }}" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Media Penjualan</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Media" id="media_id" name="media_id" disabled>
                                <option></option>
                                @foreach ($isMedia as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $item->id == $isPenjualan[0]->media_id ? 'selected' : '' }}>{{ $item->media }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Tanggal</label>
                            <input class="form-control form-control-white fs-7" type="text" name="tanggal" id="tanggal"
                                autocomplete="off" value="{{ $isPenjualan[0]->tanggal }}" disabled />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Shipper</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Shipper" id="shipper_id" name="shipper_id" disabled>
                                <option></option>
                                @foreach ($shipper as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isPenjualan[0]->shipper_id == $item->id ? 'selected' : '' }}>{{ $item->kode }}
                                        - {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-white fs-7" name="keterangan" readonly>{{ $isPenjualan[0]->keterangan }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="pt-2">
                    <span class="text-gray-600 fw-bolder fs-4">Detail</span>
                    <br id="garisBaruDetail">
                    <table class="table align-middle  gy-5">
                        <thead>
                            <tr class="fs-6 fw-bolder text-gray-600 text-center">
                                <th class="min-w-200px">Produk</th>
                                <th class="min-w-200px">Varian</th>
                                <th class="min-w-25px">Qty</th>
                                <th class="min-w-25px">Stok Qty</th>
                                <th class="min-w-100px">Harga</th>
                                <th class="min-w-100px">Jumlah</th>
                                <th class="min-w-50px">-</th>
                            </tr>
                        </thead>
                        <tbody id="tableItems">
                            @foreach ($isPenjualan as $penj)
                                <tr>
                                    <td class="fv-row">
                                        <input type="hidden" id="penjualan_detail_id"
                                            value="{{ $penj->penjualan_detail_id }}">
                                        <select class="form-select form-select-white fs-7 select-2-id"
                                            data-control="select2" data-placeholder="Pilih Produk" id="produk_id"
                                            onchange="getVarian(this)" disabled>
                                            <option></option>
                                            @foreach ($produk as $item)
                                                <option
                                                    value="{{ $item->id . ',' . $item->harga_jual . ',' . $item->harga_produk_id }}"
                                                    {{ $item->id . ',' . $item->harga_jual . ',' . $item->harga_produk_id == $penj->produk_id ? 'selected' : '' }}>
                                                    {{ $item->nama }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="fv-row">
                                        @php
                                            $produk_id = explode(',', $penj->produk_id);

                                            $produkDetail = App\Models\ProdukDetail::where(
                                                'produk_id',
                                                $produk_id[0],
                                            )->get();
                                        @endphp
                                        <select class="form-select form-select-white fs-7 select-2-id detail-form"
                                            data-control="select2" data-placeholder="Pilih Varian" id="varian_id" disabled>
                                            <option></option>
                                            @foreach ($produkDetail as $item)
                                                <option value="{{ $item->id }}"
                                                    {{ $penj->produk_detail_id == $item->id ? 'selected' : '' }}>
                                                    {{ $item->nama }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="fv-row">
                                        <input type="hidden" id="stok_qty_parent" />
                                        <input class="form-control form-control-white text-end fs-7" type="number" id="stok_qty" data-bs-toggle="tooltip" data-bs-placement="top" title="Sisa stok setelah dikurangi qty penjualan ini."
                                            autocomplete="off" readonly/>
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-white text-end fs-7" type="number"
                                            id="qty" autocomplete="off" value="{{ $penj->qty }}"
                                            onkeyup="countTotal(this)" readonly />
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-white text-end fs-7" type="text"
                                            id="price" autocomplete="off" value="{{ $penj->harga }}" readonly />
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-white text-end fs-7" type="text"
                                            id="total" autocomplete="off" readonly />
                                    </td>
                                    <td class="fv-row text-center"> -</td>

                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr class="fw-bolder fs-7">
                                <th class="text-end" colspan="4">Jumlah Quantity</th>
                                <th colspan="2"><input type="text"
                                        class="form-control text-end form-control-white fs-7 text-dark" readonly
                                        id="totalQty" /></th>
                            </tr>
                            <tr class="fw-bolder fs-7">
                                <th class="text-end" colspan="4">Total</th>
                                <th colspan="2"><input type="text"
                                        class="form-control text-end form-control-white fs-7 text-dark" readonly
                                        id="grandTotal" /></th>
                            </tr>
                            @if ($isConfirm !== 0)
                                @if ($isConfirm == 1)
                                <tr class="fw-bolder fs-7">
                                    <th class="text-end align-center" colspan="4">Tanggal Konfirmasi</th>
                                    <th colspan="2 align-center">
                                        <div class="fv-row">
                                            <input type="text"
                                                class="form-control text-end form-control-solid fs-7 text-dark"
                                                id="tanggal_pic" name="tanggal_pic" />
                                        </div>
                                    </th>
                                </tr>
                                @else
                                <tr class="fw-bolder fs-7">
                                    <th class="text-end align-center" colspan="4">Tanggal Konfirmasi</th>
                                    <th colspan="2 align-center">{{ $isPenjualan[0]->tanggal_pic }}
                                    </th>
                                </tr>
                                <tr class="fw-bolder fs-7">
                                    <th class="text-end align-center" colspan="4">PIC</th>
                                    <th colspan="2 align-center">{{ $isPenjualan[0]->pic }}
                                    </th>
                                </tr>
                                @endif
                               
                            @endif
                        </tfoot>
                    </table>
                </div>
                <div class="table-responsive p-2" id="detailHere">

                </div>
                <div class="d-flex justify-content-center p-2">
                    @if ($isConfirm == 1)
                        <div class="p-2">
                            <button type="button" class="btn btn-sm btn-primary" onclick="confirm(1)">Setujui</button>
                        </div>
                        <div class="p-2">
                            <button type="button" class="btn btn-sm btn-danger" onclick="confirm(2)">Tolak</button>
                        </div>
                </div>
                @endif
                <div class="d-flex justify-content-center">
                    <a href="/admin/penjualan" class="btn btn-sm btn-secondary">Kembali</a>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/penjualan/penjualan/show.js"></script>
@endsection
