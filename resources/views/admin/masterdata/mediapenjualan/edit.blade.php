@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Ubah Media Penjualan
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="updateMedia">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <input type="hidden" name="id" id="id" value="{{ $isMedia->id }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Kategori</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2" data-placeholder="Pilih Kategori" name="kategori_id">
                                <option></option>
                                @foreach ($isKategori as $item)
                                <option value="{{$item->id}}" {{ $isMedia->kategori_media_penjualan_id == $item->id ? 'selected' : '' }}>{{ $item->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Nama</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="nama" id="nama"
                                autocomplete="off" value="{{ $isMedia->nama }}"/>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Email</label>
                            <input class="form-control form-control-solid fs-7" type="email" name="email" id="email"
                                autocomplete="off" value="{{ $isMedia->email }}"/>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">No. Hp</label>
                            <input class="form-control form-control-solid fs-7" type="number" name="no_hp" id="no_hp"
                                autocomplete="off" value="{{ $isMedia->no_hp }}"/>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-solid fs-7" name="keterangan">{{ $isMedia->keterangan }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="update()">Ubah</button>
                    </div>
                    <div class="p-2">
                        <a href="/admin/masterdata/media-penjualan" class="btn btn-sm btn-secondary">Batal</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/masterdata/mediapenjualan/edit.js"></script>
@endsection
