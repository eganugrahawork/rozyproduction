@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Tambah Partner
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="storePartner">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Kategori</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2" data-placeholder="Pilih Kategori" name="kategori_id">
                                <option></option>
                                @foreach ($isKategori as $item)
                                <option value="{{$item->id}}">{{ $item->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-solid fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Partner" type="text" name="kode" id="kode"
                                autocomplete="off" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Nama</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="nama" id="nama"
                                autocomplete="off" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">No. Hp</label>
                            <input class="form-control form-control-solid fs-7" type="number" name="no_telp" id="no_telp"
                                autocomplete="off" />
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Alamat </label>
                            <textarea class="form-control form-control-solid fs-7" name="alamat"></textarea>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-solid fs-7" name="keterangan"></textarea>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="store()">Tambah</button>
                    </div>
                    <div class="p-2">
                        <a href="/admin/masterdata/partner" class="btn btn-sm btn-secondary">Batal</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/masterdata/partner/create.js"></script>
@endsection
