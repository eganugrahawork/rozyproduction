<div class="card">
    <div class="card-header border-0 pt-6">
        <div class="card-title fw-bolder">
            Tambah Kategori Partner
        </div>
    </div>
    <div class="card-body ">
            <form action="#" id="storeKategori">
                @csrf
                <div class="row">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Nama</label>
                            <input class="form-control form-control-solid" type="text" name="nama" autocomplete="off" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span class="text-gray-400 fw-bolder fs-8">(Opsional)</span></label>
                            <textarea class="form-control form-control-solid"  name="keterangan" ></textarea>
                        </div>
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="store()">Tambah</button>
                    </div>
                    <div class="p-2">
                        <button type="button" onclick="tutupModalTambah()" class="btn btn-sm btn-secondary">Batal</button>
                    </div>
                </div>
            </form>
    </div>
</div>

<script src="/js/admin/masterdata/partner/kategori/create.js"></script>
