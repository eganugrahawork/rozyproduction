@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Ubah Uom
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="updateUom">
                @csrf
                <div class="d-flex justify-content-center">
                    <div class="col-lg-6">
                        <div class="fv-row mb-7">
                            <input type="hidden" name="id" id="id" value="{{ $isUom->id }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Nama</label>
                            <input class="form-control form-control-solid fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Nama Uom" type="text" value="{{ $isUom->nama }}" name="nama" id="nama"
                                autocomplete="off" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Simbol</label>
                            <input class="form-control form-control-solid fs-7" type="text" value="{{ $isUom->simbol }}" name="simbol" id="simbol"
                                autocomplete="off" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-solid fs-7" name="keterangan">{{ $isUom->keterangan }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="update()">Ubah</button>
                    </div>
                    <div class="p-2">
                        <a href="/admin/masterdata/uom" class="btn btn-sm btn-secondary">Batal</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/masterdata/uom/edit.js"></script>
@endsection
