@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Info Material
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="updateMaterial">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Jenis Material</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Jenis" name="jenis_material_id" disabled>
                                <option></option>
                                @foreach ($isJenis as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isMaterial[0]->jenis_material_id == $item->id ? 'selected' : '' }}>
                                        {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Uom</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Uom" name="uom_id" id="uom_id" disabled>
                                <option></option>
                                @foreach ($isUom as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isMaterial[0]->uom_id == $item->id ? 'selected' : '' }}>{{ $item->nama }}
                                        ({{ $item->simbol }})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <input type="hidden" name="id" id="id" value="{{ $isMaterial[0]->id }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Material" type="text" name="kode" id="kode"
                                value="{{ $isMaterial[0]->kode }}" autocomplete="off" readonly />
                            <input type="hidden" id="kode_sebelumnya" value="{{ $isMaterial[0]->kode }}">
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Nama</label>
                            <input type="hidden" id="nama_sebelumnya" value="{{ $isMaterial[0]->nama }}">
                            <input class="form-control form-control-white fs-7" type="text" name="nama" id="nama"
                                value="{{ $isMaterial[0]->nama }}" autocomplete="off" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-white fs-7" name="keterangan" readonly>{{ $isMaterial[0]->keterangan }}</textarea>
                        </div>
                    </div>
                    <div class="col-lg">

                        <div class="mb-5">
                            <label class="form-label fs-5 fw-bolder text-dark">Detail Opsi</label>
                        </div>
                        <div class="landing-dark-separator p-4"></div>
                        <div id="varianDisini">
                            @foreach ($isMaterial as $item)
                                <div class="row mb-3">
                                    <div class="col-lg-4">
                                        <div class="fv-row mb-2">
                                            <input type="hidden" id="material_detail_id"
                                                value="{{ $item->material_detail_id }}">
                                            <label class="form-label fs-7 fw-bolder text-dark">Varian</label>
                                            <input class="form-control form-control-white fs-7 is-varian" type="text"
                                                autocomplete="off" id="varian" value="{{ $item->warna }}" readonly />
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="fv-row">
                                            <label class="form-label fs-7 fw-bolder text-dark">Stok Awal</label>
                                            <input class="form-control form-control-white text-end fs-7 stok-awal"
                                                type="number" autocomplete="off" id="stok_awal"
                                                value="{{ $item->stok_awal }}" readonly />
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="fv-row">
                                            <label class="form-label fs-7 fw-bolder text-dark">Harga</label>
                                            <input class="form-control form-control-white text-end fs-7" type="text"
                                                autocomplete="off" id="harga_stok_awal" readonly  value="{{ $item->harga }}"  />
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <a href="/admin/masterdata/material" class="btn btn-sm btn-secondary">Batal</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/masterdata/material/edit.js"></script>
@endsection
