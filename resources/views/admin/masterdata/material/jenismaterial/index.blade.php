<div class="card">
    <!--begin::Card header-->
    <div class="card-header border-0 pt-6">
        <!--begin::Card title-->
        <div class="card-title">
            <!--begin::Search-->
            <div class="d-flex align-items-center position-relative my-1">
                <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                <span class="svg-icon svg-icon-1 position-absolute ms-6">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1"
                            transform="rotate(45 17.0365 15.1223)" fill="black" />
                        <path
                            d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                            fill="black" />
                    </svg>
                </span>
                <!--end::Svg Icon-->
                <input type="text" id="searchjenisMaterialTable"
                    class="form-control form-control-sm form-control-solid w-250px ps-15" placeholder="Pencarian.." />
            </div>
            <!--end::Search-->
        </div>
        <!--begin::Card title-->
        <!--begin::Card toolbar-->
        <div class="card-toolbar">
            <!--begin::Toolbar-->
            <div class="d-flex justify-content-end">
                <button onclick="modalTambah()" class="btn btn-sm btn-primary">Tambah
                    Jenis</button>
            </div>
            <!--end::Toolbar-->
        </div>
        <!--end::Card toolbar-->
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body pt-0">
        <!--begin::Table-->
        <div class="table-responsive">

            <table class="table align-middle table-row-dashed fs-8 gy-5" id="jenisMaterialTable">
                <!--begin::Table head-->
                <thead>
                    <!--begin::Table row-->
                    <tr class="text-start text-gray-400 fw-bolder fs-8 text-uppercase gs-0">
                        <th class="min-w-50px">No</th>
                        <th class="min-w-125px">Nama</th>
                        <th class="min-w-125px">Keterangan</th>
                        <th class="text-end min-w-70px">Aksi</th>
                    </tr>
                    <!--end::Table row-->
                </thead>
                <!--end::Table head-->
                <!--begin::Table body-->
                <tbody class="fw-bold text-gray-600">
                </tbody>
                <!--end::Table body-->
            </table>

        </div>
        <!--end::Table-->
    </div>
    <!--end::Card body-->
    <div class="card-footer">
        <div class="d-flex justify-content-end ">
            <button type="button" class="btn btn-secondary btn-sm" onclick="tutupModal()">Keluar</button>
        </div>
    </div>
</div>
<!--end::Card-->
<!--begin::Modals-->
<div class="modal fade" id="modalTipeBarang" tabindex="-1" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <!--begin::Modal content-->
        <div class="modal-content" id="contentModalTipeBarang">
            <!--begin::Form-->

            <!--end::Form-->
        </div>
    </div>
</div>
<!--end::Modal - Customers - Add-->


<script>
    var jenisMaterialTable = $('#jenisMaterialTable').DataTable({
        processing: true,
        serverSide: true,
        searching: true,
        // ordering: false,
        pageLength: 5,
        ajax: {
            "url": "/admin/masterdata/jenis-material/lists",
            "error": function(response) {
                Swal.fire(
                    'Error',
                    response.statusText,
                    'warning'
                )
            }
        },
        drawCallback: function(settings) {
            KTMenu.createInstances();
            var api = this.api();
            if (api.data().length === 0 && !api.page.info().recordsTotal) {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                var cari = $('#searchjenisMaterialTable').val();
                toastr.warning("Data '" + cari + "' yang anda dicari tidak ditemukan!");
            }
        },
        columns: [{
                data: 'DT_RowIndex',
                searchable: false
            },
            {
                data: 'nama',
                name: 'nama'
            },
            {
                data: 'keterangan',
                name: 'keterangan'
            },
            {
                data: 'action',
                name: 'action',
                className: 'text-end'
            }
        ],
        "language": {
            "processing": '<button class="btn btn-transparent fs-8" type="button" disabled><span class="spinner-grow spinner-grow-sm fs-8" role="status" aria-hidden="true"></span>Memuat...</button>'
        },
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
    });

    $('#searchjenisMaterialTable').on('keydown', function(e) {
            jenisMaterialTable.search($(this).val()).draw();
    });

    function tutupModal() {
        $('#thisModalHere').modal('hide');
    }


    function modalTambah() {
        $.ajax({
            url: "/admin/masterdata/jenis-material/create",
            method: "get",
            success: function(view) {
                $("#modalTipeBarang").modal('show')
                $("#contentModalTipeBarang").html(view)
            },
            error: function() {

            }
        })
    }

    function modalEdit(id) {
        $.ajax({
            url: "/admin/masterdata/jenis-material/edit/"+id,
            method: "get",
            success: function(view) {
                $("#modalTipeBarang").modal('show')
                $("#contentModalTipeBarang").html(view)
            },
            error: function() {

            }
        })

    }

    function deleteDataJenis(id, nama) {

Swal.fire({
    text: "Hapus data " + nama + " ?",
    icon: "warning",
    showCancelButton: !0,
    buttonsStyling: !1,
    confirmButtonText: "Ya, Hapus!",
    cancelButtonText: "Tidak, batalkan.",
    customClass: {
        confirmButton: "btn btn-primary",
        cancelButton: "btn btn-active-light",
    },
}).then(function(t) {
    t.value ?
        $.ajax({
            url: "/admin/masterdata/jenis-material/" + id,
            method: "delete",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                if (response.success) {
                    Swal.fire({
                        text: "Berhasil menghapus Jenis Material!",
                        icon: "success",
                        buttonsStyling: !1,
                        confirmButtonText: "Lanjutkan !",
                        customClass: {
                            confirmButton: "btn btn-primary",
                        },
                    }).then(function(t) {
                        t.isConfirmed &&
                        jenisMaterialTable.ajax.reload(null, false);
                    });
                } else {
                    Swal.fire(
                        response.errors,
                        'Informasi lebih lanjut hubungi team.',
                        'error'
                    )
                }
            },
            error: function(xhr, status, error) {
                if (xhr.status === 422) {
                    var errors = xhr.responseJSON.errors;
                    Swal.fire(
                        'Peringatan',
                        errors[0],
                        'warning'
                    )
                } else {
                    Swal.fire(
                        'Error',
                        'Terjadi kesalahan: ' + status + ' ' + error,
                        'error'
                    )
                }
            }
        }) :
        "cancel" === t.dismiss &&
        Swal.fire({
            text: "Hapus data " + nama + " dibatalkan!.",
            icon: "error",
            buttonsStyling: !1,
            confirmButtonText: "Ok!",
            customClass: {
                confirmButton: "btn btn-primary",
            },
        });
});
}
</script>
