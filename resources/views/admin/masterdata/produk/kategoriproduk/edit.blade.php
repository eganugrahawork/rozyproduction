<div class="card">
    <div class="card-header border-0 pt-6">
        <div class="card-title fw-bolder">
            Ubah Kategori Produk
        </div>
    </div>
    <div class="card-body ">
            <form action="#" id="updateKategori">
                @csrf
                <div class="row">
                        <div class="fv-row mb-7">
                            <input type="hidden" id="id" name="id" value="{{$isKategori->id}}">
                            <label class="form-label fs-7 fw-bolder text-dark">Nama</label>
                            <input class="form-control form-control-solid" type="text" name="nama" value="{{$isKategori->nama}}" autocomplete="off" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span class="text-gray-400 fw-bolder fs-8">(Opsional)</span></label>
                            <textarea class="form-control form-control-solid"  name="keterangan" >{{$isKategori->keterangan}}</textarea>
                        </div>
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="update()">Ubah</button>
                    </div>
                    <div class="p-2">
                        <button type="button" onclick="tutupModalTambah()" class="btn btn-sm btn-secondary">Batal</button>
                    </div>
                </div>
            </form>
    </div>
</div>

<script src="/js/admin/masterdata/produk/kategori/edit.js"></script>
