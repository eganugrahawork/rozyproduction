@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Ubah Produk
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="updateProduk">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <input type="hidden" name="id" id="id" value="{{ $isProduk[0]->id }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Kategori</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Kategori" name="kategori_id">
                                <option></option>
                                @foreach ($isKategori as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isProduk[0]->kategori_produk_id == $item->id ? 'selected' : '' }}>
                                        {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-solid fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Partner" type="text" name="kode" id="kode"
                                autocomplete="off" value="{{ $isProduk[0]->kode }}" />

                            <input type="hidden" id="kode_sebelumnya" value="{{ $isProduk[0]->kode }}">
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Nama</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="nama" id="nama"
                                autocomplete="off"value="{{ $isProduk[0]->nama }}" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Bahan</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Bahan" name="material_id">
                                <option></option>
                                @foreach ($isMaterial as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isProduk[0]->material_id == $item->id ? 'selected' : '' }}>{{ $item->nama }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-solid fs-7" name="keterangan">{{ $isProduk[0]->keterangan }}</textarea>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Number Of Cutting Requirement</label>
                            <input class="form-control form-control-solid fs-7" type="number" name="number_cutting_require"
                                id="number_cutting_require" autocomplete="off"
                                value="{{ $isProduk[0]->number_cutting_require }}" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Harga Pokok Produk</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="hpp" id="hpp"
                                autocomplete="off" value="{{ $isProduk[0]->hpp }}" />
                        </div>
                        <div class="mb-5">
                            <label class="form-label fs-5 fw-bolder text-dark">Tambahkan Opsi</label>
                        </div>
                        <div class="landing-dark-separator p-4"></div>
                        <div id="varianDisini">
                            @foreach ($isProduk as $item)
                                <div class="row mb-3">
                                    <div class="col-lg-6">
                                        <div class="fv-row mb-2">
                                            <input type="hidden" id="produk_detail_id" value="{{ $item->produk_detail_id }}">
                                            @if (isset($item->id_history))
                                            <input type="hidden" id="id_history" value="{{$item->id_history}}">
                                            @endif
                                            <label class="form-label fs-7 fw-bolder text-dark">Varian</label>
                                            <input class="form-control  {{ isset($item->id_history) ? 'form-control-white' : 'form-control-solid'}} fs-7 is-varian" type="text"
                                                autocomplete="off" id="varian" value="{{ $item->varian }}" {{ isset($item->id_history) ? 'readonly' : '' }}/>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="fv-row">
                                            <label class="form-label fs-7 fw-bolder text-dark">Stok Awal</label>
                                            <input class="form-control  {{ isset($item->id_history) ? 'form-control-white' : 'form-control-solid'}} text-end fs-7 stok-awal"
                                                type="number" autocomplete="off" id="stok_awal"value="{{ $item->stok_awal }}" {{ isset($item->id_history) ? 'readonly' : '' }}/>
                                        </div>
                                    </div>
                                    @if (!isset($item->id_history))
                                    <div class="col-lg-2 d-flex align-items-center justify-content-end">
                                        <button class="btn btn-sm btn-danger" data-bs-toggle="tooltip"
                                            data-bs-placement="top" title="Hapus Varian" type="button"
                                            onclick="removeRowVariant(this)">-</button>
                                    </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                        <div class="d-flex justify-content-end mt-4">
                            <button class="btn btn-sm btn-warning" type="button" onclick="addRowVariant()">+</button>
                        </div>
                    </div>
                </div>
        <div class="d-flex justify-content-center p-2">
            <div class="p-2">
                <button type="button" class="btn btn-sm btn-primary" onclick="update()">Ubah</button>
            </div>
            <div class="p-2">
                <a href="/admin/masterdata/produk" class="btn btn-sm btn-secondary">Batal</a>
            </div>
        </div>
        </form>
    </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/masterdata/produk/edit.js"></script>
@endsection
