@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Info Produk
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="updateProduk">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <input type="hidden" name="id" id="id" value="{{ $isProduk[0]->id }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Kategori</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Kategori" name="kategori_id" disabled>
                                <option></option>
                                @foreach ($isKategori as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isProduk[0]->kategori_produk_id == $item->id ? 'selected' : '' }}>
                                        {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Partner" type="text" name="kode" id="kode"
                                autocomplete="off" value="{{ $isProduk[0]->kode }}" readonly/>

                            <input type="hidden" id="kode_sebelumnya" value="{{ $isProduk[0]->kode }}">
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Nama</label>
                            <input class="form-control form-control-white fs-7" type="text" name="nama" id="nama"
                                autocomplete="off"value="{{ $isProduk[0]->nama }}" readonly/>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Bahan</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Bahan" name="material_id" disabled>
                                <option></option>
                                @foreach ($isMaterial as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isProduk[0]->material_id == $item->id ? 'selected' : '' }}>{{ $item->nama }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-white fs-7" name="keterangan" readonly>{{ $isProduk[0]->keterangan }}</textarea>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Number Of Cutting Requirement</label>
                            <input class="form-control form-control-white fs-7" type="number" name="number_cutting_require"
                                id="number_cutting_require" autocomplete="off"
                                value="{{ $isProduk[0]->number_cutting_require }}" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Harga Pokok Produk</label>
                            <input class="form-control form-control-white fs-7" type="text" name="hpp" id="hpp"
                                autocomplete="off" value="{{ $isProduk[0]->hpp }}" readonly />
                        </div>
                        <div class="mb-5">
                            <label class="form-label fs-5 fw-bolder text-dark">Tambahkan Opsi</label>
                        </div>
                        <div class="landing-dark-separator p-4"></div>
                        <div id="varianDisini">
                            @foreach ($isProduk as $item)
                                <div class="row mb-3">
                                    <div class="col-lg-6">
                                        <div class="fv-row mb-2">
                                            <input type="hidden" id="produk_detail_id" value="{{ $item->produk_detail_id }}">
                                            <label class="form-label fs-7 fw-bolder text-dark">Varian</label>
                                            <input class="form-control form-control-white fs-7 is-varian" type="text"
                                                autocomplete="off" id="varian" value="{{ $item->varian }}" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="fv-row">
                                            <label class="form-label fs-7 fw-bolder text-dark">Stok Awal</label>
                                            <input class="form-control form-control-white text-end fs-7 stok-awal"
                                                type="number" autocomplete="off" id="stok_awal"value="{{ $item->stok_awal }}" readonly/>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
        <div class="d-flex justify-content-center p-2">
            <div class="p-2">
                <a href="/admin/masterdata/produk" class="btn btn-sm btn-secondary">Batal</a>
            </div>
        </div>
        </form>
    </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/masterdata/produk/edit.js"></script>
@endsection
