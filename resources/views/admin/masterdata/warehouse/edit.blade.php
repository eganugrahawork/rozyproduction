@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Ubah Warehouse
            </div>
        </div>
        <div class="card-body">
            <form action="#" id="updateWarehouse">
                @csrf
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-6">
                        <div class="fv-row mb-7">
                         <input class="form-control form-control-solid fs-7" type="hidden" name="id" id="id"
                                autocomplete="off" value="{{ $isWarehouse[0]->id}}" />
                            <label class="form-label fs-7 fw-bolder text-dark">Nama</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="nama" id="nama"
                                autocomplete="off" value="{{ $isWarehouse[0]->nama}}"/>
                        </div>
                            <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Alamat</label>
                            <textarea class="form-control form-control-solid fs-7" name="alamat">{{$isWarehouse[0]->alamat}}</textarea>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode Pos</label>
                            <input class="form-control form-control-solid fs-7" type="number" name="kode_pos" id="kode_pos"
                                autocomplete="off" value="{{ $isWarehouse[0]->kode_pos}}"/>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">PIC</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih PIC" name="pic_id">
                                <option></option>
                                @foreach ($partner as $item)
                                    <option value="{{ $item->id }}" {{$isWarehouse[0]->pic_id == $item->id ? 'selected' : ''}}>{{ $item->kode . '-' . $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    
                    </div>
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="update()">Ubah</button>
                    </div>
                    <div class="p-2">
                        <a href="/admin/masterdata/warehouse" class="btn btn-sm btn-secondary">Batal</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/masterdata/warehouse/edit.js"></script>
@endsection
