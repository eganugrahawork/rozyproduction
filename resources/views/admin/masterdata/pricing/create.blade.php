@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Atur Pricing
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="storePricing">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Produk</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2" data-placeholder="Pilih Produk" name="produk_id" id="produk_id">
                                <option></option>
                                @foreach ($isProduk as $item)
                                <option value="{{$item->id}}">{{ $item->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Kategori</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2" data-placeholder="Pilih Kategori Media Penjualan" name="kategori_media_penjualan_id" onchange="getMedia(this)">
                                <option></option>
                                @foreach ($isKategori as $item)
                                <option value="{{$item->id}}">{{ $item->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Media Penjualan</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2" data-placeholder="Pilih Media Penjualan" name="media_penjualan_id" id="media_penjualan_id">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Harga Pokok Produk</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="hpp" id="hpp"
                                autocomplete="off" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Harga Jual</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="harga_jual" id="harga_jual"
                                autocomplete="off" />
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="store()">Tambah</button>
                    </div>
                    <div class="p-2">
                        <a href="/admin/masterdata/pricing" class="btn btn-sm btn-secondary">Batal</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')s
    <script src="/js/admin/masterdata/pricing/create.js"></script>
@endsection
