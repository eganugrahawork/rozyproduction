@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Ubah Chart of Account
            </div>
        </div>
        <div class="card-body">
            <form action="#" id="updateCoa">
                @csrf
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-6">
                        <div class="fv-row mb-7">
                            <input type="hidden" name="id" id="id" value="{{ $isCoa->id }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Parent</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2" data-placeholder="Pilih Parent" name="parent_id">
                                <option></option>
                                <option value="0" {{ $isCoa->parent_id == 0 ?'selected' : '' }}>Parent</option>
                                @foreach ($parentCoa as $item)
                                <option value="{{$item->id}}" {{$isCoa->parent_id == $item->id ? 'selected' : ''}}>{{ $item->kode .'-'.$item->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <input type="hidden" name="current_code" value="{{ $isCoa->kode }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-solid fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Coa" type="text" name="kode" id="kode"
                                autocomplete="off" value="{{ $isCoa->kode }}"/>
                        </div>
                        <div class="fv-row mb-7">
                            <input type="hidden" name="current_name" value="{{ $isCoa->nama }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Nama</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="nama" id="nama"
                                autocomplete="off" value="{{ $isCoa->nama }}" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Adjustment</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2" data-placeholder="Pilih Adjustment" name="adjustment">
                                <option></option>
                                <option value="0" {{ $isCoa->adjustment == 0 ? 'selected' : '' }}>Debit</option>
                                <option value="1" {{ $isCoa->adjustment == 1 ? 'selected' : '' }}>Kredit</option>
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-solid fs-7" name="keterangan">{{ $isCoa->keterangan }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="update()">Ubah</button>
                    </div>
                    <div class="p-2">
                        <a href="/admin/masterdata/coa" class="btn btn-sm btn-secondary">Batal</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/masterdata/coa/edit.js"></script>
@endsection
