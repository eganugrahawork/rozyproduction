@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Ubah Finishing
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="updateFinishing">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <input type="hidden" name="id" value="{{ $isFinishing[0]->id }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Pengadaan" type="text" name="kode" id="kode"
                                autocomplete="off" value="{{ $isFinishing[0]->kode }}" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Partner</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Partner" id="partner_id" name="partner_id"
                                onchange="getDataPartner()">
                                <option></option>
                                @foreach ($isPartner as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isFinishing[0]->partner_id == $item->id ? 'selected' : '' }}>{{ $item->kode }} -
                                        {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">No. Telp</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="No Telepon Partner" type="text" id="no_telp"
                                autocomplete="off" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Alamat</label>
                            <textarea class="form-control form-control-white fs-7" id="alamat" readonly></textarea>
                        </div>

                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Tanggal</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="tanggal" id="tanggal"
                                autocomplete="off" value="{{ $isFinishing[0]->tanggal }}" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Pengantar</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Pengantar" id="pengantar_id" name="pengantar_id">
                                <option></option>
                                @foreach ($pengantar as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isFinishing[0]->pengantar == $item->id ? 'selected' : '' }}>{{ $item->kode }} -
                                        {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-solid fs-7" name="keterangan">{{ $isFinishing[0]->keterangan }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="pt-2">
                    <span class="text-gray-600 fw-bolder fs-4">Detail</span>
                    <br id="garisBaruDetail">
                    <table class="table align-middle  gy-5">
                        <thead>
                            <tr class="fs-6 fw-bolder text-gray-600 text-center">
                                <th class="min-w-200px">Section</th>
                                <th class="min-w-50px">Qty</th>
                                <th class="min-w-100px">Harga</th>
                                <th class="min-w-100px">Jumlah</th>
                                <th class="min-w-50px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="tableItems">
                            @foreach ($isFinishing as $finishing)
                                <tr>
                                    <td class="fv-row">
                                        <input type="hidden" id="finishing_detail_id"
                                            value="{{ $finishing->finishing_detail_id }}">
                                        <select class="form-select form-select-solid fs-7 select-2-id detail-form"
                                            data-control="select2" data-placeholder="Pilih Finishing Detail"
                                            id="sewing_detail_id" onchange="getCuttingDetail(this)">
                                            @foreach ($sewingDetail as $item)
                                                <option value="{{ $item->id }}" {{ $finishing->sewing_detail_id == $item->id ? 'selected' : '' }}>{{ $item->partner. ' | '.$item->produk_jadi. ' : '.$item->nama . '-'.$item->nama_detail }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-white text-center fw-bolder fs-7"
                                            type="text" id="qty" autocomplete="off" readonly
                                            value="{{ $finishing->produk_jadi }}" />
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-solid text-end fs-7" type="text"
                                            id="price" autocomplete="off" value="{{ $finishing->harga }}"
                                            onkeyup="countTotal()" />
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-white text-end fs-7" type="text"
                                            id="jumlah" autocomplete="off" value="{{number_format($finishing->total_harga, 2) }}"
                                            readonly />
                                    </td>
                                    <td class="fv-row text-center"> <button class="btn btn-sm btn-danger"
                                            data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus detail"
                                            type="button" onclick="removeRowVariant(this)">-</button></td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-end" colspan="5"> <button class="btn btn-sm btn-success"
                                        type="button" onclick="addRowVariant()">+</button></th>
                            </tr>
                            <tr class="fw-bolder fs-7">
                                <th class="text-end" colspan="3">Total</th>
                                <th colspan="2"><input type="text"
                                        class="form-control text-end form-control-white fs-7 text-dark" readonly
                                        id="total" /></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="table-responsive p-2" id="detailHere">

                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="update()">Ubah</button>
                    </div>
                    <div class="p-2">
                        <a href="/admin/produksi/finishing" class="btn btn-sm btn-secondary">Kembali</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/produksi/finishing/edit.js"></script>
@endsection
