@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Ubah Cutting
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="updateCutting">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <input type="hidden" name="id" id="id" value="{{ $isCutting[0]->id }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Pengadaan" type="text" name="kode" id="kode"
                                autocomplete="off" value="{{ $isCutting[0]->kode }}" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Partner</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Partner" id="partner_id" name="partner_id"
                                onchange="getDataPartner()">
                                <option></option>
                                @foreach ($isPartner as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isCutting[0]->partner_id == $item->id ? 'selected' : '' }}>{{ $item->kode }} -
                                        {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">No. Telp</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="No Telepon Partner" type="text" id="no_telp"
                                autocomplete="off" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Alamat</label>
                            <textarea class="form-control form-control-white fs-7" id="alamat" readonly></textarea>
                        </div>

                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Pengadaan</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Pengadaan" id="pengadaan_id" name="pengadaan_id"
                                onchange="getPengadaan()">
                                <option></option>
                                @foreach ($isPengadaan as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isCutting[0]->pengadaan_id == $item->id ? 'selected' : '' }}>
                                        {{ \Carbon\Carbon::parse($item->tanggal)->format('d-m-Y') }} - {{ $item->kode }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Tanggal</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="tanggal" id="tanggal"
                                value="{{ $isCutting[0]->tanggal }}" autocomplete="off" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Pengantar</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Pengantar" id="pengantar_id" name="pengantar_id" >
                                <option></option>
                                @foreach ($pengantar as $item)
                                    <option value="{{ $item->id }}" {{ $isCutting[0]->pengantar == $item->id ? 'selected' : '' }}>{{ $item->kode }} - {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-solid fs-7" name="keterangan"></textarea>
                        </div>
                    </div>
                </div>
                <div class="pt-2">
                    <span class="text-gray-600 fw-bolder fs-4">Detail</span>
                    <br id="garisBaruDetail">

                </div>
                <div class="table-responsive p-2" id="detailHere">
                    <table class="table align-middle  gy-5">
                        <thead>
                            <tr class="fs-6 fw-bolder text-gray-600 text-center">
                                <th class="min-w-200px">Pengadaan Detail</th>
                                <th class="w-200px">Produk</th>
                                <th class="mw-200px">Varian</th>
                                <th class="min-w-100px">Harga</th>
                                <th class="min-w-100px">Est. Jadi</th>
                                <th class="min-w-100px">Est. Sisa</th>
                                <th class="min-w-50px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="tableItems">
                            @foreach ($isCutting as $cut)
                                @php
                                    $isProduk = DB::select(
                                        'SELECT b.*, a.qty  FROM `pengadaan_detail` AS a
                                        JOIN produk AS b ON a.material_id = b.material_id
                                        WHERE a.id = ' .
                                            $cut->pengadaan_detail_id .
                                            '
                                        GROUP BY b.id',
                                    );

                                    $isVarian = DB::select(
                                        "SELECT f.* 
                                        FROM cutting AS a 
                                        JOIN cutting_detail AS b ON a.id = b.cutting_id AND b.deleted_at IS NULL
                                        JOIN pengadaan AS c ON a.pengadaan_id = c.id
                                        JOIN pengadaan_detail AS d ON b.pengadaan_detail_id = d.id
                                        JOIN produk AS e ON d.material_id = e.material_id 
                                        JOIN produk_detail AS f ON e.id = f.produk_id AND b.produk_id = e.id
                                        WHERE d.id = " .
                                            $cut->pengadaan_detail_id .
                                            " 
                                        GROUP BY f.id",
                                    );
                                @endphp

                                <tr>
                                    <td class="fv-row">
                                        <input type="hidden" id="qty_yard" type="number" value="{{ $cut->qty_yard }}" />
                                        <input type="hidden" id="cutting_detail_id" value="{{ $cut->cutting_detail_id }}">
                                        <select class="form-select form-select-solid fs-7 select-2-id detail-form"
                                            data-control="select2" data-placeholder="Pilih Pengadaan Detail"
                                            id="pengadaan_detail_id" onchange="getProduk(this)">
                                            @foreach ($isPengadaanDetail as $item)
                                                <option value="{{ $item->id }}"
                                                    {{ $cut->pengadaan_detail_id == $item->id ? 'selected' : '' }}>
                                                    {{ $item->qty . $item->uom . ' : ' . $item->nama . '-' . $item->warna }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="fv-row">
                                        <select class="form-select form-select-solid fs-7 select-2-id detail-produk-form"
                                            data-control="select2" data-placeholder="Pilih Produk" id="produk_id"
                                            onchange="getVarianDetail(this)">
                                            @foreach ($isProduk as $item)
                                                <option value="{{ $item->id }}"
                                                    {{ $cut->produk_id == $item->id ? 'selected' : '' }}>
                                                    {{ $item->nama . ' - ' . number_format($item->number_cutting_require, 2) . 'yr' }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="fv-row">
                                        <select class="form-select form-select-solid fs-7 select-2-id detail-varian-form"
                                            data-control="select2" data-placeholder="Pilih Produk Detail" id="varian_id">
                                            <option></option>
                                            @foreach ($isVarian as $item)
                                            <option value="{{ $item->id }}" {{ $item->id == $cut->produk_detail_id ? 'selected' : '' }}>{{ $item->nama }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-solid text-end fs-7" type="text"
                                            id="price" autocomplete="off" value="{{ $cut->harga }}" />
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-white text-end fs-7" type="text"
                                            id="est_jadi" autocomplete="off" value="{{ $cut->est_jadi }}" readonly />
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-white text-end fs-7" type="text"
                                            id="est_sisa" autocomplete="off" value="{{ $cut->est_sisa }}" readonly />
                                    </td>
                                    <td class="fv-row text-center"> <button class="btn btn-sm btn-danger"
                                            data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus detail"
                                            type="button" onclick="removeRowVariant(this)">-</button></td>

                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-end" colspan="7"> <button class="btn btn-sm btn-success"
                                        type="button" onclick="addRowVariant()">+</button></th>
                            </tr>
                            <tr class="fw-bolder fs-7">
                                <th class="text-end" colspan="5"> Est. Jumlah Jadi</th>
                                <th colspan="2"><input type="text"
                                        class="form-control text-end form-control-white fs-7 text-dark" readonly
                                        id="jumlah_est_jadi" /></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="update()">Ubah</button>
                    </div>
                    <div class="p-2">
                        <a href="/admin/produksi/cutting" class="btn btn-sm btn-secondary">Batal</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/produksi/cutting/edit.js"></script>
@endsection
