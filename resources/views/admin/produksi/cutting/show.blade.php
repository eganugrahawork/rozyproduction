@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                {{ $isConfirm == 1 ? 'Proses' : 'Detail' }} Cutting
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="showCutting">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <input type="hidden" name="id" id="id" value="{{ $isCutting[0]->id }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Pengadaan" type="text" name="kode" id="kode"
                                autocomplete="off" value="{{ $isCutting[0]->kode }}" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Partner</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Partner" id="partner_id" name="partner_id"
                                onchange="getDataPartner()" disabled>
                                <option></option>
                                @foreach ($isPartner as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isCutting[0]->partner_id == $item->id ? 'selected' : '' }}>{{ $item->kode }} -
                                        {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">No. Telp</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="No Telepon Partner" type="text" id="no_telp"
                                autocomplete="off" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Alamat</label>
                            <textarea class="form-control form-control-white fs-7" id="alamat" readonly></textarea>
                        </div>

                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Pengadaan</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Pengadaan" id="pengadaan_id" name="pengadaan_id"
                                onchange="getPengadaan()" disabled>
                                <option></option>
                                @foreach ($isPengadaan as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isCutting[0]->pengadaan_id == $item->id ? 'selected' : '' }}>
                                        {{ \Carbon\Carbon::parse($item->tanggal)->format('d-m-Y') }} - {{ $item->kode }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Tanggal</label>
                            <input class="form-control form-control-white fs-7" type="text" name="tanggal" id="tanggal"
                                value="{{ $isCutting[0]->tanggal }}" autocomplete="off" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-white fs-7" name="keterangan" readonly></textarea>
                        </div>
                    </div>
                </div>
                <div class="pt-2">
                    <span class="text-gray-600 fw-bolder fs-4">Detail</span>
                    <br id="garisBaruDetail">

                </div>
                <div class="row">
                    @foreach ($isCutting as $cut)
                        <div class="col-lg-4">
                            <div class="card shadow-sm">
                                <div class="card-header ribbon">
                                    <input type="hidden" id="pengadaanDetailAja" value="{{ $cut->pengadaan_detail_id }}">

                                    @if ($cut->status == 1)
                                        <div class="ribbon-label bg-success">Selesai</div>
                                    @else
                                        <div class="ribbon-label bg-warning">Proses</div>
                                    @endif
                                    <div class="card-title fw-bolder fs-6" id="judulAja">{{ $cut->pengadaan }}</div>
                                </div>
                                <div class="card-body">
                                    <div class="text-center fs-5 text-gray-800 fw-bolder">{{ $cut->produk }}</div>
                                    <div class="text-center fs-6 text-gray-600 mb-2 fw-bolder">{{ $cut->detail_produk }}
                                    </div>
                                    <div class="d-flex flex-wrap mb-5">
                                        <div
                                            class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                                            <div class="fs-6 text-gray-800 fw-bolder" id="estJadiAja">{{ $cut->est_jadi }}
                                            </div>
                                            <div class="fw-bold text-gray-400">Estimasi Jadi</div>
                                        </div>
                                        <div
                                            class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mb-3">
                                            <div class="fs-6 text-gray-800 fw-bolder">{{ $cut->est_sisa }}</div>
                                            <div class="fw-bold text-gray-400">Estimasi Sisa</div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-wrap mb-5">
                                        <div
                                            class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                                            <div class="fs-6 text-gray-800 fw-bolder" id="hargaAja">{{ $cut->harga }}</div>
                                            <div class="fw-bold text-gray-400">Harga Satuan</div>
                                        </div>

                                        <div
                                            class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4  mb-3">
                                            <div class="fs-6 text-gray-800 fw-bolder">
                                                @if ($cut->koreksi !== null)
                                                    {{ $cut->koreksi }}
                                                @else
                                                    <span class="svg-icon svg-icon-danger svg-icon-3"><svg
                                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none">
                                                            <path
                                                                d="M12.6 7C12 7 11.6 6.6 11.6 6V3C11.6 2.4 12 2 12.6 2C13.2 2 13.6 2.4 13.6 3V6C13.6 6.6 13.2 7 12.6 7ZM10 7.59998C10.5 7.29998 10.6 6.69995 10.4 6.19995L9 3.80005C8.7 3.30005 8.10001 3.20002 7.60001 3.40002C7.10001 3.70002 7.00001 4.30005 7.20001 4.80005L8.60001 7.19995C8.80001 7.49995 9.1 7.69995 9.5 7.69995C9.7 7.69995 9.9 7.69998 10 7.59998ZM8 9.30005C8.3 8.80005 8.10001 8.20002 7.60001 7.90002L5.5 6.69995C5 6.39995 4.40001 6.59998 4.10001 7.09998C3.80001 7.59998 4 8.2 4.5 8.5L6.60001 9.69995C6.80001 9.79995 6.90001 9.80005 7.10001 9.80005C7.50001 9.80005 7.9 9.70005 8 9.30005ZM7.20001 12C7.20001 11.4 6.80001 11 6.20001 11H4C3.4 11 3 11.4 3 12C3 12.6 3.4 13 4 13H6.20001C6.70001 13 7.20001 12.6 7.20001 12Z"
                                                                fill="black" />
                                                            <path opacity="0.3"
                                                                d="M17.4 5.5C17.4 6.1 17 6.5 16.4 6.5C15.8 6.5 15.4 6.1 15.4 5.5C15.4 4.9 15.8 4.5 16.4 4.5C17 4.5 17.4 5 17.4 5.5ZM5.80001 17.1L7.40001 16.1C7.90001 15.8 8.00001 15.2 7.80001 14.7C7.50001 14.2 6.90001 14.1 6.40001 14.3L4.80001 15.3C4.30001 15.6 4.20001 16.2 4.40001 16.7C4.60001 17 4.90001 17.2 5.30001 17.2C5.50001 17.3 5.60001 17.2 5.80001 17.1ZM8.40001 20.2C8.20001 20.2 8.10001 20.2 7.90001 20.1C7.40001 19.8 7.3 19.2 7.5 18.7L8.30001 17.3C8.60001 16.8 9.20002 16.7 9.70002 16.9C10.2 17.2 10.3 17.8 10.1 18.3L9.30001 19.7C9.10001 20 8.70001 20.2 8.40001 20.2ZM12.6 21.2C12 21.2 11.6 20.8 11.6 20.2V18.8C11.6 18.2 12 17.8 12.6 17.8C13.2 17.8 13.6 18.2 13.6 18.8V20.2C13.6 20.7 13.2 21.2 12.6 21.2ZM16.7 19.9C16.4 19.9 16 19.7 15.8 19.4L15.2 18.5C14.9 18 15.1 17.4 15.6 17.1C16.1 16.8 16.7 17 17 17.5L17.6 18.4C17.9 18.9 17.7 19.5 17.2 19.8C17 19.9 16.8 19.9 16.7 19.9ZM19.4 17C19.2 17 19.1 17 18.9 16.9L18.2 16.5C17.7 16.2 17.6 15.6 17.8 15.1C18.1 14.6 18.7 14.5 19.2 14.7L19.9 15.1C20.4 15.4 20.5 16 20.3 16.5C20.1 16.8 19.8 17 19.4 17ZM20.4 13H19.9C19.3 13 18.9 12.6 18.9 12C18.9 11.4 19.3 11 19.9 11H20.4C21 11 21.4 11.4 21.4 12C21.4 12.6 20.9 13 20.4 13ZM18.9 9.30005C18.6 9.30005 18.2 9.10005 18 8.80005C17.7 8.30005 17.9 7.70002 18.4 7.40002L18.6 7.30005C19.1 7.00005 19.7 7.19995 20 7.69995C20.3 8.19995 20.1 8.79998 19.6 9.09998L19.4 9.19995C19.3 9.19995 19.1 9.30005 18.9 9.30005Z"
                                                                fill="black" />
                                                        </svg></span>
                                                @endif
                                            </div>
                                            <div class="fw-bold text-gray-400">Koreksi</div>
                                        </div>
                                    </div>
                                    <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mb-3">
                                        <div class="fs-6 text-gray-800 fw-bolder text-center">
                                            @if ($cut->jumlah_jadi == 0)
                                                <span class="svg-icon svg-icon-danger svg-icon-3"><svg
                                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none">
                                                        <path
                                                            d="M12.6 7C12 7 11.6 6.6 11.6 6V3C11.6 2.4 12 2 12.6 2C13.2 2 13.6 2.4 13.6 3V6C13.6 6.6 13.2 7 12.6 7ZM10 7.59998C10.5 7.29998 10.6 6.69995 10.4 6.19995L9 3.80005C8.7 3.30005 8.10001 3.20002 7.60001 3.40002C7.10001 3.70002 7.00001 4.30005 7.20001 4.80005L8.60001 7.19995C8.80001 7.49995 9.1 7.69995 9.5 7.69995C9.7 7.69995 9.9 7.69998 10 7.59998ZM8 9.30005C8.3 8.80005 8.10001 8.20002 7.60001 7.90002L5.5 6.69995C5 6.39995 4.40001 6.59998 4.10001 7.09998C3.80001 7.59998 4 8.2 4.5 8.5L6.60001 9.69995C6.80001 9.79995 6.90001 9.80005 7.10001 9.80005C7.50001 9.80005 7.9 9.70005 8 9.30005ZM7.20001 12C7.20001 11.4 6.80001 11 6.20001 11H4C3.4 11 3 11.4 3 12C3 12.6 3.4 13 4 13H6.20001C6.70001 13 7.20001 12.6 7.20001 12Z"
                                                            fill="black" />
                                                        <path opacity="0.3"
                                                            d="M17.4 5.5C17.4 6.1 17 6.5 16.4 6.5C15.8 6.5 15.4 6.1 15.4 5.5C15.4 4.9 15.8 4.5 16.4 4.5C17 4.5 17.4 5 17.4 5.5ZM5.80001 17.1L7.40001 16.1C7.90001 15.8 8.00001 15.2 7.80001 14.7C7.50001 14.2 6.90001 14.1 6.40001 14.3L4.80001 15.3C4.30001 15.6 4.20001 16.2 4.40001 16.7C4.60001 17 4.90001 17.2 5.30001 17.2C5.50001 17.3 5.60001 17.2 5.80001 17.1ZM8.40001 20.2C8.20001 20.2 8.10001 20.2 7.90001 20.1C7.40001 19.8 7.3 19.2 7.5 18.7L8.30001 17.3C8.60001 16.8 9.20002 16.7 9.70002 16.9C10.2 17.2 10.3 17.8 10.1 18.3L9.30001 19.7C9.10001 20 8.70001 20.2 8.40001 20.2ZM12.6 21.2C12 21.2 11.6 20.8 11.6 20.2V18.8C11.6 18.2 12 17.8 12.6 17.8C13.2 17.8 13.6 18.2 13.6 18.8V20.2C13.6 20.7 13.2 21.2 12.6 21.2ZM16.7 19.9C16.4 19.9 16 19.7 15.8 19.4L15.2 18.5C14.9 18 15.1 17.4 15.6 17.1C16.1 16.8 16.7 17 17 17.5L17.6 18.4C17.9 18.9 17.7 19.5 17.2 19.8C17 19.9 16.8 19.9 16.7 19.9ZM19.4 17C19.2 17 19.1 17 18.9 16.9L18.2 16.5C17.7 16.2 17.6 15.6 17.8 15.1C18.1 14.6 18.7 14.5 19.2 14.7L19.9 15.1C20.4 15.4 20.5 16 20.3 16.5C20.1 16.8 19.8 17 19.4 17ZM20.4 13H19.9C19.3 13 18.9 12.6 18.9 12C18.9 11.4 19.3 11 19.9 11H20.4C21 11 21.4 11.4 21.4 12C21.4 12.6 20.9 13 20.4 13ZM18.9 9.30005C18.6 9.30005 18.2 9.10005 18 8.80005C17.7 8.30005 17.9 7.70002 18.4 7.40002L18.6 7.30005C19.1 7.00005 19.7 7.19995 20 7.69995C20.3 8.19995 20.1 8.79998 19.6 9.09998L19.4 9.19995C19.3 9.19995 19.1 9.30005 18.9 9.30005Z"
                                                            fill="black" />
                                                    </svg></span>
                                            @else
                                                {{ number_format($cut->jumlah_jadi, 0, ',', '.') }}
                                            @endif
                                        </div>
                                        <div class="fw-bold text-gray-400 fw-bolder text-center">Hasil Akhir</div>
                                    </div>
                                    @if ($cut->pembawa !== null)
                                        <hr class="mb-1">
                                        <p class="mb-0">Dibawa Oleh : <span class="fw-bolder">{{ $cut->nama_pembawa }}</span></p>

                                    @endif


                                </div>
                                <div class="card-footer pt-0">
                                    <div class="row">
                                        <div class="col-lg d-flex justify-content-start">
                                            @if ($cut->flag == 1)
                                                <span class="svg-icon svg-icon-primary svg-icon-2hx"><svg
                                                        xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                                        viewBox="0 0 24 24" version="1.1">
                                                        <circle fill="#000000" cx="12" cy="12" r="8" />
                                                    </svg></span>
                                            @elseif ($cut->flag == 2)
                                                <span class="svg-icon svg-icon-warning svg-icon-2hx"><svg
                                                        xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                                        viewBox="0 0 24 24" version="1.1">
                                                        <circle fill="#000000" cx="12" cy="12" r="8" />
                                                    </svg></span>
                                            @elseif ($cut->flag == 3)
                                                <span class="svg-icon svg-icon-danger svg-icon-2hx"><svg
                                                        xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                                        viewBox="0 0 24 24" version="1.1">
                                                        <circle fill="#000000" cx="12" cy="12" r="8" />
                                                    </svg></span>
                                            @elseif ($cut->flag == 4)
                                                <span class="svg-icon svg-icon-success svg-icon-2hx"><svg
                                                        xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                                        viewBox="0 0 24 24" version="1.1">
                                                        <circle fill="#000000" cx="12" cy="12" r="8" />
                                                    </svg></span>
                                            @else
                                                <span class="svg-icon svg-icon-secondary svg-icon-2hx"><svg
                                                        xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                                        viewBox="0 0 24 24" version="1.1">
                                                        <circle fill="#000000" cx="12" cy="12" r="8" />
                                                    </svg></span>
                                            @endif
                                        </div>
                                        <div class="col-lg d-flex justify-content-end">
                                            @if ($cut->status == 0)
                                                <button type="button"
                                                    onclick="closeCuttingModal(this, {{ $cut->cutting_detail_id }})"
                                                    class="btn btn-sm btn-icon btn-primary"><span
                                                        class="svg-icon svg-icon-muted svg-icon-2"><svg
                                                            xmlns="http://www.w3.org/2000/svg" width="24"
                                                            height="24" viewBox="0 0 24 24" fill="none">
                                                            <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z"
                                                                fill="black" />
                                                            <path
                                                                d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z"
                                                                fill="black" />
                                                            <path
                                                                d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z"
                                                                fill="black" />
                                                        </svg></span></button>
                                            @else
                                                <div class="">
                                                    <p class="mb-0 fs-9 text-gray-600">Selesai pada : </p>
                                                    <p class="fs-7 text-active-gray-600 fw-bolder">{{ $cut->tanggal_selesai }}</p>
                                                </div>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <a href="/admin/produksi/cutting" class="btn btn-sm btn-secondary">Kembali</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="closeCuttingModal" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content" id="closeCuttingModalContent">
                <!--begin::Form-->
                <div class="card">
                    <div class="card-header">
                        <div class="card-title fw-bolder">
                            Hasil Akhir
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="text-gray-600 fs-6 fw-bolder text-center" id="judulModal">
                        </div>
                        <form id="formModalCutting">
                            @csrf
                            <div class="d-flex justify-content-center">
                                <input type="hidden" name="pengadaan_detail_id" id="pengadaan_detail_id">
                                <input type="hidden" name="tanggal_diserahkan" value="{{ $isCutting[0]->tanggal }}" >
                                <input type="hidden" name="kode" value="{{ $isCutting[0]->kode }}" />
                                <input type="hidden" name="price" id="price">
                                <input type="hidden" name="id_cutting_detail" id="id_cutting_detail">
                                <input type="hidden" name="est_jadi_modal" id="est_jadi_modal">
                                <div class="fv-row mb-7 col-lg-6">
                                    <input type="number" class="form-control form-control-solid fs-7" id="jumlah_akhir" placeholder="Masukkan Hasil Akhir/Potong"
                                        name="jumlah_akhir">
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="fv-row mb-7 col-lg-6">
                                    <label for="tanggal_selesai" class="form-label fs-7 fw-bolder text-dark ">Tanggal
                                        Selesai</label>
                                    <input type="text" class="form-control form-control-solid fs-7"
                                        id="tanggal_selesai" name="tanggal_selesai">
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="fv-row mb-7 col-lg-6">
                                    <label class="form-label fs-7 fw-bolder text-dark">Pembawa</label>
                                    <select class="form-select form-select-solid fs-7" data-control="select2"
                                        data-placeholder="Pilih Pembawa" id="pembawa_id" name="pembawa_id" >
                                        <option></option>
                                        @foreach ($pembawa as $item)
                                            <option value="{{ $item->id }}">{{ $item->kode }} - {{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg d-flex justify-content-end">
                                    <button type="button" class="btn btn-sm btn-primary"
                                        onclick="storeHasilCutting()">Simpan</button>
                                </div>
                                <div class="col-lg d-flex justify-content-start">
                                    <button type="button" onclick="$('#closeCuttingModal').modal('hide')"
                                        class="btn btn-sm btn-secondary">Batal</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/produksi/cutting/show.js"></script>
@endsection
