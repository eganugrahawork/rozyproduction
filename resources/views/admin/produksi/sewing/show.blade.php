@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                {{ $isConfirm == 1 ? 'Proses' : 'Info' }} Jahit
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="processSewing">
                @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row mb-7">
                            <input type="hidden" name="id" value="{{ $isSewing[0]->id }}">
                            <label class="col-lg-4 fw-bold text-muted">Kode Jahit</label>
                            <div class="col-lg-8">
                                <span class="fw-bolder fs-6 text-gray-800">{{ $isSewing[0]->kode }}</span>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <input type="hidden" name="partner_id" id="partner_id" value="{{ $isSewing[0]->partner_id }}">
                            <label class="col-lg-4 fw-bold text-muted">Penjahit</label>
                            <div class="col-lg-8">
                                <span class="fw-bolder fs-6 text-gray-800">{{ $isSewing[0]->nama_partner }}</span>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <label class="col-lg-4 fw-bold text-muted">No Telepon</label>
                            <div class="col-lg-8">
                                <span class="fw-bolder fs-6 text-gray-800" id="no_telp">{{ $isSewing[0]->no_telp }}</span>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <label class="col-lg-4 fw-bold text-muted">Alamat</label>
                            <div class="col-lg-8">
                                <span class="fw-bolder fs-6 text-gray-800" id="alamat">{{ $isSewing[0]->alamat }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row mb-7">
                            <label class="col-lg-4 fw-bold text-muted">Tanggal Jahit</label>
                            <div class="col-lg-8">
                                <span
                                class="fw-bolder fs-6 text-gray-800">{{ Carbon\Carbon::parse($isSewing[0]->tanggal)->format('d M Y') }}</span>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <input type="hidden" name="pengantar_id" value="{{ $isSewing[0]->id }}">
                            <label class="col-lg-4 fw-bold text-muted">Pengantar</label>
                            <div class="col-lg-8">
                                <span class="fw-bolder fs-6 text-gray-800">{{ $isSewing[0]->nama_pengantar }}</span>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <input type="hidden" name="keterangan" value="{{ $isSewing[0]->id }}">
                            <label class="col-lg-4 fw-bold text-muted">Keterangan</label>
                            <div class="col-lg-8">
                                <span class="fw-bolder fs-6 text-gray-800">{{ $isSewing[0]->keterangan }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-2">
                    @foreach ($isSewing as $sewing)
                    <div class="col-lg-6 card shadow-sm">
                        <div class="card-header">
                            <div class="card-title m-0 fw-bolder">
                                {{ $sewing->produk . '-' . $sewing->varian }}
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <input type="hidden" name="cutting_detail_id" value="{{ $sewing->cutting_detail_id }}">
                                <input type="hidden" id="sewing_detail_id" value="{{ $sewing->sewing_detail_id }}">
                                <input type="hidden" id="jumlah_cutting" value="{{ $sewing->jumlah_cutting }}">
                                <input type="hidden" id="harga_jahit" value="{{ $sewing->harga }}">
                                <input type="hidden" id="tanggal_diserahkan" value="{{ $isSewing[0]->tanggal }}">
                                <label class="col-lg-4 fw-bold text-muted">Dari</label>
                                <div class="col-lg-8">
                                    <span class="fw-bolder fs-6 text-gray-800">{{ $sewing->nama_pemotong }}</span>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 fw-bold text-muted">Quantity</label>
                                <div class="col-lg-8">
                                    <span class="fw-bolder fs-6 text-gray-800">{{ $sewing->jumlah_cutting }}</span>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 fw-bold text-muted">Harga</label>
                                <div class="col-lg-8">
                                    <span class="fw-bolder fs-6 text-gray-800">{{ number_format($sewing->harga,2,",",".") }}</span>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label class="col-lg-4 fw-bold text-muted">Jumlah</label>
                                <div class="col-lg-8">
                                    <span class="fw-bolder fs-6 text-gray-800">{{ number_format($sewing->total_harga,2,",",".") }}</span>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-4 fw-bold text-muted">Selesai</label>
                                <div class="col-lg-8">
                                    <span class="fw-bolder fs-6 text-gray-800" id="selesaiJahit"></span>
                                </div>
                            </div>
                            @if ($isConfirm == 1 && $sewing->status != 1)
                            <div class="row mb-3">
                                <label class="col-lg-7 d-flex align-items-center text-hover-warning fw-bold text-muted fs-8" data-bs-toggle="tooltip" data-bs-placement="top" title="Masukan tanggal selesai jahit !">Tanggal Selesai</label>
                                <div class="col-lg-5">
                                    <input class="form-control form-control-solid form-control-sm fs-8" type="text" id="tanggal_selesai" id="tanggal_selesai"
                                    autocomplete="off" />
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-7 d-flex align-items-center text-hover-warning fw-bold text-muted fs-8" data-bs-toggle="tooltip" data-bs-placement="top" title="Masukan jumlah barang yang selesai dijahit !">Masukan jumlah selesai</label>
                                <div class="col-lg-5">
                                    <input type="number" class="form-control form-control-solid form-control-sm fs-8 fw-bolder text-end" id="input_selesai" data-bs-toggle="tooltip" data-bs-placement="top" title="Harap pastikan semua potongan sudah selesai karena ini tidak bisa diedit.">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-5 d-flex align-items-center text-hover-warning fw-bold text-muted fs-8" data-bs-toggle="tooltip" data-bs-placement="top" title="Orang yang membawa jahitan.">Pembawa</label>
                                <div class="col-lg-7">
                                    <select class="form-select form-select-sm form-select-solid fs-8" data-control="select2"
                                    data-placeholder="Pilih Pembawa" id="pembawa_id" name="pembawa_id" >
                                    @foreach ($pembawa as $item)
                                        <option value="{{ $item->id }}">{{ $item->kode }} - {{ $item->nama }}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>


                            <div class="row mb-3">
                                <div class="p-2 d-flex justify-content-end">
                                    <button type="button" class="btn btn-sm btn-primary" onclick="proccess(this)">Proses</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <a href="/admin/produksi/jahit" class="btn btn-sm btn-secondary">Kembali</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/produksi/sewing/show.js"></script>
@endsection
