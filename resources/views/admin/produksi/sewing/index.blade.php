@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <!--begin::Card header-->
        <div class="card-header border-0 pt-6">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                    <span class="svg-icon svg-icon-1 position-absolute ms-6">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1"
                                transform="rotate(45 17.0365 15.1223)" fill="black" />
                            <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="black" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <input type="text" id="searchsewingTable"
                        class="form-control form-control-sm form-control-solid w-250px ps-15"
                        placeholder="Cari jahit" />
                </div>
                <!--end::Search-->
            </div>
            <!--begin::Card title-->
            <!--begin::Card toolbar-->
            <div class="card-toolbar">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end">
                    <!--begin::Export-->
                    <button type="button" class="btn btn-sm btn-light-primary me-3" data-bs-toggle="modal"
                        data-bs-target="#kt_items_export_modal">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr078.svg-->
                        <span class="svg-icon svg-icon-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.3" x="12.75" y="4.25" width="12" height="2" rx="1"
                                    transform="rotate(90 12.75 4.25)" fill="black" />
                                <path
                                    d="M12.0573 6.11875L13.5203 7.87435C13.9121 8.34457 14.6232 8.37683 15.056 7.94401C15.4457 7.5543 15.4641 6.92836 15.0979 6.51643L12.4974 3.59084C12.0996 3.14332 11.4004 3.14332 11.0026 3.59084L8.40206 6.51643C8.0359 6.92836 8.0543 7.5543 8.44401 7.94401C8.87683 8.37683 9.58785 8.34458 9.9797 7.87435L11.4427 6.11875C11.6026 5.92684 11.8974 5.92684 12.0573 6.11875Z"
                                    fill="black" />
                                <path
                                    d="M18.75 8.25H17.75C17.1977 8.25 16.75 8.69772 16.75 9.25C16.75 9.80228 17.1977 10.25 17.75 10.25C18.3023 10.25 18.75 10.6977 18.75 11.25V18.25C18.75 18.8023 18.3023 19.25 17.75 19.25H5.75C5.19772 19.25 4.75 18.8023 4.75 18.25V11.25C4.75 10.6977 5.19771 10.25 5.75 10.25C6.30229 10.25 6.75 9.80228 6.75 9.25C6.75 8.69772 6.30229 8.25 5.75 8.25H4.75C3.64543 8.25 2.75 9.14543 2.75 10.25V19.25C2.75 20.3546 3.64543 21.25 4.75 21.25H18.75C19.8546 21.25 20.75 20.3546 20.75 19.25V10.25C20.75 9.14543 19.8546 8.25 18.75 8.25Z"
                                    fill="#C4C4C4" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->Export</button>
                    <!--end::Export-->

                    <a href="/admin/produksi/jahit/create" class="btn btn-sm btn-primary">Tambah
                        Jahit</a>
                </div>
                <!--end::Toolbar-->
            </div>
            <!--end::Card toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body pt-0">
            <!--begin::Table-->
            <div class="table-responsive">

                <table class="table align-middle table-row-dashed fs-8 gy-5" id="sewingTable">
                    <!--begin::Table head-->
                    <thead>
                        <!--begin::Table row-->
                        <tr class="text-start text-gray-400 fw-bolder fs-8 text-uppercase gs-0">
                            <th class="min-w-50px text-start">No</th>
                            <th class="min-w-100px text-start" data-bs-toggle="tooltip" data-bs-placement="top" title="Tanggal jahit">Tanggal
                            </th>
                            <th class="min-w-100px text-start" data-bs-toggle="tooltip" data-bs-placement="top" title="Kode jahit">Kode
                            </th>
                            <th class="min-w-100px text-start" data-bs-toggle="tooltip" data-bs-placement="top" title="Nama jahiter">Partner
                            </th>
                            <th class="min-w-100px text-start" data-bs-toggle="tooltip" data-bs-placement="top" title="Detail yang akan dibuat">Detail</th>
                            <th class="min-w-50px text-start">Jumlah Cutting</th>
                            <th class="min-w-50px text-start">Jumlah Jadi</th>
                            <th class="min-w-50px text-start">Koreksi</th>
                            <th class="min-w-125px text-start">Status</th>
                            <th class="text-end min-w-70px text-center">Aksi</th>
                        </tr>
                        <!--end::Table row-->
                    </thead>
                    <!--end::Table head-->
                    <!--begin::Table body-->
                    <tbody class="fw-bold text-gray-600">

                    </tbody>
                    <!--end::Table body-->
                </table>

            </div>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->
    <!--begin::Modals-->
    <div class="modal fade" id="thisModalHere" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content" id="hereModalContent">
                <!--begin::Form-->

                <!--end::Form-->
            </div>
        </div>
    </div>
    <!--end::Modal - Customers - Add-->
    <!--begin::Modal - Adjust Balance-->
    <div class="modal fade" id="kt_items_export_modal" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Modal header-->
                <div class="modal-header">
                    <!--begin::Modal title-->
                    <h2 class="fw-bolder">Export Data</h2>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <button type="button" class="btn btn-icon btn-sm btn-active-icon-primary" onclick="tutupExportModal()">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                    transform="rotate(-45 6 17.3137)" fill="black" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                                    transform="rotate(45 7.41422 6)" fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </button>
                    <!--end::Close-->
                </div>
                <!--end::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
                    <!--begin::Form-->
                    <form id="#" class="form" action="#">

                        <!--begin::Input group-->
                        <div class="fv-row mb-10">
                            <!--begin::Label-->
                            <label class="fs-5 fw-bold form-label mb-5">Pilih Format Export:</label>
                            <!--end::Label-->
                            <!--begin::Input-->
                            <select data-control="select2" data-placeholder="Select a format" data-hide-search="true"
                                name="format" id="formatExport" class="form-select form-select-solid">
                                <option value="excell">Excel</option>
                                <option value="pdf">PDF</option>
                                <option value="cvs">CVS</option>
                                <option value="zip">ZIP</option>
                            </select>
                            <!--end::Input-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Actions-->
                        <div class="text-center">
                            <button type="reset" onclick="tutupExportModal()" class="btn btn-light me-3">Batal</button>
                            <button type="button" onclick="exportList()" class="btn btn-primary">
                                Submit
                            </button>
                        </div>
                        <!--end::Actions-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Modal body-->
            </div>
            <!--end::Modal content-->
        </div>
        <!--end::Modal dialog-->
    </div>
@endsection


@section('jsOnPage')
    <script>
        var sewingTable = $('#sewingTable').DataTable({
            processing: true,
            serverSide: true,
            searching: true,
            "ordering": false,
            ajax: {
                "url": "{{ url('/admin/produksi/jahit/lists') }}",
                "error": function(response) {
                    Swal.fire(
                        'Error',
                        response.statusText,
                        'warning'
                    )
                }
            },
            drawCallback: function(settings) {
                KTMenu.createInstances();
                var api = this.api();
                if (api.data().length === 0 && !api.page.info().recordsTotal) {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    var cari = $('#searchsewingTable').val();
                    toastr.warning("Data '" + cari + "' yang anda dicari tidak ditemukan!");
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    searchable: false
                },
                {
                    data: 'tanggal',
                    name: 'tanggal'
                },
                {
                    data: 'kode',
                    name: 'kode'
                },
                {
                    data: 'nama',
                    name: 'nama'
                },
                {
                    data: 'produk',
                    name: 'produk'
                },
                {
                    data: 'jumlah_cutting',
                    name: 'jumlah_cutting'
                },
                {
                    data: 'jumlah_jahit',
                    name: 'jumlah_jahit',
                    render: $.fn.dataTable.render.number('.', ',', 0, ' ')
                },
                {
                    data: 'koreksi_jahit',
                    name: 'koreksi_jahit',
                    render: $.fn.dataTable.render.number('.', ',', 0, ' ')
                },
                {
                    data: 'status',
                    name: 'status',
                },
                {
                    data: 'action',
                    name: 'action',
                    className : 'text-center'
                }
            ],
            "language": {
                "processing": '<button class="btn btn-transparent" type="button" disabled><span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>Memuat...</button>'
            },
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "orderable": false
        });

        $('#searchsewingTable').on('keydown', function(e) {
            sewingTable.search($(this).val()).draw();
        });

        function tutupExportModal() {
            Swal.fire({
                text: "Yakin ingin menutup ?",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                confirmButtonText: "Ya, tutup!",
                cancelButtonText: "Tidak, batalkan.",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light",
                },
            }).then(function(t) {
                t.value ?
                    ($('#kt_items_export_modal').modal('hide')) :
                    "cancel" === t.dismiss &&
                    Swal.fire({
                        text: "Tidak jadi menutup!.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok!",
                        customClass: {
                            confirmButton: "btn btn-primary",
                        },
                    });
            });
        }

        function exportList() {
            Swal.fire({
                text: "Export data format " + $('#formatExport').text() + ' ?',
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                confirmButtonText: "Ya, Export!",
                cancelButtonText: "Tidak, batalkan.",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light",
                },
            }).then(function(t) {
                t.value ?
                    ($('#kt_items_export_modal').modal('hide')) :
                    "cancel" === t.dismiss &&
                    Swal.fire({
                        text: "Tidak jadi!.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok!",
                        customClass: {
                            confirmButton: "btn btn-primary",
                        },
                    });
            });
        }


        function deleteData(id, nama) {

            Swal.fire({
                text: "Hapus data " + nama + " ?",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                confirmButtonText: "Ya, Hapus!",
                cancelButtonText: "Tidak, batalkan.",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light",
                },
            }).then(function(t) {
                t.value ?
                    $.ajax({
                        url: "/admin/produksi/jahit/" + id,
                        method: "delete",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(response) {
                            if (response.success) {
                                Swal.fire({
                                    text: "Berhasil menghapus jahit!",
                                    icon: "success",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Lanjutkan !",
                                    customClass: {
                                        confirmButton: "btn btn-primary",
                                    },
                                }).then(function(t) {
                                    t.isConfirmed &&
                                        sewingTable.ajax.reload(null, false);
                                });
                            } else {
                                Swal.fire(
                                    response.errors,
                                    'Informasi lebih lanjut hubungi team.',
                                    'error'
                                )
                            }
                        },
                        error: function(xhr, status, error) {
                            if (xhr.status === 422) {
                                var errors = xhr.responseJSON.errors;
                                Swal.fire(
                                    'Peringatan',
                                    errors[0],
                                    'warning'
                                )
                            } else {
                                Swal.fire(
                                    'Error',
                                    'Terjadi kesalahan: ' + status + ' ' + error,
                                    'error'
                                )
                            }
                        }
                    }) :
                    "cancel" === t.dismiss &&
                    Swal.fire({
                        text: "Hapus data " + nama + " dibatalkan!.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok!",
                        customClass: {
                            confirmButton: "btn btn-primary",
                        },
                    });
            });
        }
    </script>
@endsection
