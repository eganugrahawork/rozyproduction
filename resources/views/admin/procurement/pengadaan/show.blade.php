@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                {{ $isConfirm == 1 ? 'Konfirmasi ' : 'Info ' }} Pengadaan
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="approvePengadaan">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <input type="hidden" id="id" name="id" value="{{ $isPengadaan[0]->id }}">
                            <input type="hidden" name="jenis_material_id" name="jenis_material_id"
                                value="{{ $isPengadaan[0]->jenis_material_id }}">
                            <input type="hidden" name="partner_id" name="partner_id"
                                value="{{ $isPengadaan[0]->partner_id }}">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Pengadaan" type="text" name="kode" id="kode"
                                autocomplete="off" value="{{ $isPengadaan[0]->kode }}" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Kategori</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Kategori" id="kategori_id" name="kategori_id"
                                onchange="getCategory()" disabled>
                                <option></option>
                                @foreach ($isJenis as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isPengadaan[0]->jenis_material_id == $item->id ? 'selected' : '' }}>
                                        {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Partner</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Partner" id="partner_id" name="partner_id"
                                onchange="getDataPartner()" disabled>
                                <option></option>
                                @foreach ($isPartner as $item)
                                    <option value="{{ $item->id }}"
                                        {{ $isPengadaan[0]->partner_id == $item->id ? 'selected' : '' }}>
                                        {{ $item->kode }}
                                        - {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">No. Telp</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="No Telepon Partner" type="text" id="no_telp"
                                autocomplete="off" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Alamat</label>
                            <textarea class="form-control form-control-white fs-7" id="alamat" readonly></textarea>
                        </div>

                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Tanggal</label>
                            <input class="form-control form-control-white fs-7" type="text" name="tanggal" id="tanggal"
                                autocomplete="off" value="{{ $isPengadaan[0]->tanggal_ }}" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Pembawa</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Pembawa" id="pembawa_id" name="pembawa_id" disabled>
                                <option></option>
                                @foreach ($pembawa as $item)
                                    <option value="{{ $item->id }}" {{ $isPengadaan[0]->pembawa == $item->id ? 'selected' : '' }}>{{ $item->kode }} - {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Warehouse</label>
                            <select class="form-select form-select-white fs-7" data-control="select2"
                                data-placeholder="Pilih Warehouse" id="warehouse_id" name="warehouse_id" disabled>
                                <option></option>
                                @foreach ($warehouse as $item)
                                    <option value="{{ $item->id }}" {{ $isPengadaan[0]->warehouse_id == $item->id ? 'selected' : '' }}>{{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-white fs-7" name="keterangan" readonly>{{ $isPengadaan[0]->keterangan }}</textarea>
                        </div>


                        <div class="card shadow rounded">
                            <div class="card-body">
                                <p class="fw-bolder fs-5 "> Informasi @if ($isPengadaan[0]->status == 0)
                                    <span class="badge badge-warning">Pending</span>
                                @elseif($isPengadaan[0]->status == 11)
                                <span class="badge badge-danger">Ditolak</span>
                                @else
                                <span class="badge badge-primary">Telah Dikonfirmasi</span>
                            @endif</p>

                                <div class="fw-bolder fs-7 text-gray-600">Dibuat pada tanggal {{ $isPengadaan[0]->tanggal_dibuat }} oleh {{ $isPengadaan[0]->pembuat }}
                                    @if ($isPengadaan[0]->status !== 0)
                                    kemudian dilakukan konfirmasi oleh {{ $isPengadaan[0]->pengonfirmasi }} pada tanggal {{ $isPengadaan[0]->tanggal_konfirmasi }}
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="pt-2">
                    <span id="keterangan" class="text-gray-600 fw-bolder fs-4">Detail</span>
                </div>
                <div class="table-responsive p-2" id="detailHere">
                    <table class="table align-middle  gy-5">
                        <thead>
                            <tr class="fs-6 fw-bolder text-gray-600 text-center">
                                <th class="min-w-125px">Material</th>
                                <th class="min-w-125px">Detail</th>
                                <th class="w-100px">Uom</th>
                                <th class="w-100px">Quantity</th>
                                <th class="mw-70px">Harga</th>
                                <th class="mw-100px">Total</th>
                            </tr>
                        </thead>
                        <tbody id="tableItems">
                            @foreach ($isPengadaan as $item)
                                <tr>
                                    <td class="fv-row">
                                        <input type="hidden" value="{{ $item->pengadaan_detail_id }}"
                                            id="pengadaan_detail_id">
                                        <input type="hidden" value="{{ $item->material_id }}" id="material_id">
                                        <input type="hidden" value="{{ $item->material_detail_id }}"
                                            id="material_detail_id">
                                        <select class="form-select form-select-white fs-7 select-2-id detail-form"
                                            data-control="select2" data-placeholder="Pilih Produk"
                                            onchange="getMaterialVarian(this)" disabled>
                                            <option></option>
                                            @foreach ($material as $m)
                                                <option value="{{ $m->id }}"
                                                    {{ $m->id == $item->material_id ? 'selected' : '' }}>
                                                    {{ $m->nama }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="fv-row">
                                        <select class="form-select form-select-white fs-7 select-2-id"
                                            data-control="select2" data-placeholder="Pilih Varian" disabled>
                                            <option></option>
                                            @foreach ($materialDetail as $m)
                                                <option value="{{ $m->id }}"
                                                    {{ $m->id == $item->material_detail_id ? 'selected' : '' }}>
                                                    {{ $m->warna }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control text-gray-600 form-control-white text-center fs-7"
                                            type="text" id="is_uom" autocomplete="off"
                                            value="({{ $item->uom }})" readonly />
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-white text-end fs-7" type="text"
                                            id="qty" autocomplete="off" value="{{ $item->qty }}"
                                            onkeyup="countTotal()" readonly />
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-white text-end fs-7" type="text"
                                            id="price" autocomplete="off" onkeyup="countTotal()"
                                            value="{{ $item->price }}" readonly />
                                    </td>
                                    <td class="fv-row">
                                        <input class="form-control form-control-white text-end fs-7" type="text"
                                            id="total" autocomplete="off" value="{{ $item->total }}" readonly />
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr class="fw-bolder fs-7">
                                <th class="text-end" colspan="5"> Grand Total</th>
                                <th colspan="1"><input type="text"
                                        class="form-control text-end form-control-white fs-7 text-dark" readonly
                                        id="grandTotal" /></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="d-flex justify-content-center p-2">
                    @if ($isConfirm == 1)
                        <div class="p-2">
                            <button type="button" class="btn btn-sm btn-primary" onclick="confirm(1)">Setujui</button>
                        </div>
                        <div class="p-2">
                            <button type="button" class="btn btn-sm btn-danger" onclick="confirm(11)">Tolak</button>
                        </div>
                    @endif
                    <div class="p-2">
                        <a href="/admin/procurement/pengadaan" class="btn btn-sm btn-secondary">Batal</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/procurement/pengadaan/approve.js"></script>
@endsection
