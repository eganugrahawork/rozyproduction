@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title fw-bolder">
                Tambah Pengadaan
            </div>
        </div>
        <div class="card-body ">
            <form action="#" id="storePengadaan">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Kode</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Kode Pengadaan" type="text" name="kode" id="kode"
                                autocomplete="off" value="{{ $isSequence }}" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Kategori</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Kategori" id="kategori_id" name="kategori_id" onchange="getCategory()">
                                <option></option>
                                @foreach ($isJenis as $item)

                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Partner</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Partner" id="partner_id" name="partner_id" onchange="getDataPartner()">
                                <option></option>
                                @foreach ($isPartner as $item)
                                    <option value="{{ $item->id }}">{{ $item->kode }} - {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">No. Telp</label>
                            <input class="form-control form-control-white fs-7" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="No Telepon Partner" type="text" id="no_telp"
                                autocomplete="off" readonly />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Alamat</label>
                            <textarea class="form-control form-control-white fs-7" id="alamat" readonly></textarea>
                        </div>

                    </div>
                    <div class="col-lg">
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Tanggal</label>
                            <input class="form-control form-control-solid fs-7" type="text" name="tanggal" id="tanggal"
                                autocomplete="off" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Pembawa</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Pembawa" id="pembawa_id" name="pembawa_id" >
                                <option></option>
                                @foreach ($pembawa as $item)
                                    <option value="{{ $item->id }}">{{ $item->kode }} - {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Warehouse</label>
                            <select class="form-select form-select-solid fs-7" data-control="select2"
                                data-placeholder="Pilih Warehouse" id="warehouse_id" name="warehouse_id">
                                <option></option>
                                @foreach ($warehouse as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fs-7 fw-bolder text-dark">Keterangan <span
                                    class="text-gray-400 fs-8 fw-bold">( Opsional )</span></label>
                            <textarea class="form-control form-control-solid fs-7" name="keterangan"></textarea>
                        </div>
                    </div>
                </div>
                <div class="pt-2">
                    <span id="keterangan" class="text-gray-600 fw-bolder fs-4">Detail</span>
                </div>
                <div class="table-responsive p-2" id="detailHere">

                </div>
                <div class="d-flex justify-content-center p-2">
                    <div class="p-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="store()">Tambah</button>
                    </div>
                    <div class="p-2">
                        <a href="/admin/procurement/pengadaan" class="btn btn-sm btn-secondary">Batal</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('jsOnPage')
    <script src="/js/admin/procurement/pengadaan/create.js"></script>
@endsection
