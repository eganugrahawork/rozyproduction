<?php

namespace Database\Seeders;

use App\Models\MediaPenjualan;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MediaPenjualanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MediaPenjualan::create([
            'kategori_media_penjualan_id' => '1',
            'nama' => 'Bu Yanti',
            'email' => 'buyanti@gmail.com',
            'no_hp' => '0828391238',
            'keterangan' => '',
        ]);
        MediaPenjualan::create([
            'kategori_media_penjualan_id' => '2',
            'nama' => 'Rozy',
            'email' => 'rozy@gmail.com',
            'no_hp' => '02391930',
            'keterangan' => '',
        ]);
        MediaPenjualan::create([
            'kategori_media_penjualan_id' => '2',
            'nama' => 'Ameera',
            'email' => 'ameera@gmail.com',
            'no_hp' => '082831114',
            'keterangan' => '',
        ]);
        MediaPenjualan::create([
            'kategori_media_penjualan_id' => '3',
            'nama' => 'Rozy Official',
            'email' => 'rozyofficial@gmail.com',
            'no_hp' => '02391930010',
            'keterangan' => '',
        ]);
        MediaPenjualan::create([
            'kategori_media_penjualan_id' => '3',
            'nama' => 'Fashion by Ameera',
            'email' => 'fashinbyameera@gmail.com',
            'no_hp' => '037829131',
            'keterangan' => '',
        ]);
    }
}
