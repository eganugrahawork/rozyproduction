<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => "Super Admin",
            'username' => "superadmin",
            'role_id' => 1,
            'email' => 'superadmin@developer.com',
            'password' => Hash::make('Password!2'),
        ]);

        DB::table('users')->insert([
            'name' => "Ega Nugraha",
            'username' => "eganugrahaid",
            'role_id' => 1,
            'email' => 'eganugrahaid@gmail.com',
            'password' => Hash::make('Password!2'),
        ]);

        DB::table('users')->insert([
            'name' => "Ica",
            'username' => "icarozy",
            'role_id' => 1,
            'email' => 'icarozy@gmail.com',
            'password' => Hash::make('123456'),
        ]);


        DB::table('users')->insert([
            'name' => "Ilmi Rahma",
            'username' => "ilmirozy",
            'role_id' => 1,
            'email' => 'ilmirozy@gmail.com',
            'password' => Hash::make('123456'),
        ]);

    }
}
