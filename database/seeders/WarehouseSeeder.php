<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Warehouse;
class WarehouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Warehouse::create([
            'nama' => "Gudang 1",
            'alamat' => "Purbaratu",
            'kode_pos' => "242189",
            'pic_id' => "13",
        ]);

        Warehouse::create([
            'nama' => "Gudang 2",
            'alamat' => "A.Yani",
            'kode_pos' => "32899",
            'pic_id' => "12",
        ]);

    }
}
