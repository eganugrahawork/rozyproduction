<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        // Toko
        DB::table('partner')->insert([
            'kategori_partner_id' => '1',
            'kode' => 'AZD',
            'nama' => 'Azad Mayasari',
            'no_telp' => '0821158893',
            'alamat' => 'Mayasari Plaza',
            'keterangan' => '',
        ]);

        DB::table('partner')->insert([
            'kategori_partner_id' => '1',
            'kode' => 'CLST',
            'nama' => 'Calysta Jalan Baru',
            'no_telp' => '0821111223',
            'alamat' => 'Jalan Baru Purbaratu',
            'keterangan' => 'Calysta',
        ]);

        DB::table('partner')->insert([
            'kategori_partner_id' => '1',
            'kode' => 'WRN',
            'nama' => 'Warna Cihideung',
            'no_telp' => '083372783',
            'alamat' => 'Cihideung',
            'keterangan' => '',
        ]);

        // Cutting
        DB::table('partner')->insert([
            'kategori_partner_id' => '2',
            'kode' => 'CC01',
            'nama' => 'Wa Ade',
            'no_telp' => '0823372715',
            'alamat' => 'Belakang',
            'keterangan' => '',
        ]);

        DB::table('partner')->insert([
            'kategori_partner_id' => '2',
            'kode' => 'CC02',
            'nama' => 'Teh Ani',
            'no_telp' => '0812342721',
            'alamat' => 'Belakang',
            'keterangan' => '',
        ]);

        // Penjahit
        DB::table('partner')->insert([
            'kategori_partner_id' => '3',
            'kode' => 'PJ01',
            'nama' => 'Muti',
            'no_telp' => '0812342721',
            'alamat' => 'Konveksi Dalem',
            'keterangan' => '',
        ]);
        DB::table('partner')->insert([
            'kategori_partner_id' => '3',
            'kode' => 'PJ02',
            'nama' => 'Galang',
            'no_telp' => '0812342721',
            'alamat' => 'Konveksi Dalem',
            'keterangan' => '',
        ]);
        DB::table('partner')->insert([
            'kategori_partner_id' => '3',
            'kode' => 'PJ03',
            'nama' => 'Ateng',
            'no_telp' => '0812342721',
            'alamat' => 'Konveksi Dalem',
            'keterangan' => '',
        ]);
        DB::table('partner')->insert([
            'kategori_partner_id' => '3',
            'kode' => 'PJ04',
            'nama' => 'Ijang',
            'no_telp' => '0812342721',
            'alamat' => 'Konveksi Dalem',
            'keterangan' => '',
        ]);
        DB::table('partner')->insert([
            'kategori_partner_id' => '3',
            'kode' => 'PJ05',
            'nama' => 'Kipli',
            'no_telp' => '0812342721',
            'alamat' => 'Konveksi Dalem',
            'keterangan' => '',
        ]);

        // Finishing
        DB::table('partner')->insert([
            'kategori_partner_id' => '4',
            'kode' => 'RZ00329',
            'nama' => 'Wa Enok',
            'no_telp' => '02381903',
            'alamat' => 'Hum',
            'keterangan' => 'Finishing',
        ]);
        DB::table('partner')->insert([
            'kategori_partner_id' => '4',
            'kode' => 'R2910232',
            'nama' => 'Wa Nia',
            'no_telp' => '0812342721',
            'alamat' => 'Hum',
            'keterangan' => 'Finishing',
        ]);

        // Karyawan
        DB::table('partner')->insert([
            'kategori_partner_id' => '5',
            'kode' => 'RZ00219',
            'nama' => 'Sex',
            'no_telp' => '0812342721',
            'alamat' => 'Hum',
            'keterangan' => 'Produksi',
        ]);
        DB::table('partner')->insert([
            'kategori_partner_id' => '5',
            'kode' => 'RZ00220',
            'nama' => 'Dani',
            'no_telp' => '0812342721',
            'alamat' => 'Hum',
            'keterangan' => 'Produksi',
        ]);
        DB::table('partner')->insert([
            'kategori_partner_id' => '5',
            'kode' => 'RZ00221',
            'nama' => 'Ilmi',
            'no_telp' => '0812342721',
            'alamat' => 'Hum',
            'keterangan' => 'Admin',
        ]);
        DB::table('partner')->insert([
            'kategori_partner_id' => '5',
            'kode' => 'RZ00222',
            'nama' => 'Ica',
            'no_telp' => '0812342721',
            'alamat' => 'Hum',
            'keterangan' => 'Admin',
        ]);
        DB::table('partner')->insert([
            'kategori_partner_id' => '5',
            'kode' => 'RZ00223',
            'nama' => 'Ega',
            'no_telp' => '0812342721',
            'alamat' => 'Hum',
            'keterangan' => 'Admin',
        ]);
    }
}
