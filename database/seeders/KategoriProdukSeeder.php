<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class KategoriProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('kategori_produk')->insert([
            'nama' => "Dress",
            'keterangan' => "Pemotong"
        ]);
        DB::table('kategori_produk')->insert([
            'nama' => "Daster",
            'keterangan' => ""
        ]);
        DB::table('kategori_produk')->insert([
            'nama' => "Setelan",
            'keterangan' => ""
        ]);
    }
}
