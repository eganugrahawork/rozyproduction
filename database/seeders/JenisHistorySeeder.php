<?php

namespace Database\Seeders;

use App\Models\JenisHistory;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class JenisHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        JenisHistory::create([
            'nama' => "Saldo Awal",
        ]);

        JenisHistory::create([
            'nama' => "Pembelian/Pengadaan",
        ]);

        JenisHistory::create([
            'nama' => "Cutting",
        ]);
        JenisHistory::create([
            'nama' => "Jahit",
        ]);
        JenisHistory::create([
            'nama' => "Finishing",
        ]);
        JenisHistory::create([
            'nama' => "Produksi Finishing",
        ]);
        JenisHistory::create([
            'nama' => "Penjualan",
        ]);
    }
}
