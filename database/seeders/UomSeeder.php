<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('uom')->insert([
            'nama' => 'Yard',
            'simbol' => 'Yr',
        ]);

        DB::table('uom')->insert([
            'nama' => 'Kilogram',
            'simbol' => 'Kg',
        ]);

        DB::table('uom')->insert([
            'nama' => 'Meter',
            'simbol' => 'M',
        ]);


        DB::table('uom')->insert([
            'nama' => 'Pieces',
            'simbol' => 'pcs',
        ]);
    }
}
