<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            UsersSeeder::class,
            RoleSeeder::class,
            PartnerSeeder::class,
            UomSeeder::class,
            JenisMutasiSeeder::class,
            // MaterialSeeder::class,
            // ProdukSeeder::class,
            SequenceSeeder::class,
            JenisMaterialSeeder::class,
            KategoriPartnerSeeder::class,
            KategoriProdukSeeder::class,
            WarehouseSeeder::class,
            JenisHistorySeeder::class,
            KategoriMediaPenjualanSeeder::class,
            MediaPenjualanSeeder::class,
        ]);

    }
}
