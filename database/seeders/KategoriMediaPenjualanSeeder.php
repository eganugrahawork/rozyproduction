<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\KategoriMediaPenjualan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class KategoriMediaPenjualanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        KategoriMediaPenjualan::create(
            [
                'nama' => 'Konvensional',
            ]
            );
        KategoriMediaPenjualan::create(
            [
                'nama' => 'Shopee',
            ]
            );
        KategoriMediaPenjualan::create(
            [
                'nama' => 'Tiktok Shop',
            ]
            );
    }
}
