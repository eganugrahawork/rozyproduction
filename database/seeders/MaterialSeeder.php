<?php

namespace Database\Seeders;

use App\Models\MaterialStok;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
      $material =  DB::table('material')->insert([
            'jenis_material_id'=> 1,
            'uom_id'=> '1',
            'kode'=> 'Airflow01',
            'nama'=> 'Crinkle Airflow',
            'keterangan'=> '',
        ]);

        DB::table('material_detail')->insert([
            'material_id' => '1',
            'warna' => 'Merah',
        ]);

        MaterialStok::create([
            'material_id' => 1,
            'material_detail_id' => 1,
            'stok_awal' => 0,
                    'stok_masuk' => 0,
                    'stok_keluar' => 0,
                    'stok_akhir' => 0,
            'status' => 0,
        ]);

        DB::table('material_detail')->insert([
            'material_id' => '1',
            'warna' => 'Kuning',
        ]);

        MaterialStok::create([
            'material_id' => 1,
            'material_detail_id' => 2,
            'stok_awal' => 0,
                    'stok_masuk' => 0,
                    'stok_keluar' => 0,
                    'stok_akhir' => 0,
            'status' => 0,
        ]);

        DB::table('material_detail')->insert([
            'material_id' => '1',
            'warna' => 'Hijau',
        ]);

        MaterialStok::create([
            'material_id' => 1,
            'material_detail_id' => 3,
           'stok_awal' => 0,
                    'stok_masuk' => 0,
                    'stok_keluar' => 0,
                    'stok_akhir' => 0,
            'status' => 0,
        ]);

        // Bola

      $material =  DB::table('material')->insert([
            'jenis_material_id'=> 2,
            'uom_id'=> '5',
            'kode'=> 'Bola',
            'nama'=> 'Bola Obras',
            'keterangan'=> '',
        ]);

        DB::table('material_detail')->insert([
            'material_id' => '2',
            'warna' => 'Putih',
        ]);
        MaterialStok::create([
            'material_id' => 2,
            'material_detail_id' => 4,
           'stok_awal' => 0,
                    'stok_masuk' => 0,
                    'stok_keluar' => 0,
                    'stok_akhir' => 0,
            'status' => 0,
        ]);

        DB::table('material_detail')->insert([
            'material_id' => '2',
            'warna' => 'Hitam',
        ]);
        MaterialStok::create([
            'material_id' => 2,
            'material_detail_id' => 5,
           'stok_awal' => 0,
                    'stok_masuk' => 0,
                    'stok_keluar' => 0,
                    'stok_akhir' => 0,
            'status' => 0,
        ]);

        DB::table('material_detail')->insert([
            'material_id' => '2',
            'warna' => 'Navy',
        ]);

        MaterialStok::create([
            'material_id' => 2,
            'material_detail_id' => 6,
           'stok_awal' => 0,
                    'stok_masuk' => 0,
                    'stok_keluar' => 0,
                    'stok_akhir' => 0,
            'status' => 0,
        ]);
        
        // Rayon
        $material =  DB::table('material')->insert([
            'jenis_material_id'=> 1,
            'uom_id'=> '1',
            'kode'=> 'Ryn10',
            'nama'=> 'Rayon KBI',
            'keterangan'=> '',
        ]);

        DB::table('material_detail')->insert([
            'material_id' => '3',
            'warna' => 'Merah',
        ]);

        MaterialStok::create([
            'material_id' => 3,
            'material_detail_id' => 7,
         'stok_awal' => 0,
                    'stok_masuk' => 0,
                    'stok_keluar' => 0,
                    'stok_akhir' => 0,
            'status' => 0,
        ]);

        DB::table('material_detail')->insert([
            'material_id' => '3',
            'warna' => 'Kuning',
        ]);

        MaterialStok::create([
            'material_id' => 3,
            'material_detail_id' => 8,
            'stok_awal' => 0,
                    'stok_masuk' => 0,
                    'stok_keluar' => 0,
                    'stok_akhir' => 0,
            'status' => 0,
        ]);

        DB::table('material_detail')->insert([
            'material_id' => '3',
            'warna' => 'Hijau',
        ]);

        MaterialStok::create([
            'material_id' => 3,
            'material_detail_id' => 9,
            'stok_awal' => 0,
                    'stok_masuk' => 0,
                    'stok_keluar' => 0,
                    'stok_akhir' => 0,
            'status' => 0,
        ]);
    }
}
