<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class JenisMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('jenis_material')->insert([
            'nama' => "Kain",
            'keterangan' => ""
        ]);
        DB::table('jenis_material')->insert([
            'nama' => "Material Jahit",
        ]);
        DB::table('jenis_material')->insert([
            'nama' => "Lainnya",
        ]);
    }
}
