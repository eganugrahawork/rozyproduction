<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class KategoriPartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('kategori_partner')->insert([
            'nama' => "Toko",
            'keterangan' => "Toko"
        ]);
        DB::table('kategori_partner')->insert([
            'nama' => "Cutting Kain",
            'keterangan' => "Pemotong"
        ]);
        DB::table('kategori_partner')->insert([
            'nama' => "Penjahit",
            'keterangan' => ""
        ]);
        DB::table('kategori_partner')->insert([
            'nama' => "Finishing",
            'keterangan' => ""
        ]);
        DB::table('kategori_partner')->insert([
            'nama' => "Karyawan Rozy",
            'keterangan' => ""
        ]);
    }
}
