<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class SequenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('sequence')->insert([
            'tab' => 'Pengadaan',
            'first' => 'P',
            'middle' => '1328',
            'last' => '',
        ]);
        DB::table('sequence')->insert([
            'tab' => 'Cutting',
            'first' => 'CT',
            'middle' => '117',
            'last' => '',
        ]);
        DB::table('sequence')->insert([
            'tab' => 'Sewing',
            'first' => 'SEW',
            'middle' => '117',
            'last' => '',
        ]);
        DB::table('sequence')->insert([
            'tab' => 'Finishing',
            'first' => 'FIN',
            'middle' => '117',
            'last' => '',
        ]);
        DB::table('sequence')->insert([
            'tab' => 'Penjualan',
            'first' => 'SELL',
            'middle' => '011',
            'last' => '',
        ]);
    }
}
