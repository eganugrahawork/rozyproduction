<?php

namespace Database\Seeders;
use Carbon\Carbon;
use App\Models\Produk;
use App\Models\ProdukStok;
use App\Models\MaterialStok;

use App\Models\ProdukDetail;
use App\Models\HistoryProduk;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Hellena
        $produkId = Produk::create([
            'kode' => 'HLN01S',
            'kategori_produk_id' => '1',
            'nama' => 'Hellena',
            'material_id' => '1',
            'hpp' => '42000',
            'number_cutting_require' => '2.4',
            'keterangan' => '',
        ])->id;

        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Maroon',
            'status' => 1,
        ])->id;

        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '42000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);

        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Hitam',
            'status' => 1,
        ])->id;

        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '42000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);
        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Lemon',
            'status' => 1,
        ])->id;

        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '42000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);
        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Navy',
            'status' => 1,
        ])->id;

        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '42000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);
        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Denim',
            'status' => 1,
        ])->id;
        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '42000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);
        // Fiora
        $produkId = Produk::create([
            'kode' => 'FR2019',
            'kategori_produk_id' => '1',
            'nama' => 'Fiora',
            'material_id' => '1',
            'hpp' => '38000',
            'number_cutting_require' => '2',
            'keterangan' => '',
        ])->id;

        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Olive',
            'status' => 1,
        ])->id;
        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '38000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);

        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Hitam',
            'status' => 1,
        ])->id;

        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '38000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);
        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Denim',
            'status' => 1,
        ])->id;

        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '38000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);
        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Navy',
            'status' => 1,
        ])->id;

        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '38000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);

        // Hani
        $produkId = Produk::create([
            'kode' => 'HN1102',
            'kategori_produk_id' => '1',
            'nama' => 'Hani',
            'material_id' => '1',
            'hpp' => '28000',
            'number_cutting_require' => '1.5',
            'keterangan' => '',
        ])->id;

        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Taro',
            'status' => 1,
        ])->id;

        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '28000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);

        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Bronze',
            'status' => 1,
        ])->id;

      
        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '28000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);
        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Biru',
            'status' => 1,
        ])->id;

        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '28000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);
        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Hitam',
            'status' => 1,
        ])->id;

      
        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '28000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);
        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Lemon',
            'status' => 1,
        ])->id;

        
        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '28000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);
        $produkDetailId = ProdukDetail::create([
            'produk_id' => $produkId,
            'nama' => 'Hazelnut',
            'status' => 1,
        ])->id;

       
        ProdukStok::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'stok_awal' => 50,
            'stok_akhir' => 50,
            'status' => 1,
        ]);

        HistoryProduk::create([
            'produk_id' => $produkId,
            'produk_detail_id' => $produkDetailId,
            'tanggal_history' => Carbon::now(),
            'tipe_history' => 0,
            'jenis_history_id' => 1,
            'detail_id_dari_jenis' => $produkDetailId,
            'harga' => '28000',
            'stok_sebelum' => 0,
            'qty' => 50,
            'stok_sesudah' => 50,
            'keterangan' => "Saldo Awal",
            'note' => "Tambah data produk",
            'user_id' => 1,
        ]);
    }
}
