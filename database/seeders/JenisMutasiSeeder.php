<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\JenisMutasi;
class JenisMutasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        JenisMutasi::create([
            'nama' => "Saldo Awal",
        ]);

        JenisMutasi::create([
            'nama' => "Pembelian/Pengadaan",
        ]);

        JenisMutasi::create([
            'nama' => "Cutting",
        ]);
        JenisMutasi::create([
            'nama' => "Jahit",
        ]);
        JenisMutasi::create([
            'nama' => "Finishing",
        ]);
        JenisMutasi::create([
            'nama' => "Produksi Finishing",
        ]);
    }
}
