<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sewing', function (Blueprint $table) {
            $table->id();
            $table->string('kode');
            $table->integer('partner_id');
            $table->dateTime('tanggal');
            $table->mediumText('keterangan')->nullable();
            $table->integer('status')->default(0);
            $table->integer('pembuat')->nullable();
            $table->integer('pengantar')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sewing');
    }
};
