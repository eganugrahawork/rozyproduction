<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('history_produks', function (Blueprint $table) {
            $table->id();
            $table->integer('produk_id');
            $table->integer('produk_detail_id');
            $table->dateTime('tanggal_history');
            $table->integer('tipe_history')->comment("0 history masuk, 1 history keluar");
            $table->integer('jenis_history_id')->comment("foreign key dari jenis history");
            $table->integer('detail_id_dari_jenis')->comment("Id Detail/ id dari Jenis history.");
            $table->decimal('harga', $precision = 20, $scale = 4)->comment("Harga dalam satuan qty ");
            $table->decimal('stok_sebelum', $precision = 20, $scale = 4)->comment("Jumlah dalam satuan qty ");
            $table->decimal('qty', $precision = 20, $scale = 4)->comment("Jumlah dalam satuan qty ");
            $table->decimal('stok_sesudah', $precision = 20, $scale = 4)->comment("Jumlah dalam satuan qty ");
            $table->mediumText('keterangan')->comment("Diinput dari form depan.")->nullable();
            $table->mediumText('note')->comment("Diinput dari sistem biasanya sebagai penanda.");
            $table->string('user_id')->comment("user yang melakukan input");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('history_produks');
    }
};
