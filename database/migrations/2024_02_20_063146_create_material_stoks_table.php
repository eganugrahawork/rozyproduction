<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('material_stok', function (Blueprint $table) {
            $table->id();
            $table->integer('material_id');
            $table->integer('material_detail_id');
            $table->decimal('stok_awal',$precision = 20, $scale = 4);
            $table->decimal('stok_masuk',$precision = 20, $scale = 4);
            $table->decimal('stok_keluar',$precision = 20, $scale = 4);
            $table->decimal('stok_akhir',$precision = 20, $scale = 4);
            $table->integer('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('material_stok');
    }
};
