<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('produk', function (Blueprint $table) {
            $table->id();
            $table->integer('kategori_produk_id');
            $table->string('kode');
            $table->string('nama');
            $table->mediumText('keterangan')->nullable();
            $table->mediumText('url_img')->nullable();
            $table->integer('material_id');
            $table->decimal('hpp', $precision = 20, $scale = 4)->comment("Hpp");
            $table->decimal('number_cutting_require', $precision = 20, $scale = 4)->comment("Kebutuhan cutting satuan yard");
            $table->integer('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('produk');
    }
};
