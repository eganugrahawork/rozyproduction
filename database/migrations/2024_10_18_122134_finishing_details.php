<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('finishing_details', function (Blueprint $table) {
            $table->id();
            $table->integer('finishing_id');
            $table->integer('sewing_detail_id');
            $table->decimal('harga', $precision = 20, $scale = 4);
            $table->decimal('qty_jadi', $precision = 20, $scale = 4)->nullable();
            $table->decimal('koreksi', $precision = 20, $scale = 4)->nullable();
            $table->decimal('total_harga', $precision = 20, $scale = 4);
            $table->dateTime('tanggal_selesai')->nullable();
            $table->integer('status')->default(0);
            $table->integer('pembawa')->nullable();
            $table->integer('warehouse_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('finishing_details');
    }
};
