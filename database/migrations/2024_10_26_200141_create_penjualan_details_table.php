<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('penjualan_details', function (Blueprint $table) {
            $table->id();
            $table->integer('penjualan_id');
            $table->integer('mutasi_id')->default(0);
            $table->integer('produk_id');
            $table->integer('produk_detail_id');
            $table->integer('harga_produk_id');
            $table->decimal('harga', $precision = 20, $scale = 4);
            $table->decimal('qty', $precision = 20, $scale = 4);
            $table->decimal('total_harga', $precision = 20, $scale = 4);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('penjualan_details');
    }
};
