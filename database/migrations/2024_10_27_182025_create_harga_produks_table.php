<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('harga_produks', function (Blueprint $table) {
            $table->id();
            $table->integer('produk_id');
            $table->integer('kategori_media_penjualan_id');
            $table->integer('media_penjualan_id');
            $table->decimal('hpp_sebelumnya', $precision = 20, $scale = 4);
            $table->decimal('hpp', $precision = 20, $scale = 4);
            $table->decimal('harga_jual_sebelumnya', $precision = 20, $scale = 4);
            $table->decimal('harga_jual', $precision = 20, $scale = 4);
            $table->dateTime('tanggal_diubah');
            $table->integer('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('harga_produks');
    }
};
