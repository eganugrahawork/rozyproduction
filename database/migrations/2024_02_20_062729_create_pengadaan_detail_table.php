<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pengadaan_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('pengadaan_id');
            $table->integer('material_id');
            $table->integer('material_detail_id');
            $table->decimal('qty',$precision = 20, $scale = 4);
            $table->decimal('price',$precision = 20, $scale = 4);
            $table->decimal('total',$precision = 20, $scale = 4)->comment('qty * price');
            $table->mediumText('keterangan')->nullable();
            $table->integer('status')->default(0)->comment('0 -> menunggu proses cutting, 1 -> dalam proses cutting, 2 -> selesai proses cutting');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pengadaan_detail');
    }
};
