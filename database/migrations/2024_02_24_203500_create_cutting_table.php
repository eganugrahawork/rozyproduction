<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cutting', function (Blueprint $table) {
            $table->id();
            $table->string('kode');
            $table->integer('pengadaan_id');
            $table->integer('partner_id');
            $table->dateTime('tanggal');
            $table->mediumText('keterangan')->nullable();
            $table->integer('status')->default(0)->comment('0 => menunggu proses, 1=> selesai, 2 => dalam proses');
            $table->integer('pembuat')->default(0);
            $table->integer('pengantar')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cutting');
    }
};
