<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('penjualans', function (Blueprint $table) {
            $table->id();
            $table->string('kode');
            $table->integer('media_penjualan_id');
            $table->dateTime('tanggal');
            $table->mediumText('keterangan')->nullable();
            $table->integer('status')->default(0)->comment('0 -> belum disetujui', '1 ->disetujui');
            $table->integer('shipper_id')->comment('User yang melakukan pengiriman');
            $table->integer('pembuat')->nullable()->comment('User yang membuat');
            $table->dateTime('tanggal_pic')->nullable();
            $table->integer('pic_id')->nullable()->comment('User PIC');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('penjualans');
    }
};
