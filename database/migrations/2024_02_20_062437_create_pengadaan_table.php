<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pengadaan', function (Blueprint $table) {
            $table->id();
            $table->string('kode');
            $table->integer('jenis_material_id');
            $table->integer('partner_id');
            $table->dateTime('tanggal');
            $table->integer('warehouse_id')->default(0);
            $table->mediumText('keterangan')->nullable();
            $table->integer('status')->default(0)->comment('0 -> Pending, 1 -> Dalam proses Produksi, 2 -> Selesai Produksi, 11 -> Ditolak');
            $table->integer('pembuat')->comment('User yang melakukan input');
            $table->integer('pengonfirmasi')->nullable()->comment('User yang melakukan konfirmasi');
            $table->integer('pembawa')->nullable()->comment('User yang membawa kain');
            $table->dateTime('tanggal_konfirmasi')->nullable()->comment('Tanggal melakukan konfirmasi');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pengadaan');
    }
};
