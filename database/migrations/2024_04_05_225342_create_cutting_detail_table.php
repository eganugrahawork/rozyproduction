<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cutting_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('cutting_id');
            $table->integer('pengadaan_detail_id');
            $table->integer('produk_id');
            $table->integer('produk_detail_id');
            $table->decimal('harga', $precision = 20, $scale = 4);
            $table->decimal('est_jadi', $precision = 20, $scale = 4);
            $table->decimal('est_sisa', $precision = 20, $scale = 4);
            $table->decimal('jumlah_jadi', $precision = 20, $scale = 4)->default(0);
            $table->decimal('koreksi', $precision = 20, $scale = 4)->nullable()->comment('jumlah perbedaan antara jumlah jadi dan est jadi');
            $table->dateTime('tanggal_selesai')->nullable();
            $table->integer('status')->default(0)->comment('0 => Proses Pending, 1 => Selesai, 2 => ke jahit');
            $table->integer('flag')->default(0)->comment("untuk penanda indikator bermaksud jika terjadi ketidak sesuaian jumlah jadi.");
            $table->integer('pembawa')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cutting_detail');
    }
};
