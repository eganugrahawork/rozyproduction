<?php
namespace App\Http\Controllers\Admin\Procurement\Pengadaan;

use Carbon\Carbon;
use App\Models\Partner;
use App\Models\Material;
use App\Models\Sequence;
use App\Models\Pengadaan;
use App\Models\Warehouse;
use App\Models\MaterialStok;
use Illuminate\Http\Request;
use App\Models\JenisMaterial;
use App\Models\MaterialDetail;
use App\Models\HistoryMaterial;
use App\Models\PengadaanDetail;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PengadaanController extends Controller
{
    public function index(){
        return view('admin.procurement.pengadaan.index');
    }

    public function jumlahPengadaan(Request $request){
        $jumlahPengadaan = DB::select("SELECT FORMAT(SUM(a.qty * a.price),2) as total, SUM(a.total) as total_2 from (
            SELECT b.* FROM pengadaan AS a
            JOIN pengadaan_detail AS b ON a.id = b.pengadaan_id AND b.deleted_at IS NULL
            WHERE a.deleted_at IS null and a.status = 1  AND a.tanggal BETWEEN '".$request->tanggal_mulai."' AND '".$request->tanggal_akhir."'
            GROUP BY b.id
            ) AS a ");

            return response()->json(['jumlah' => $jumlahPengadaan[0]->total]);
    }

    public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            $tanggalMulai = $request->tanggal_mulai;
            $tanggalAkhir = $request->tanggal_akhir;
            if ($search) {
                $search = "AND a.kode LIKE '%" . $search . "%' OR a.keterangan LIKE '%" . $search . "%' OR c.nama LIKE '%" . $search . "%' OR d.warna LIKE '%" . $search . "%' OR e.nama LIKE '%" . $search . "%'";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table("pengadaan")->where('deleted_at', 'is null')->get()->count();
            $lists = DB::select("SELECT a.id, DATE(a.tanggal) as tanggal, a.kode, DATE(a.tanggal_konfirmasi) as tanggal_konfirmasi, e.nama AS partner, c.nama AS material, SUM(b.qty) as qty, SUM(b.price) as price, SUM(b.total) as total, a.status
            FROM pengadaan AS a
           JOIN pengadaan_detail AS b ON a.id = b.pengadaan_id AND b.deleted_at IS NULL
           JOIN material AS c ON b.material_id = c.id
           JOIN material_detail AS d ON b.material_detail_id = d.id
           JOIN partner AS e ON a.partner_id =e.id WHERE a.deleted_at is null
           AND a.tanggal BETWEEN '".$tanggalMulai."' AND '".$tanggalAkhir."'
           $search group by a.id order by a.created_at desc limit $limit offset $offset");
            $totalRecords = count($lists);

            return DataTables::of($lists)
                ->addIndexColumn()->addColumn('status', function($model){
                    if($model->status == 0){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-800 fs-8">'.$model->tanggal.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-warning fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Menunggu Persetujuan</div>
                        </div>
                        </div>';
                    }elseif($model->status == 11){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-warning fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Menunggu</div>
                        </div>
                        <div class="timeline-item">
														<div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal_konfirmasi.'</div>
														<div class="timeline-badge">
															<i class="fa fa-genderless text-danger fs-5"></i>
														</div>
														<div class="timeline-content d-flex">
															<span class="fw-bolder text-gray-800 ps-3">Ditolak/Selesai</span>
														</div>
													</div>
                        </div>';
                    }elseif($model->status == 1){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal_konfirmasi.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-primary fs-5"></i>
                            </div>
                            <div class="fw-bolder timeline-content text-gray-800 ps-3">Disetujui</div>
                        </div>
                        <div class="timeline-item">
														<div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal.'</div>
														<div class="timeline-badge">
															<i class="fa fa-genderless text-warning fs-5"></i>
														</div>
														<div class="timeline-content d-flex">
															<span class="fw-normal text-gray-800 ps-3">Menunggu</span>
														</div>
													</div>
                        </div>';
                    }

                    return $status;
                })
                ->addColumn('action', function ($model) {
                    $action =
                        '  <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                            <span class="svg-icon svg-icon-5 m-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none">
                                    <path
                                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                        fill="black" />
                                </svg>
                            </span></a>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-8 w-125px py-4"
                            data-kt-menu="true">
                    ';

        if($model->status == 0 ){
            $action .= '    <div class="menu-item px-3">
                <a href="/admin/procurement/pengadaan/show/' .
                $model->id .'/1" class="menu-link px-3">Approve</a>
            </div>
            <div class="menu-item px-3">
                <a href="/admin/procurement/pengadaan/' .
                $model->id .'" class="menu-link px-3">Edit</a>
            </div>
            <div class="menu-item px-3">
                <button type="button" class="menu-link px-3 btn btn-transparent fs-8" onclick="deleteData(' .
                            $model->id .
                            ', ' .
                            "'" .
                            $model->partner .
                            "'" .
                            ')">Delete</button>
            </div>
            ';
        }

        $action .= '
        <div class="menu-item px-3">
        <a href="/admin/procurement/pengadaan/show/' .
        $model->id .'/0" class="menu-link px-3">Info</a>
</div>  </div>';
                    return $action;
                })
                ->rawColumns(['action', 'status'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function create(){
        $isSequence = Sequence::where(['tab' => 'Pengadaan'])->first();
        $isPartner = Partner::where(['kategori_partner_id' => 1])->get();
        $isJenis = JenisMaterial::all();
        $pembawa = Partner::where('kategori_partner_id', 5)->get();
        $warehouse = Warehouse::all();

        $isSequence = $isSequence->first . $isSequence->middle + 1 . $isSequence->last;
        return view('admin.procurement.pengadaan.create', ['isPartner' => $isPartner, 'isSequence' =>$isSequence, 'isJenis' => $isJenis, 'pembawa' => $pembawa,'warehouse' => $warehouse]);
    }

    public function getDataPartner($id){
        $partner = Partner::where('id', $id)->first();

        return response()->json(['partner' => $partner]);
    }

    public function getDetailForm($id){
            $material = Material::where('jenis_material_id', $id)->get();

            return response()->json(['material' => $material]);
    }

    public function getDetailMaterial($id){
            $materialDetail = MaterialDetail::where('material_id', $id)->get();
            $uom = DB::select('select b.* from material as a join uom as b on a.uom_id = b.id where a.id = '. $id);

            return response()->json(['materialDetail' => $materialDetail, 'uom' => $uom[0]]);
    }

    public function store(Request $request){
        // dd($request);
        try {
            DB::beginTransaction();
            $pengadaanId = Pengadaan::create([
                'kode' => $request->kode,
                'jenis_material_id' => $request->kategori_id,
                'partner_id' => $request->partner_id,
                'tanggal' => $request->tanggal,
                'warehouse_id' => $request->warehouse_id,
                'keterangan' => $request->keterangan,
                'status' => 0,
                'pembuat' => auth()->user()->id,
                'pembawa' => $request->pembawa_id,
            ])->id;

            foreach($request->material_id as $i => $value){
               $pengadaanDetailId = PengadaanDetail::create([
                    'pengadaan_id' => $pengadaanId,
                    'material_id' => $request->material_id[$i],
                    'material_detail_id' => $request->material_detail_id[$i],
                    'qty' => $request->qty[$i],
                    'price' => str_replace(',', '',$request->price[$i]),
                    'total' => str_replace(',', '',$request->total[$i])
                ]);
            }
            $isSequence = Sequence::where(['tab' => 'Pengadaan'])->first();
            $SeqUpdate = $isSequence->middle + 1;
            Sequence::where(['tab' => 'Pengadaan'])->update(['middle' => $SeqUpdate]);
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function edit($id){
        $isPengadaan = DB::select("SELECT a.*, b.id as pengadaan_detail_id, b.material_id ,b.material_detail_id,FORMAT(b.qty,2) as qty,
        FORMAT(b.price, 0) as price, FORMAT(b.total, 0) as total, c.jenis_material_id, d.simbol as uom, a.warehouse_id
        FROM pengadaan AS a
            JOIN pengadaan_detail AS b ON a.id = b.pengadaan_id AND b.deleted_at IS NULL
            JOIN material AS c ON b.material_id = c.id
            JOIN uom as d ON c.uom_id = d.id
        WHERE a.id = $id
            GROUP BY b.id");
        $material = Material::where(['jenis_material_id' => $isPengadaan[0]->jenis_material_id])->get();
        $materialDetail = MaterialDetail::where(['material_id' => $isPengadaan[0]->material_id])->get();
        $isPartner = Partner::where(['kategori_partner_id' => 1])->get();
        $isJenis = JenisMaterial::all();
        $pembawa = Partner::where('kategori_partner_id', 5)->get();
        $warehouse = Warehouse::all();
        return view('admin.procurement.pengadaan.edit', ['isPartner' => $isPartner, 'isJenis' => $isJenis, 'isPengadaan' => $isPengadaan, 'material' => $material, 'materialDetail'=>$materialDetail ,'pembawa' => $pembawa, 'warehouse'=>$warehouse]);
    }

    public function update(Request $request){
        // dd($request);
        try {
            DB::beginTransaction();
             Pengadaan::where(['id' => $request->id])->update([
                'kode' => $request->kode,
                'jenis_material_id' => $request->kategori_id,
                'partner_id' => $request->partner_id,
                'tanggal' => $request->tanggal,
                'warehouse_id' => $request->warehouse_id,
                'keterangan' => $request->keterangan,
                'status' => 0,
                'pembuat' => auth()->user()->id,
                'pembawa' => $request->pembawa_id,
            ]);

            if(isset($request->pengadaan_detail_id)){
                $inDb = PengadaanDetail::where(['pengadaan_id' => $request->id])->count();
                $inReq = count($request->pengadaan_detail_id);
                if($inDb > $inReq){
                    PengadaanDetail::where('pengadaan_id', $request->id)
                    ->whereNotIn('id', $request->pengadaan_detail_id)
                    ->delete();
                }
            }else{
                PengadaanDetail::where('pengadaan_id', $request->id)->delete();
            }

            foreach($request->material_id as $i => $value){
                if(isset($request->pengadaan_detail_id[$i])){
                    PengadaanDetail::where(['id' => $request->pengadaan_detail_id[$i]])->update([
                        'material_id' => $request->material_id[$i],
                        'material_detail_id' => $request->material_detail_id[$i],
                        'qty' => $request->qty[$i],
                        'price' => str_replace(',', '',$request->price[$i]),
                        'total' => str_replace(',', '',$request->total[$i])
                ]);
                }else{
                    PengadaanDetail::create([
                        'pengadaan_id' => $request->id,
                        'material_id' => $request->material_id[$i],
                        'material_detail_id' => $request->material_detail_id[$i],
                        'qty' => $request->qty[$i],
                        'price' => str_replace(',', '',$request->price[$i]),
                        'total' => str_replace(',', '',$request->total[$i])
                    ]);
                }
            }

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function destroy($id){
        try {
            DB::beginTransaction();
            Pengadaan::where('id', $id)->delete();
            PengadaanDetail::where('pengadaan_id', $id)->delete();
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function show($id, $isConfirm){
        $isPengadaan = DB::select("SELECT a.*, DATE(a.tanggal) as tanggal_,DATE(a.created_at) as tanggal_dibuat, DATE(a.tanggal_konfirmasi) as tanggal_konfirmasi, e.name as pembuat, f.name as pengonfirmasi, b.id as pengadaan_detail_id, b.material_id ,b.material_detail_id,FORMAT(b.qty,2) as qty, a.warehouse_id,
        FORMAT(b.price, 0) as price, FORMAT(b.total, 0) as total, c.jenis_material_id, d.simbol as uom
        FROM pengadaan AS a
            JOIN pengadaan_detail AS b ON a.id = b.pengadaan_id AND b.deleted_at IS NULL
            JOIN material AS c ON b.material_id = c.id
            JOIN uom as d ON c.uom_id = d.id
            JOIN users as e ON a.pembuat = e.id
            LEFT JOIN users as f on a.pengonfirmasi = f.id
        WHERE a.id = $id
            GROUP BY b.id");
        $material = Material::where(['jenis_material_id' => $isPengadaan[0]->jenis_material_id])->get();
        $materialDetail = MaterialDetail::where(['material_id' => $isPengadaan[0]->material_id])->get();
        $isPartner = Partner::all();
        $isJenis = JenisMaterial::all();
        $pembawa = Partner::where('kategori_partner_id', 5)->get();
        $warehouse = Warehouse::all();
        return view('admin.procurement.pengadaan.show', ['isPartner' => $isPartner, 'isJenis' => $isJenis, 'isPengadaan' => $isPengadaan, 'material' => $material, 'materialDetail'=>$materialDetail, 'isConfirm'=>$isConfirm, 'pembawa' => $pembawa, 'warehouse'=>$warehouse]);
    }

    public function confirm(Request $request,$value){
        try {
            DB::beginTransaction();
            if($value == 1){
                Pengadaan::where(['id' => $request->id])->update(['status' => 1,
                'tanggal_konfirmasi' => Carbon::now(),
                'pengonfirmasi' => auth()->user()->id]);
                foreach($request->pengadaan_detail_id as $i => $val){
                    $stok = MaterialStok::where( ['material_id' => $request->material_id[$i],
                    'material_detail_id' => $request->material_detail_id[$i]])->first();

                    HistoryMaterial::create([
                        'material_id' => $request->material_id[$i],
                        'material_detail_id' => $request->material_detail_id[$i],
                        'tanggal_history' => $request->tanggal,
                        'tipe_history' => 0,
                        'jenis_history_id' => 2,
                        'detail_id_dari_jenis' => $request->pengadaan_detail_id[$i],
                        'harga' => str_replace(',', '',$request->price[$i]),
                        'stok_sebelum' => $stok->stok_akhir,
                        'stok_sesudah' => $stok->stok_akhir + $request->qty[$i],
                        'qty' => $request->qty[$i],
                        'keterangan' => "Pengadaan",
                        'note' => "Masuk Pengadaan",
                        'user_id' => auth()->user()->id,
                    ]);

                  DB::select('UPDATE material_stok SET stok_masuk = stok_masuk +'.$request->qty[$i].', stok_akhir = stok_akhir + '.$request->qty[$i].', updated_at = "'.Carbon::now()->format('Y-m-d H:i:s') .'" WHERE material_id = '.$request->material_id[$i].' AND material_detail_id = '.$request->material_detail_id[$i]);


                }

            }else{
                Pengadaan::where(['id' => $request->id])->update(['status' => 11,
                'tanggal_konfirmasi' => Carbon::now(),
                'pengonfirmasi' => auth()->user()->id]);
            }

            DB::commit();
            return response()->json(['success' => true]);
          } catch (\GuzzleHttp\Exception\RequestException $e) {
                DB::rollback();
              $response = $e->getResponse();
              $responseData = json_decode($response->getBody(), true);
              if ($responseData['message']) {
                  return response()->json(['errors' => $responseData['message']]);
              } else {
                  return response()->json(['errors' => 'Gagal memanggil API !']);
              }
          }

    }

}
