<?php

namespace App\Http\Controllers\Admin\Masterdata\Produk;

use Carbon\Carbon;
use App\Models\Produk;
use App\Models\Material;
use App\Models\ProdukStok;
use App\Models\ProdukDetail;
use Illuminate\Http\Request;
use App\Models\HistoryProduk;
use App\Models\KategoriProduk;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ProdukController extends Controller
{
    public function index()
    {
        return view('admin.masterdata.produk.index');
    }

    public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.nama LIKE '%" . $search . "%' OR a.keterangan LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('produk')->count();
            $lists = DB::select("SELECT MAX(d.id) AS isIt, a.*, CAST(a.number_cutting_require AS DECIMAL(10,2)) as ncr, c.nama as kategori,b.varian
            FROM produk AS a
           JOIN (
			  SELECT aa.id, GROUP_CONCAT(bb.nama SEPARATOR '<br>') AS varian
			  from produk AS aa
			  join produk_detail AS bb ON aa.id = bb.produk_id AND bb.deleted_at IS NULL
           GROUP BY aa.id) AS b ON a.id = b.id
           JOIN kategori_produk as c on a.kategori_produk_id = c.id
           LEFT JOIN history_produks AS d ON a.id = d.produk_id AND d.jenis_history_id <> 1
            WHERE a.deleted_at is null $search
				group by a.id order by a.created_at desc limit $limit offset $offset");
            $totalRecords = count($lists);

            return DataTables::of($lists)
                ->addIndexColumn()
                ->addColumn('action', function ($model) {
                    $action =
                        '  <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                                    <span class="svg-icon svg-icon-5 m-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <path
                                                d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                fill="black" />
                                        </svg>
                                    </span></a>
                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-8 w-125px py-4"
                                    data-kt-menu="true">
                                    <div class="menu-item px-3">
                                            <a href="/admin/masterdata/produk/show/' .
                                                            $model->id . '" class="menu-link px-3">Detail</a>
                                    </div>
                                <div class="menu-item px-3">
                        <a href="/admin/masterdata/produk/edit/' .
                                        $model->id . '" class="menu-link px-3">Edit</a>
                </div>';
                if(!isset($model->isIt)){
                $action .= '<div class="menu-item px-3">
                            <button type="button" class="menu-link px-3 btn btn-transparent fs-8" onclick="deleteData(' .
                                        $model->id .
                                        ', ' .
                                        "'" .
                                        $model->nama .
                                        "'" .
                                        ')">Delete</button>
                        </div>';
                    }
                    $action .= '</div>';
                    return $action;
                })
                ->rawColumns(['action', 'varian'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function create()
    {
        $isKategori = KategoriProduk::all();
        $isMaterial = Material::where(['jenis_material_id' => 1])->get();
        return view('admin.masterdata.produk.create', ['isKategori' => $isKategori, 'isMaterial' => $isMaterial]);
    }

    public function store(Request $request){
        // dd($request);
        try {
            DB::beginTransaction();
            $produkId = Produk::create([
                'kode' => $request->kode,
                'kategori_produk_id' => $request->kategori_id,
                'nama' => $request->nama,
                'hpp' => str_replace(',', '',$request->hpp),
                'material_id' => $request->material_id,
                'number_cutting_require' => $request->number_cutting_require,
                'keterangan' => $request->keterangan,
            ])->id;

            foreach($request->warna as $i => $value){
              $produkDetailId =  ProdukDetail::create([
                    'produk_id' => $produkId,
                    'nama' => $value,
                    'status' => 1,
                ])->id;

                // Belum ada history
                ProdukStok::create([
                    'produk_id' => $produkId,
                    'produk_detail_id' => $produkDetailId,
                    'stok_awal' => $request->stok_awal[$i],
                    'stok_akhir' => $request->stok_awal[$i],
                    'status' => 1,
                ]);

                HistoryProduk::create([
                    'produk_id' => $produkId,
                    'produk_detail_id' => $produkDetailId,
                    'tanggal_history' => Carbon::now(),
                    'tipe_history' => 0,
                    'jenis_history_id' => 1,
                    'detail_id_dari_jenis' => $produkDetailId,
                    'harga' => str_replace(',', '',$request->hpp),
                    'stok_sebelum' => 0,
                    'qty' => $request->stok_awal[$i],
                    'stok_sesudah' => $request->stok_awal[$i],
                    'keterangan' => "Saldo Awal",
                    'note' => "Tambah data produk",
                    'user_id' => auth()->user()->id,
                ]);
            }
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function edit($id){
        $isKategori = KategoriProduk::all();
        $isMaterial = Material::where(['jenis_material_id' => 1])->get();
        $isProduk = DB::select("SELECT a.*, b.id AS produk_detail_id, b.nama as varian,
        FORMAT(d.stok_awal,0) as stok_awal,  MAX(e.id) AS id_history
        FROM produk AS a
        JOIN produk_detail AS b ON a.id = b.produk_id and b.deleted_at is null
        JOIN kategori_produk AS c ON a.kategori_produk_id = c.id
        JOIN produk_stok as d ON a.id = d.produk_id and b.id = d.produk_detail_id
        LEFT JOIN history_produks AS e ON a.id = e.produk_id AND e.jenis_history_id <> 1 AND b.id = e.produk_detail_id
        WHERE a.id = $id
        GROUP BY b.id");

        return view('admin.masterdata.produk.edit', ['isProduk' => $isProduk, 'isKategori' => $isKategori, 'isMaterial' => $isMaterial]);
    }

    public function update(Request $request){
        // dd($request);
        try {
            DB::beginTransaction();
             Produk::where(['id' => $request->id])->update([
                'kategori_produk_id' => $request->kategori_id,
                'kode' => $request->kode,
                'nama' => $request->nama,
                'hpp' => str_replace(',', '',$request->hpp),
                'material_id' => $request->material_id,
                'number_cutting_require' => $request->number_cutting_require,
                'keterangan' => $request->keterangan,
            ]);

            if(isset($request->produk_detail_id)){
                $inDb = ProdukDetail::where(['produk_id' => $request->id])->count();
                $inReq = count($request->produk_detail_id);
                if($inDb > $inReq){
                    ProdukDetail::where('produk_id', $request->id)
                    ->whereNotIn('id', $request->produk_detail_id)
                    ->delete();
                    ProdukStok::where('produk_id', $request->id)
                    ->whereNotIn('produk_detail_id', $request->produk_detail_id)
                    ->delete();
                    HistoryProduk::where(['produk_id' => $request->id, 'jenis_history_id' => 1])
                    ->whereNotIn('produk_detail_id', $request->produk_detail_id)->delete();
                }
            }else{
                ProdukDetail::where('produk_id', $request->id)->delete();
                ProdukStok::where('produk_id', $request->id)->delete();
                HistoryProduk::where(['produk_id' => $request->id, 'jenis_history_id' => 1])->delete();
            }

            foreach($request->warna as $i => $value){
                if(isset($request->produk_detail_id[$i])){
                    ProdukDetail::where(['id' => $request->produk_detail_id[$i]])->update([
                    'produk_id' => $request->id,
                    'nama' => $request->warna[$i],
                    ]);
                    if(!isset($request->id_history[$i])){
                        ProdukStok::where(['produk_id' => $request->id, 'produk_detail_id' => $request->produk_detail_id[$i]])->update([
                            'stok_awal' => $request->stok_awal[$i],
                            'stok_akhir' => $request->stok_awal[$i],
                        ]);

                        HistoryProduk::where(['produk_id' => $request->id, 'jenis_history_id' => 1, 'produk_detail_id' => $request->produk_detail_id[$i]])->update([
                            'qty' => $request->stok_awal[$i],
                            'harga'=> str_replace(',', '',$request->hpp),
                        ]);
                    }

                }else{
                    $produkDetailId = ProdukDetail::create([
                    'produk_id' => $request->id,
                    'nama' => $request->warna[$i],
                    ])->id;
                    ProdukStok::create([
                        'produk_id' => $request->id,
                        'produk_detail_id' => $produkDetailId,
                        'stok_awal' => $request->stok_awal[$i],
                        'stok_akhir' => $request->stok_awal[$i],
                        'status' => 1,
                    ]);

                    HistoryProduk::create([
                        'produk_id' => $request->id,
                        'produk_detail_id' => $produkDetailId,
                        'tanggal_history' => Carbon::now(),
                        'tipe_history' => 0,
                        'jenis_history_id' => 1,
                        'detail_id_dari_jenis' => $produkDetailId,
                        'harga' => str_replace(',', '',$request->hpp),
                        'stok_sebelum' => 0,
                        'qty' => $request->stok_awal[$i],
                        'stok_sesudah' => $request->stok_awal[$i],
                        'keterangan' => "Saldo Awal",
                        'note' => "Tambah data produk",
                        'user_id' => auth()->user()->id,
                    ]);

                }
            }

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function check_code(Request $request)
    {
        if ($request->kode_sebelumnya) {
            $code = Produk::where(['kode' => $request->kode])
                ->where('kode', '!=', $request->kode_sebelumnya)
                ->first();
        } else {
            $code = Produk::where(['kode' => $request->kode])->first();
        }
        if ($code) {
            return response()->json(['valid' => false]);
        }
        return response()->json(['valid' => true]);
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Produk::where('id', $id)->delete();
            ProdukDetail::where('produk_id', $id)->delete();
            ProdukStok::where('produk_id', $id)->delete();
            HistoryProduk::where('produk_id', $id)->delete();
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function show($id){
        $isKategori = KategoriProduk::all();
        $isMaterial = Material::where(['jenis_material_id' => 1])->get();
        $isProduk = DB::select("SELECT a.*, b.id AS produk_detail_id, b.nama as varian,
        FORMAT(d.qty,0) as stok_awal
        FROM produk AS a
        JOIN produk_detail AS b ON a.id = b.produk_id and b.deleted_at is null
        JOIN kategori_produk AS c ON a.kategori_produk_id = c.id
        JOIN history_produks as d ON a.id = d.produk_id and b.id = d.produk_detail_id
        WHERE a.id = $id and d.jenis_history_id = 1
        GROUP BY b.id");

        return view('admin.masterdata.produk.show', ['isProduk' => $isProduk, 'isKategori' => $isKategori, 'isMaterial' => $isMaterial]);
    }

}
