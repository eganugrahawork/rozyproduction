<?php

namespace App\Http\Controllers\Admin\Masterdata\Coa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Models\Coa;

class CoaController extends Controller
{
    public function index(){
        return view('admin.masterdata.coa.index');
    }

    public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.kode LIKE '%" . $search . "%' OR a.nama LIKE '%" . $search . "%' OR b.kode LIKE '%" . $search . "%' OR b.nama LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('coas')->where('deleted_at','is_null')->count();
            $lists = DB::select("SELECT IFNULL(CONCAT(b.kode, '-', b.nama),'Parent') as parent, CONCAT(a.kode,'-',a.nama) as coa, CASE 
        WHEN a.adjustment = 0 THEN 'Debit'
        WHEN a.adjustment = 1 THEN 'Kredit'
    END as adjustment, a.keterangan, a.id from coas as a
            LEFT JOIN coas as b on a.parent_id = b.id
           WHERE a.deleted_at is null $search group by a.id order by a.created_at desc limit $limit offset $offset");
            $totalRecords = count($lists);

            return DataTables::of($lists)
                ->addIndexColumn()
                ->addColumn('action', function ($model) {
                    $action =
                        '  <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
        data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
        <span class="svg-icon svg-icon-5 m-0">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                viewBox="0 0 24 24" fill="none">
                <path
                    d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                    fill="black" />
            </svg>
        </span></a>
    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-8 w-125px py-4"
        data-kt-menu="true">

        <div class="menu-item px-3">
        <a href="/admin/masterdata/coa/edit/' .
        $model->id .'" class="menu-link px-3">Edit</a>
</div>
        <div class="menu-item px-3">
            <button type="button" class="menu-link px-3 btn btn-transparent fs-8" onclick="deleteData(' .
                        $model->id .
                        ', ' .
                        "'" .
                        $model->coa .
                        "'" .
                        ')">Delete</button>
        </div>
    </div>';
                    return $action;
                })
                ->rawColumns(['action'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function create(){
        $parentCoa = Coa::all();
        return view('admin.masterdata.coa.create', ['parentCoa' => $parentCoa]);
    }

    public function store(Request $request){
        try {
            DB::beginTransaction();
            $coaId = Coa::create([
                'parent_id' => $request->parent_id,
                'kode' => $request->kode,
                'nama' => $request->nama,
                'adjustment' => $request->adjustment,
                'keterangan' => $request->keterangan,
            ])->id;

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }

    }

    public function checkUnique(Request $request){
        if(isset($request->id)){
            if($request->kode){
                $checked = Coa::where('kode', $request->kode)->where('id', '!=', $request->id)->first();
                if(isset($checked->kode)){
                    $checked = false;
                }else{
                    $checked = true;
                }
            }else{
                $checked = Coa::where('nama', $request->nama)->where('id', '!=', $request->id)->first();
                if(isset($checked->nama)){
                    $checked = false;
                }else{
                    $checked = true;
                }
            }
        }else{
            
        if($request->kode){
            $checked = Coa::where('kode', $request->kode)->first();
            if(isset($checked->kode)){
                $checked = false;
            }else{
                $checked = true;
            }
        }else{
            $checked = Coa::where('nama', $request->nama)->first();
            if(isset($checked->nama)){
                $checked = false;
            }else{
                $checked = true;
            }
        }
        
    }
        if($checked){
            return response()->json(['valid' => true]);
        }else{
            return response()->json(['valid' => false]);
        }
    }

    public function edit($id){
        // dd($id);
        $parentCoa = Coa::where('id', '!=', $id)->get();
        $isCoa = Coa::where('id', $id)->first();

        return view('admin.masterdata.coa.edit', ['parentCoa' => $parentCoa, 'isCoa'=>$isCoa]);
    }

    public function update(Request $request){
        try {
            DB::beginTransaction();
            Coa::where(['id' => $request->id])->update([
                'parent_id' => $request->parent_id,
                'kode' => $request->kode,
                'nama' => $request->nama,
                'adjustment' => $request->adjustment,
                'keterangan' => $request->keterangan,
            ]);

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Coa::where('id', $id)->delete();
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }
}
