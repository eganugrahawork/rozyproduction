<?php

namespace App\Http\Controllers\Admin\Masterdata\Uom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Uom;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class UomController extends Controller
{
    public function index(){
        return view('admin.masterdata.uom.index');
    }

    public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.nama LIKE '%" . $search . "%' OR a.simbol LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('uom')->count();
            $lists = DB::select("select a.* from uom as a  WHERE a.deleted_at is null $search group by a.id order by a.created_at desc limit $limit offset $offset");
            $totalRecords = count($lists);

            return DataTables::of($lists)
                ->addIndexColumn()
                ->addColumn('action', function ($model) {
                    $action =
                        '  <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
        data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
        <span class="svg-icon svg-icon-5 m-0">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                viewBox="0 0 24 24" fill="none">
                <path
                    d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                    fill="black" />
            </svg>
        </span></a>
    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-8 w-125px py-4"
        data-kt-menu="true">

        <div class="menu-item px-3">
        <a href="/admin/masterdata/uom/edit/' .
                        $model->id . '" class="menu-link px-3">Edit</a>
</div>
        <div class="menu-item px-3">
            <button type="button" class="menu-link px-3 btn btn-transparent fs-8" onclick="deleteData(' .
                        $model->id .
                        ', ' .
                        "'" .
                        $model->nama .
                        "'" .
                        ')">Delete</button>
        </div>
    </div>';
                    return $action;
                })
                ->rawColumns(['action', 'varian'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function create(){
        return view('admin.masterdata.uom.create');
    }

    public function store(Request $request){
        try {
            DB::beginTransaction();
            Uom::create([
                'simbol' => $request->simbol,
                'nama' => $request->nama,
                'keterangan' => $request->keterangan,
            ])->id;
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function edit($id){
        $isUom = Uom::where('id', $id)->first();
        return view('admin.masterdata.uom.edit', ['isUom' => $isUom]);
    }

    public function update(Request $request){
        try {
            DB::beginTransaction();
            Uom::where('id', $request->id)->update([
                'simbol' => $request->simbol,
                'nama' => $request->nama,
                'keterangan' => $request->keterangan,
            ]);
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function destroy($id){
        try {
            DB::beginTransaction();
            Uom::where('id', $id)->delete();
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }
}
