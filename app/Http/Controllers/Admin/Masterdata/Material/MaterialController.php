<?php


namespace App\Http\Controllers\Admin\Masterdata\Material;

use Carbon\Carbon;
use App\Models\Uom;
use App\Models\Material;
use App\Models\MaterialStok;
use Illuminate\Http\Request;
use App\Models\JenisMaterial;
use App\Models\MaterialDetail;
use App\Models\HistoryMaterial;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MaterialController extends Controller
{
    public function index(){
        return view('admin.masterdata.material.index');
    }

    public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.nama LIKE '%" . $search . "%' OR a.keterangan LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('material')->count();
            $lists = DB::select("SELECT MAX(e.id) AS isIt, a.*, b.warna,  c.nama as jenisnya, d.simbol
            FROM material AS a
           JOIN 
           (
               SELECT aa.id, GROUP_CONCAT(bb.warna SEPARATOR '<br>') AS warna 
                FROM material AS aa
                JOIN material_detail AS bb ON aa.id = bb.material_id AND bb.deleted_at IS NULL
                GROUP BY aa.id
           ) as b on a.id = b.id
           JOIN jenis_material as c ON a.jenis_material_id = c.id
           JOIN uom as d on a.uom_id = d.id
           LEFT JOIN history_materials AS e ON a.id = e.material_id AND e.jenis_history_id <> 1
           WHERE a.deleted_at is null $search group by a.id order by a.created_at desc limit $limit offset $offset");
            $totalRecords = count($lists);

            return DataTables::of($lists)
                ->addIndexColumn()
                ->addColumn('action', function ($model) {
                    $action =
                        '  <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
        data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
        <span class="svg-icon svg-icon-5 m-0">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                viewBox="0 0 24 24" fill="none">
                <path
                    d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                    fill="black" />
            </svg>
        </span></a>
    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-8 w-125px py-4"
        data-kt-menu="true">
            <div class="menu-item px-3">
                    <a href="/admin/masterdata/material/show/' .
                                    $model->id . '" class="menu-link px-3">Detail</a>
            </div>
            <div class="menu-item px-3">
        <a href="/admin/masterdata/material/edit/' .
        $model->id .'" class="menu-link px-3">Edit</a>
</div>
        ';
        if(!isset($model->isIt)){
$action .= '
        <div class="menu-item px-3">
            <button type="button" class="menu-link px-3 btn btn-transparent fs-8" onclick="deleteData(' .
                        $model->id .
                        ', ' .
                        "'" .
                        $model->nama .
                        "'" .
                        ')">Delete</button>
        </div>
    ';
}
$action .= '</div>';
return $action;
                    return $action;
                })
                ->rawColumns(['action', 'warna'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function create(){
        $isJenis = JenisMaterial::all();
        $isUom = Uom::all();
        return view('admin.masterdata.material.create', ['isJenis' => $isJenis, 'isUom' => $isUom]);
    }

    public function store(Request $request){
        try {
            DB::beginTransaction();
            $materialId = Material::create([
                'jenis_material_id' => $request->jenis_material_id,
                'uom_id' => $request->uom_id,
                'kode' => $request->kode,
                'nama' => $request->nama,
                'keterangan' => $request->keterangan,
            ])->id;

            foreach($request->warna as $i => $value){
                $materialDetailId = MaterialDetail::create([
                    'material_id' => $materialId,
                    'warna' => $request->warna[$i],
                    'status' => 1,
                ])->id;

                // Belum ada history
                MaterialStok::create([
                    'material_id' => $materialId,
                    'material_detail_id' => $materialDetailId,
                    'stok_awal' => $request->stok_awal[$i],
                    'stok_masuk' => 0,
                    'stok_keluar' => 0,
                    'stok_akhir' =>  $request->stok_awal[$i],
                    'status' => 0,
                ]);

                HistoryMaterial::create([
                    'material_id' => $materialId,
                    'material_detail_id' => $materialDetailId,
                    'tanggal_history' => Carbon::now(),
                    'tipe_history' => 0,
                    'jenis_history_id' => 1,
                    'detail_id_dari_jenis' => $materialDetailId,
                    'harga' => str_replace(',', '',$request->harga_stok_awal[$i]),
                    'stok_sebelum' => '0',
                    'stok_sesudah' =>$request->stok_awal[$i],
                    'qty' => $request->stok_awal[$i],
                    'keterangan' => "Saldo Awal",
                    'note' => "Input Pertama Material",
                    'user_id' => auth()->user()->id,
                ]);

            }
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function edit($id){
        $isMaterial = DB::select("SELECT a.id, a.uom_id, a.jenis_material_id, a.kode, 
        a.nama,a.keterangan,b.id AS material_detail_id, b.warna, FORMAT(c.stok_awal,0) as stok_awal,
        MAX(d.id) AS id_history, e.harga
        FROM material AS a
        JOIN material_detail AS b ON a.id = b.material_id and b.deleted_at is null
        JOIN material_stok as c on c.material_id = a.id and c.material_detail_id = b.id
        LEFT JOIN history_materials AS d ON a.id = d.material_id AND d.jenis_history_id <> 1 AND b.id = d.material_detail_id
         JOIN history_materials AS e ON a.id = e.material_id AND e.jenis_history_id = 1 AND b.id = e.material_detail_id
        WHERE a.id = $id
        GROUP BY b.id");
        $isUom = Uom::all();
        $isJenis = JenisMaterial::all();
        return view('admin.masterdata.material.edit', ['isMaterial' => $isMaterial, 'isJenis' => $isJenis, 'isUom'=>$isUom]);
    }

    public function update(Request $request){
        try {
            DB::beginTransaction();
             Material::where(['id' => $request->id])->update([
                'jenis_material_id' => $request->jenis_material_id,
                'uom_id' => $request->uom_id,
                'kode' => $request->kode,
                'nama' => $request->nama,
                'keterangan' => $request->keterangan,
            ]);

            if(isset($request->material_detail_id)){
                $inDb = MaterialDetail::where(['material_id' => $request->id])->count();
                $inReq = count($request->material_detail_id);
                if($inDb > $inReq){
                    MaterialDetail::where('material_id', $request->id)
                    ->whereNotIn('id', $request->material_detail_id)
                    ->delete();

                    MaterialStok::where('material_id', $request->id)
                    ->whereNotIn('material_detail_id', $request->material_detail_id)
                    ->delete();
                    HistoryMaterial::where(['material_id' => $request->id, 'jenis_history_id' => 1])
                    ->whereNotIn('material_detail_id', $request->material_detail_id)->delete();
                }
            }else{
                MaterialDetail::where('material_id', $request->id)->delete();
                MaterialStok::where('material_id', $request->id)->delete();
                HistoryMaterial::where(['material_id' => $request->id, 'jenis_history_id' => 1])
                ->delete();
            }

            foreach($request->warna as $i => $value){
                if(isset($request->material_detail_id[$i])){
                    MaterialDetail::where(['id' => $request->material_detail_id[$i]])->update([
                    'material_id' => $request->id,
                    'warna' => $request->warna[$i],
                    ]);

                    if(!isset($request->id_history[$i])){

                        MaterialStok::where(['material_id' => $request->id, 'material_detail_id' => $request->material_detail_id[$i]])->update([
                            'stok_awal' => $request->stok_awal[$i],
                            'stok_akhir' => $request->stok_awal[$i],
                        ]);

                        HistoryMaterial::where(['material_id' => $request->id, 'jenis_history_id' => 1, 'material_detail_id' => $request->material_detail_id[$i]])->update([
                            'stok_sesudah' => $request->stok_awal[$i],
                            'harga' => str_replace(',', '',$request->harga_stok_awal[$i]),
                            'qty' => $request->stok_awal[$i],
                        ]);
                
                    }

                }else{
                    $materialDetailId = MaterialDetail::create([
                    'material_id' => $request->id,
                    'warna' => $request->warna[$i],
                    ])->id;

                    MaterialStok::create([
                        'material_id' => $request->id,
                        'material_detail_id' => $materialDetailId,
                        'stok_awal' => $request->stok_awal[$i],
                        'stok_masuk' => 0,
                        'stok_keluar' => 0,
                        'stok_akhir' => $request->stok_awal[$i],
                        'status' => 1,
                    ]);
                    
                    HistoryMaterial::create([
                        'material_id' =>$request->id,
                        'material_detail_id' => $materialDetailId,
                        'tanggal_history' => Carbon::now(),
                        'tipe_history' => 0,
                        'jenis_history_id' => 1,
                        'detail_id_dari_jenis' => $materialDetailId,
                        'harga' => str_replace(',', '',$request->harga_stok_awal[$i]),
                        'stok_sebelum' => '0',
                        'stok_sesudah' =>$request->stok_awal[$i],
                        'qty' => $request->stok_awal[$i],
                        'keterangan' => "Saldo Awal",
                        'note' => "Input Pertama Material",
                        'user_id' => auth()->user()->id,
                    ]);

                }
            }

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }


    public function check_code(Request $request){
        if($request->kode){
            if ($request->kode_sebelumnya) {
                $code = Material::where(['kode' => $request->kode])
                    ->where('kode', '!=', $request->kode_sebelumnya)
                    ->first();
            } else {
                $code = Material::where(['kode' => $request->kode])->first();
            }
            if ($code) {
                return response()->json(['valid' => false]);
            }
            return response()->json(['valid' => true]);

        }elseif ($request->nama) {
            if ($request->nama_sebelumnya) {
                $nama = Material::where(['nama' => $request->nama])
                    ->where('nama', '!=', $request->nama_sebelumnya)
                    ->first();
            } else {
                $nama = Material::where(['nama' => $request->nama])->first();
            }
            if ($nama) {
                return response()->json(['valid' => false]);
            }
            return response()->json(['valid' => true]);
        }
    }


    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Material::where('id', $id)->delete();
            MaterialDetail::where('material_id', $id)->delete();
            MaterialStok::where('material_id', $id)->delete();
            HistoryMaterial::where('material_id', $id)->delete();
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function show($id){
        $isMaterial = DB::select("SELECT a.id, a.uom_id, a.jenis_material_id, a.kode, 
        a.nama,a.keterangan,b.id AS material_detail_id, b.warna, FORMAT(c.stok_awal,0) as stok_awal, e.harga
        FROM material AS a
        JOIN material_detail AS b ON a.id = b.material_id and b.deleted_at is null
        JOIN material_stok as c on c.material_id = a.id and c.material_detail_id = b.id
        LEFT JOIN history_materials AS d ON a.id = d.material_id AND d.jenis_history_id <> 1 AND b.id = d.material_detail_id
        JOIN history_materials AS e ON a.id = e.material_id AND e.jenis_history_id = 1 AND b.id = e.material_detail_id
        WHERE a.id = $id
        GROUP BY b.id");
        $isUom = Uom::all();
        $isJenis = JenisMaterial::all();
        return view('admin.masterdata.material.show', ['isMaterial' => $isMaterial, 'isJenis' => $isJenis, 'isUom'=>$isUom]);
    }
}
