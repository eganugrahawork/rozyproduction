<?php

namespace App\Http\Controllers\Admin\Masterdata\Pricing;

use Carbon\Carbon;
use App\Models\Produk;
use App\Models\HargaProduk;
use Illuminate\Http\Request;
use App\Models\MediaPenjualan;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\KategoriMediaPenjualan;

class HargaProdukController extends Controller
{
    public function index()
    {
        return view('admin.masterdata.pricing.index');
    }

    public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.nama LIKE '%" . $search . "%' OR a.keterangan LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('harga_produks')->count();
            $lists = DB::select("SELECT DATE(a.tanggal_diubah) as tanggal_digunakan, b.nama AS produk, c.nama AS kategori, d.nama AS media, a.* 
            FROM harga_produks AS a
           JOIN produk AS b ON b.id = a.produk_id 
           JOIN kategori_media_penjualans	AS c ON a.kategori_media_penjualan_id = c.id
           JOIN media_penjualans AS d ON a.media_penjualan_id = d.id
			  WHERE a.deleted_at is null $search GROUP BY a.id order by a.created_at desc limit $limit offset $offset");
            $totalRecords = count($lists);

            return DataTables::of($lists)
                ->addIndexColumn()
                ->addColumn('action', function ($model) {
                    $action = '<a href="/admin/masterdata/pricing/'.$model->id.'" class="btn btn-sm btn-light btn-active-light-primary">Detail</a>';
                    
                    return $action;
                })->addColumn('status', function ($model) {
                    if ($model->status == 0){
                        $status = '<span class="badge badge-light-danger fw-bolder">Tidak Aktif</span>';
                    }else{
                        $status = '<span class="badge badge-light-success fw-bolder">Aktif</span>';

                    }

                    return $status;

                })
                ->rawColumns(['action', 'status'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function create(){
        $isKategori = KategoriMediaPenjualan::all();
        $isProduk = Produk::all();
        return view('admin.masterdata.pricing.create', ['isKategori' => $isKategori, 'isProduk' => $isProduk]);
    }

    public function get_media($id){
        $media = MediaPenjualan::where('kategori_media_penjualan_id', $id)->get();

        return response()->json(['data'=>$media]);

    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $harga = HargaProduk::where(['produk_id' => $request->produk_id,
                'kategori_media_penjualan_id' => $request->kategori_media_penjualan_id,
                'media_penjualan_id' => $request->media_penjualan_id, 'status'=>1])->first();
                $hpp_sebelumnya = 0;
                $harga_jual_sebelumnya = 0;

                if(isset($harga->id)){
                    $hpp_sebelumnya = $harga->hpp;
                    $harga_jual_sebelumnya = $harga->harga_jual;
                    HargaProduk::where('id', $harga->id)->update(['status'=> 0]);
                }


            HargaProduk::create([
                'produk_id' => $request->produk_id,
                'kategori_media_penjualan_id' => $request->kategori_media_penjualan_id,
                'media_penjualan_id' => $request->media_penjualan_id,
                'hpp_sebelumnya' => $hpp_sebelumnya ,
                'hpp' =>  str_replace(',', '',$request->hpp),
                'harga_jual' =>  str_replace(',', '',$request->harga_jual),
                'harga_jual_sebelumnya' => $harga_jual_sebelumnya,
                'status' => 1,
                'tanggal_diubah' =>  Carbon::now(),
            ]);
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

}
