<?php

namespace App\Http\Controllers\Admin\Produksi\Finishing;

use Carbon\Carbon;
use App\Models\Partner;
use App\Models\Sequence;
use App\Models\Finishing;
use App\Models\Warehouse;
use App\Models\ProdukStok;
use Illuminate\Http\Request;
use App\Models\HistoryProduk;
use App\Models\FinishingDetail;
use App\Models\HistoryMaterial;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class FInishingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.produksi.finishing.index');
    }

    /**
     * Show the form for creating a new resource.
     */

     public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.kode LIKE '%" . $search . "%' OR a.keterangan LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('finishing')->count();
            $lists = DB::select("SELECT a.id, DATE(a.tanggal) AS tanggal, a.kode, d.nama,
            GROUP_CONCAT(e.nama, ' : ', f.nama, ', <br>') AS produk,
            FORMAT(SUM(c.qty_jadi),2) AS jumlah_jahit,
            FORMAT(SUM(b.qty_jadi),2) AS jumlah_finishing,
            FORMAT(SUM(b.koreksi),2) AS koreksi_finishing, a.status, DATE(b.tanggal_selesai) as tanggal_selesai
            FROM finishing AS a
            JOIN finishing_details AS b ON a.id = b.finishing_id AND b.deleted_at IS NULL
            JOIN sewing_detail AS c ON b.sewing_detail_id = c.id
            JOIN partner AS d ON a.partner_id = d.id
            JOIN cutting_detail as ee ON c.cutting_detail_id = ee.id
            JOIN produk AS e ON ee.produk_id = e.id
            JOIN produk_detail AS f ON ee.produk_detail_id = f.id
            WHERE a.deleted_at IS NULL
            GROUP BY a.id
            ORDER BY a.tanggal DESC");
            $totalRecords = count($lists);

            return DataTables::of($lists)
                ->addIndexColumn()->addColumn('status', function($model){
                    if($model->status == 0){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-800 fs-8">'.$model->tanggal.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-warning fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Dalam Proses</div>
                        </div>
                        </div>';
                    }elseif($model->status == 2){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-primary fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Menunggu Proses</div>
                        </div>

                        </div>';
                    }elseif($model->status == 1){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal_selesai.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-success fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Selesai</div>
                        </div>
                        <div class="timeline-item">
														<div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal.'</div>
														<div class="timeline-badge">
															<i class="fa fa-genderless text-warning fs-5"></i>
														</div>
														<div class="timeline-content d-flex">
															<span class="fw-bolder text-gray-800 ps-3">Proses</span>
														</div>
													</div>
                        </div>';
                    }

                    return $status;
                })
                ->addColumn('action', function ($model) {
                    $action =
                        '  <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                            <span class="svg-icon svg-icon-5 m-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none">
                                    <path
                                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                        fill="black" />
                                </svg>
                            </span></a>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-8 w-125px py-4"
                            data-kt-menu="true">
                    ';

        if($model->status == 0 ){
            $action .= '    <div class="menu-item px-3">
                <a href="/admin/produksi/finishing/show/' .
                $model->id .'/1" class="menu-link px-3">Proses</a>
            </div>
            <div class="menu-item px-3">
                <a href="/admin/produksi/finishing/' .
                $model->id .'" class="menu-link px-3">Edit</a>
            </div>
            <div class="menu-item px-3">
                <button type="button" class="menu-link px-3 btn btn-transparent fs-8" onclick="deleteData(' .
                            $model->id .
                            ', ' .
                            "'" .
                            $model->nama .
                            "'" .
                            ')">Delete</button>
            </div>
            ';
        }elseif($model->status == 2){
            $action .= ' <div class="menu-item px-3">
            <a href="/admin/produksi/finishing/show/' .
            $model->id .'/1" class="menu-link px-3">Proses</a>
        </div>';
        }


        $action .= '
        <div class="menu-item px-3">
        <a href="/admin/produksi/finishing/show/' .
        $model->id .'/0" class="menu-link px-3">Info</a>
</div>  </div>';
                    return $action;
                })
                ->rawColumns(['action', 'status', 'produk'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

     public function getDetailForm(Request $request){
        $sewingDetail = DB::select('SELECT * FROM (
            SELECT f.id, e.nama AS partner, b.nama, c.nama AS nama_detail, FORMAT(f.qty_jadi,0) AS produk_jadi,
            h.id AS sd_id
             FROM cutting_detail AS a
                    JOIN produk AS b ON a.produk_id = b.id
                    JOIN produk_detail AS c ON a.produk_detail_id = c.id
                    JOIN cutting AS d ON a.cutting_id = d.id
                    JOIN sewing_detail AS f ON f.cutting_detail_id = a.id AND f.deleted_at IS NULL
                    JOIN sewing as g ON f.sewing_id = g.id AND g.deleted_at IS NULL
                     JOIN partner AS e ON g.partner_id = e.id
                    LEFT JOIN finishing_details as h ON h.sewing_detail_id = f.id AND h.deleted_at IS NULL
                    WHERE f.status = 1 AND a.deleted_at IS NULL
                    GROUP BY a.id
                    ) AS a WHERE a.sd_id IS NULL');

        return response()->json(['sewingDetail' => $sewingDetail]);
    }


    public function create()
    {
        $isSequence = Sequence::where(['tab' => 'Finishing'])->first();
        $isPartner = Partner::where(['kategori_partner_id' => 4])->get();
        $pengantar = Partner::where('kategori_partner_id', 5)->get();

        $isSequence = $isSequence->first . $isSequence->middle + 1 . $isSequence->last;
        return view('admin.produksi.finishing.create', ['isPartner' => $isPartner, 'isSequence' =>$isSequence, 'pengantar' => $pengantar]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $finishingId = Finishing::create([
                'kode'=> $request->kode,
                'partner_id'=> $request->partner_id,
                'tanggal'=> $request->tanggal,
                'keterangan'=> $request->keterangan,
                'pembuat'=> auth()->user()->id,
                'pengantar'=> $request->pengantar_id,

            ])->id;

            foreach($request->sewing_detail_id as $i => $value){
                FinishingDetail::create([
                    'finishing_id'=> $finishingId,
                    'sewing_detail_id'=> $request->sewing_detail_id[$i],
                    'harga'=> str_replace(',', '',$request->price[$i]),
                    'total_harga'=> str_replace(',', '',$request->jumlah[$i])
                ]);
            }

            $isSequence = Sequence::where(['tab' => 'Finishing'])->first();
            $SeqUpdate = $isSequence->middle + 1;
            Sequence::where(['tab' => 'Finishing'])->update(['middle' => $SeqUpdate]);

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id,  $isConfirm)
    {
        $isFinishing = DB::select("SELECT a.id, a.kode, a.partner_id, a.pengantar, a.tanggal,
 			a.keterangan, b.id AS finishing_detail_id,
 			f.nama as nama_partner, f.no_telp, f.alamat,
        b.harga, b.total_harga, format(b.qty_jadi,0 )AS qty_finish,  format(b.koreksi,0 )AS koreksi,
		  format(c.qty_jadi,0 )AS qty_jahit,
		  b.sewing_detail_id,
		  h.nama as nama_penjahit,
		  i.nama as varian, j.nama as produk,
		  k.nama as nama_pengantar, b.status,
          l.nama as warehouse
        FROM finishing AS a
        JOIN finishing_details AS b ON a.id = b.finishing_id AND b.deleted_at IS NULL
        JOIN sewing_detail AS c ON c.id = b.sewing_detail_id AND b.deleted_at IS NULL
        JOIN sewing AS d ON c.sewing_id = d.id
		  JOIN cutting_detail AS e ON c.cutting_detail_id = e.id AND c.deleted_at IS NULL
        JOIN partner as f ON a.partner_id = f.id
        JOIN cutting as g ON e.cutting_id = g.id
        JOIN partner AS h ON d.partner_id = h.id
        JOIN produk_detail as i ON i.id = e.produk_detail_id
        JOIN produk as j ON e.produk_id = j.id
        JOIN partner as k ON a.pengantar = k.id
        LEFT JOIN warehouses as l ON b.warehouse_id = l.id
        WHERE a.id = $id
        GROUP BY b.id
        ");
        $isPartner = Partner::where(['kategori_partner_id' => 3])->get();
        $pengantar = Partner::where('kategori_partner_id', 5)->get();
        $pembawa = Partner::where('kategori_partner_id', 5)->get();
        $warehouse = Warehouse::all();
        return view('admin.produksi.finishing.show', ['isPartner' => $isPartner, 'isFinishing' =>$isFinishing, 'pengantar' => $pengantar, 'isConfirm' => $isConfirm, 'pembawa' => $pembawa, 'warehouse' => $warehouse]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $isFinishing = DB::select("SELECT a.id, a.kode, a.partner_id, a.pengantar, a.tanggal, a.keterangan, b.id AS finishing_detail_id,
        b.harga, b.total_harga, format(c.qty_jadi,0 )AS produk_jadi, b.sewing_detail_id
        FROM finishing AS a
        JOIN finishing_details AS b ON a.id = b.finishing_id AND b.deleted_at IS NULL
        JOIN sewing_detail AS c ON b.sewing_detail_id = c.id
        WHERE a.id = $id
        GROUP BY b.id");
        $isPartner = Partner::where(['kategori_partner_id' => 4])->get();
        $pengantar = Partner::where('kategori_partner_id', 5)->get();
        $sewingDetail = DB::select("SELECT * FROM (
           SELECT aa.id, e.nama AS partner, b.nama, c.nama AS nama_detail, FORMAT(aa.qty_jadi,0) AS produk_jadi,
            f.id AS sd_id
            FROM sewing_detail AS aa
            JOIN sewing AS bb ON aa.sewing_id = bb.id
				JOIN cutting_detail as a ON aa.cutting_detail_id = a.id
            JOIN produk AS b ON a.produk_id = b.id
            JOIN produk_detail AS c ON a.produk_detail_id = c.id
            JOIN cutting AS d ON a.cutting_id = d.id
            JOIN partner AS e ON bb.partner_id = e.id
            JOIN finishing_details AS f ON f.sewing_detail_id = aa.id AND f.deleted_at IS NULL
            JOIN finishing AS g ON f.finishing_id = g.id
            WHERE g.id = $id AND aa.deleted_at IS NULL
            GROUP BY aa.id

            UNION ALL
        SELECT * FROM (
                    SELECT aa.id, e.nama AS partner, b.nama, c.nama AS nama_detail, FORMAT(aa.qty_jadi,0) AS produk_jadi,
                    f.id AS sd_id
                     FROM sewing_detail AS aa
                     JOIN sewing AS bb ON aa.sewing_id = bb.id AND aa.deleted_at IS null
				JOIN cutting_detail as a ON aa.cutting_detail_id = a.id
            JOIN produk AS b ON a.produk_id = b.id
            JOIN produk_detail AS c ON a.produk_detail_id = c.id
            JOIN cutting AS d ON a.cutting_id = d.id
            JOIN partner AS e ON bb.partner_id = e.id
           LEFT JOIN finishing_details AS f ON f.sewing_detail_id = aa.id AND f.deleted_at IS NULL
            LEFT JOIN finishing AS g ON f.finishing_id = g.id
                            WHERE aa.status = 1 AND aa.deleted_at IS NULL
                            GROUP BY aa.id
                            ) AS a WHERE a.sd_id IS NULL
        ) AS combined_query
        ");

        return view('admin.produksi.finishing.edit', ['isPartner' => $isPartner, 'isFinishing' =>$isFinishing, 'pengantar' => $pengantar, 'sewingDetail' => $sewingDetail]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
             Finishing::where(['id' => $request->id])->update([
                'kode'=> $request->kode,
                'partner_id'=> $request->partner_id,
                'tanggal'=> $request->tanggal,
                'pengantar'=> $request->pengantar_id,
                'keterangan'=> $request->keterangan,
                'pembuat' => auth()->user()->id,
            ]);

            if(isset($request->finishing_detail_id)){
                $inDb = FinishingDetail::where(['finishing_id' => $request->id])->count();
                $inReq = count($request->finishing_detail_id);
                if($inDb > $inReq){
                    FinishingDetail::where('finishing_id', $request->id)
                    ->whereNotIn('id', $request->finishing_detail_id)
                    ->delete();
                }
            }else{
                FinishingDetail::where('finishing_id', $request->id)->delete();
            }

            foreach($request->sewing_detail_id as $i => $value){
                if(isset($request->finishing_detail_id[$i])){
                    FinishingDetail::where(['id' => $request->finishing_detail_id[$i]])->update([
                        'sewing_detail_id'=> $request->sewing_detail_id[$i],
                        'harga'=> str_replace(',', '',$request->price[$i]),
                        'total_harga'=> str_replace(',', '',$request->jumlah[$i]),
                ]);
                }else{
                    FinishingDetail::create([
                        'finishing_id'=> $request->id,
                        'sewing_detail_id'=> $request->sewing_detail_id[$i],
                        'harga'=> str_replace(',', '',$request->price[$i]),
                        'total_harga'=> str_replace(',', '',$request->jumlah[$i]),
                        'status'=> 0
                    ]);
                }
            }

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Finishing::where('id', $id)->delete();
            FinishingDetail::where('finishing_id', $id)->delete();
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function proccess(Request $request){
        // dd($request);
        try {
            DB::beginTransaction();
            $koreksi = $request->qty_jadi - $request->qty_jahit ;
            $flag = 1;

            FinishingDetail::where('id', $request->finishing_detail_id)->update([
                'koreksi' => $koreksi,
                'tanggal_selesai' => $request->tanggal_selesai,
                'total_harga' => $request->qty_jadi * $request->harga,
                'qty_jadi' => $request->qty_jadi,
                'status' => 1,
                'pembawa' => $request->pembawa_id,
                'warehouse_id' => $request->warehouse_id,
            ]);

            // History
            $sewing = DB::select("SELECT a.sewing_detail_id, d.material_id,
                            d.material_detail_id,
                            b.harga AS harga_jahit,
                            b.qty_jadi as qty_jahit,
                            a.harga as harga_finishing,
                            c.produk_id,
                            c.produk_detail_id,
                            e.hpp,
                            b.tanggal_selesai
                            FROM finishing_details AS a
                            JOIN sewing_detail AS b ON a.sewing_detail_id = b.id
                            JOIN cutting_detail AS c ON b.cutting_detail_id = c.id
                            JOIN pengadaan_detail AS d ON c.pengadaan_detail_id = d.id
                            JOIN produk as e ON c.produk_id = e.id
                            WHERE a.deleted_at IS NULL AND a.id = $request->finishing_detail_id
                            GROUP BY a.id");
            $sewing = $sewing[0];

            // Belum update ke stok produk dan ke history produk belum.
            $stokProduk = ProdukStok::where( ['produk_id' => $sewing->produk_id,
            'produk_detail_id' => $sewing->produk_detail_id])->first();
            HistoryProduk::create([
                'produk_id' => $sewing->produk_id,
                'produk_detail_id' => $sewing->produk_detail_id,
                'tanggal_history' => $request->tanggal_selesai,
                'tipe_history' => 0,
                'jenis_history_id' => 6,
                'detail_id_dari_jenis' => $request->finishing_detail_id,
                'harga' => $sewing->hpp, // Sebenarnya ini bisa pakai rumus diambil harga rata rata.
                'stok_sebelum' => $stokProduk->stok_akhir,
                'qty' => $request->qty_jadi,
                'stok_sesudah' => $stokProduk->stok_akhir + $request->qty_jadi,
                'keterangan' => "Finishing",
                'note' => "Selesai Produksi",
                'user_id' => auth()->user()->id,
            ]);

            DB::select('UPDATE produk_stok SET stok_masuk = stok_masuk +'.$request->qty_jadi.', stok_akhir = stok_akhir + '.$request->qty_jadi.', updated_at = "'.Carbon::now()->format('Y-m-d H:i:s') .'" WHERE produk_id = '.$sewing->produk_id.' AND produk_detail_id = '.$sewing->produk_detail_id);

            // History Keluar dari Jahit
            HistoryMaterial::create([
                'material_id' => $sewing->material_id,
                'material_detail_id' => $sewing->material_detail_id,
                'tanggal_history' => $sewing->tanggal_selesai,
                'tipe_history' => 1,
                'jenis_history_id' => 4,
                'detail_id_dari_jenis' => $sewing->sewing_detail_id,
                'harga' => $sewing->harga_jahit,
                'qty' => $sewing->qty_jahit,
                'keterangan' => "Jahit",
                'note' => "Keluar Jahit",
                'user_id' => auth()->user()->id,
            ]);

            // History Masuk Finishing
            HistoryMaterial::create([
                'material_id' => $sewing->material_id,
                'material_detail_id' => $sewing->material_detail_id,
                'tanggal_history' => $request->tanggal_diserahkan,
                'tipe_history' => 0,
                'jenis_history_id' => 5,
                'detail_id_dari_jenis' => $request->finishing_detail_id,
                'harga' => $request->harga,
                'qty' => $request->qty_jadi,
                'keterangan' => "Finishing",
                'note' => "Masuk Finishing",
                'user_id' => auth()->user()->id,
            ]);

            // History Finishing
            HistoryMaterial::create([
                'material_id' => $sewing->material_id,
                'material_detail_id' => $sewing->material_detail_id,
                'tanggal_history' => $request->tanggal_selesai,
                'tipe_history' => 1,
                'jenis_history_id' => 5,
                'detail_id_dari_jenis' => $request->finishing_detail_id,
                'harga' => $request->harga,
                'qty' => $request->qty_jadi,
                'keterangan' => "Finishing",
                'note' => "Keluar Finishing",
                'user_id' => auth()->user()->id,
            ]);
            // End history

            $checkFinishing = DB::select('SELECT b.*
            FROM (SELECT a.id FROM finishing AS a
            JOIN finishing_details AS b ON a.id = b.finishing_id AND b.deleted_at IS NULL
            WHERE b.id = '.$request->finishing_detail_id.'
            GROUP BY a.id ) AS a
            JOIN finishing_details AS b ON b.finishing_id = a.id
            WHERE b.deleted_at IS NULL
            GROUP BY b.id');
            $id = $checkFinishing[0]->finishing_id;
            $status = 1;
            foreach($checkFinishing as $fin){
                if($fin->status == 0) {
                    $status = 0;
                }
            }

            if($status == 1){
                Finishing::where(['id' => $id])->update(['status' => 1]);
            }else{
                // 2 berarti sudah ada selesai tapi belum semua
                Finishing::where(['id' => $id])->update(['status' => 2]);
            }

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

}
