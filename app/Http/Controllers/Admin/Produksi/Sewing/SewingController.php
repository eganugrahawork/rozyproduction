<?php

namespace App\Http\Controllers\Admin\Produksi\Sewing;

use Carbon\Carbon;
use App\Models\HistoryMaterial;
use App\Models\Sewing;
use App\Models\Cutting;
use App\Models\Partner;
use App\Models\Sequence;
use App\Models\SewingDetail;
use Illuminate\Http\Request;
use App\Models\CuttingDetail;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SewingController extends Controller
{
    public function index(){
        return view('admin.produksi.sewing.index');
    }
    public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.nama LIKE '%" . $search . "%' OR a.keterangan LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('sewing')->count();
            $lists = DB::select("SELECT a.id, DATE(a.tanggal) AS tanggal, a.kode, d.nama,
            GROUP_CONCAT(e.nama, ' : ', f.nama, ', <br>') AS produk,
            FORMAT(SUM(c.jumlah_jadi),2) AS jumlah_cutting,
            FORMAT(SUM(b.qty_jadi),2) AS jumlah_jahit,
            FORMAT(SUM(b.koreksi),2) AS koreksi_jahit, a.status, DATE(b.tanggal_selesai) as tanggal_selesai
            FROM sewing AS a
            JOIN sewing_detail AS b ON a.id = b.sewing_id AND b.deleted_at IS NULL
            JOIN cutting_detail AS c ON b.cutting_detail_id = c.id
            JOIN partner AS d ON a.partner_id = d.id
            JOIN produk AS e ON c.produk_id = e.id
            JOIN produk_detail AS f ON c.produk_detail_id = f.id
            WHERE a.deleted_at IS NULL
            GROUP BY a.id
            ORDER BY a.tanggal DESC");
            $totalRecords = count($lists);

            return DataTables::of($lists)
                ->addIndexColumn()->addColumn('status', function($model){
                    if($model->status == 0){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-800 fs-8">'.$model->tanggal.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-warning fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Dalam Proses</div>
                        </div>
                        </div>';
                    }elseif($model->status == 2){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-primary fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Menunggu Proses</div>
                        </div>

                        </div>';
                    }elseif($model->status == 1){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal_selesai.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-success fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Selesai</div>
                        </div>
                        <div class="timeline-item">
														<div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal.'</div>
														<div class="timeline-badge">
															<i class="fa fa-genderless text-warning fs-5"></i>
														</div>
														<div class="timeline-content d-flex">
															<span class="fw-bolder text-gray-800 ps-3">Proses</span>
														</div>
													</div>
                        </div>';
                    }

                    return $status;
                })
                ->addColumn('action', function ($model) {
                    $action =
                        '  <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                            <span class="svg-icon svg-icon-5 m-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none">
                                    <path
                                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                        fill="black" />
                                </svg>
                            </span></a>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-8 w-125px py-4"
                            data-kt-menu="true">
                    ';

        if($model->status == 0 ){
            $action .= '    <div class="menu-item px-3">
                <a href="/admin/produksi/jahit/show/' .
                $model->id .'/1" class="menu-link px-3">Proses</a>
            </div>
            <div class="menu-item px-3">
                <a href="/admin/produksi/jahit/' .
                $model->id .'" class="menu-link px-3">Edit</a>
            </div>
            <div class="menu-item px-3">
                <button type="button" class="menu-link px-3 btn btn-transparent fs-8" onclick="deleteData(' .
                            $model->id .
                            ', ' .
                            "'" .
                            $model->nama .
                            "'" .
                            ')">Delete</button>
            </div>
            ';
        }elseif($model->status == 2){
            $action .= ' <div class="menu-item px-3">
            <a href="/admin/produksi/jahit/show/' .
            $model->id .'/1" class="menu-link px-3">Proses</a>
        </div>';
        }


        $action .= '
        <div class="menu-item px-3">
        <a href="/admin/produksi/jahit/show/' .
        $model->id .'/0" class="menu-link px-3">Info</a>
</div>  </div>';
                    return $action;
                })
                ->rawColumns(['action', 'status', 'produk'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function create(){
        $isSequence = Sequence::where(['tab' => 'Sewing'])->first();
        $isPartner = Partner::where(['kategori_partner_id' => 3])->get();
        $pengantar = Partner::where('kategori_partner_id', 5)->get();

        $isSequence = $isSequence->first . $isSequence->middle + 1 . $isSequence->last;
        return view('admin.produksi.sewing.create', ['isPartner' => $isPartner, 'isSequence' =>$isSequence, 'pengantar' => $pengantar]);
    }

    public function getDetailForm(Request $request){
        $cuttingDetail = DB::select('SELECT * FROM (
            SELECT a.id, e.nama AS partner, b.nama, c.nama AS nama_detail, FORMAT(a.jumlah_jadi,0) AS bahan_jadi,
            f.id AS sd_id
             FROM cutting_detail AS a
                    JOIN produk AS b ON a.produk_id = b.id
                    JOIN produk_detail AS c ON a.produk_detail_id = c.id
                    JOIN cutting AS d ON a.cutting_id = d.id
                    JOIN partner AS e ON d.partner_id = e.id
                    LEFT JOIN sewing_detail AS f ON f.cutting_detail_id = a.id AND f.deleted_at IS NULL
                    WHERE a.status = 1 AND a.deleted_at IS NULL
                    GROUP BY a.id
                    ) AS a WHERE a.sd_id IS NULL');

        return response()->json(['cuttingDetail' => $cuttingDetail]);
    }

    public function store(Request $request){
        // dd($request);
        // Validasi jika memasukan cutting detail id yang sama belum ada (06-10-2024)
        try {
            DB::beginTransaction();
            $sewingId = Sewing::create([
                'kode'=> $request->kode,
                'partner_id'=> $request->partner_id,
                'tanggal'=> $request->tanggal,
                'keterangan'=> $request->keterangan,
                'pembuat'=> auth()->user()->id,
                'pengantar'=> $request->pengantar_id,

            ])->id;

            foreach($request->cutting_detail_id as $i => $value){
                SewingDetail::create([
                    'sewing_id'=> $sewingId,
                    'cutting_detail_id'=> $request->cutting_detail_id[$i],
                    'harga'=> str_replace(',', '',$request->price[$i]),
                    'total_harga'=> str_replace(',', '',$request->jumlah[$i])
                ]);
            }

            $isSequence = Sequence::where(['tab' => 'Sewing'])->first();
            $SeqUpdate = $isSequence->middle + 1;
            Sequence::where(['tab' => 'Sewing'])->update(['middle' => $SeqUpdate]);

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function edit($id){
        $isSewing = DB::select("SELECT a.id, a.kode, a.partner_id, a.pengantar, a.tanggal, a.keterangan, b.id AS sewing_detail_id,
        b.harga, b.total_harga, format(c.jumlah_jadi,0 )AS jumlah_cutting, b.cutting_detail_id
        FROM sewing AS a
        JOIN sewing_detail AS b ON a.id = b.sewing_id AND b.deleted_at IS NULL
        JOIN cutting_detail AS c ON b.cutting_detail_id = c.id
        WHERE a.id = $id
        GROUP BY b.id");
        $isPartner = Partner::where(['kategori_partner_id' => 3])->get();
        $pengantar = Partner::where('kategori_partner_id', 5)->get();
        $cuttingDetail = DB::select("SELECT * FROM (
            SELECT a.id, e.nama AS partner, b.nama, c.nama AS nama_detail, FORMAT(a.jumlah_jadi,0) AS bahan_jadi,
            f.id AS sd_id
            FROM cutting_detail AS a
            JOIN produk AS b ON a.produk_id = b.id
            JOIN produk_detail AS c ON a.produk_detail_id = c.id
            JOIN cutting AS d ON a.cutting_id = d.id
            JOIN partner AS e ON d.partner_id = e.id
            JOIN sewing_detail AS f ON f.cutting_detail_id = a.id AND f.deleted_at IS NULL
            JOIN sewing AS g ON f.sewing_id = g.id
            WHERE g.id = $id AND a.deleted_at IS NULL
            GROUP BY a.id

            UNION ALL
        SELECT * FROM (
                    SELECT a.id, e.nama AS partner, b.nama, c.nama AS nama_detail, FORMAT(a.jumlah_jadi,0) AS bahan_jadi,
                    f.id AS sd_id
                     FROM cutting_detail AS a
                            JOIN produk AS b ON a.produk_id = b.id
                            JOIN produk_detail AS c ON a.produk_detail_id = c.id
                            JOIN cutting AS d ON a.cutting_id = d.id
                            JOIN partner AS e ON d.partner_id = e.id
                            LEFT JOIN sewing_detail AS f ON f.cutting_detail_id = a.id AND f.deleted_at IS NULL
                            WHERE a.status = 1 AND a.deleted_at IS NULL
                            GROUP BY a.id
                            ) AS a WHERE a.sd_id IS NULL
        ) AS combined_query;
        ");

        return view('admin.produksi.sewing.edit', ['isPartner' => $isPartner, 'isSewing' =>$isSewing, 'pengantar' => $pengantar, 'cuttingDetail' => $cuttingDetail]);

    }
    public function update(Request $request){
        // dd($request);
        try {
            DB::beginTransaction();
             Sewing::where(['id' => $request->id])->update([
                'kode'=> $request->kode,
                'partner_id'=> $request->partner_id,
                'tanggal'=> $request->tanggal,
                'keterangan'=> $request->keterangan,
                'pembuat'=> auth()->user()->id,
                'pengantar'=> $request->pengantar_id,
            ]);

            if(isset($request->sewing_detail_id)){
                $inDb = SewingDetail::where(['sewing_id' => $request->id])->count();
                $inReq = count($request->sewing_detail_id);
                if($inDb > $inReq){
                    SewingDetail::where('sewing_id', $request->id)
                    ->whereNotIn('id', $request->sewing_detail_id)
                    ->delete();
                }
            }else{
                SewingDetail::where('sewing_id', $request->id)->delete();
            }

            foreach($request->cutting_detail_id as $i => $value){
                if(isset($request->sewing_detail_id[$i])){
                    SewingDetail::where(['id' => $request->sewing_detail_id[$i]])->update([
                        'cutting_detail_id'=> $request->cutting_detail_id[$i],
                        'harga'=> str_replace(',', '',$request->price[$i]),
                        'total_harga'=> str_replace(',', '',$request->jumlah[$i])
                ]);
                }else{
                    SewingDetail::create([
                        'sewing_id'=> $request->id,
                        'cutting_detail_id'=> $request->cutting_detail_id[$i],
                        'harga'=> str_replace(',', '',$request->price[$i]),
                        'total_harga'=> str_replace(',', '',$request->jumlah[$i])
                    ]);
                }
            }

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function show($id, $isConfirm){
        $isSewing = DB::select("SELECT a.id, a.kode, a.partner_id, a.pengantar, a.tanggal, a.keterangan, b.id AS sewing_detail_id, d.nama as nama_partner, d.no_telp, d.alamat,
        b.harga, b.total_harga, format(c.jumlah_jadi,0 )AS jumlah_cutting, b.cutting_detail_id, f.nama as nama_pemotong, g.nama as varian, h.nama as produk, i.nama as nama_pengantar, b.status
        FROM sewing AS a
        JOIN sewing_detail AS b ON a.id = b.sewing_id AND b.deleted_at IS NULL
        JOIN cutting_detail AS c ON b.cutting_detail_id = c.id AND c.deleted_at IS NULL
        JOIN partner as d ON a.partner_id = d.id
        JOIN cutting as e ON c.cutting_id = e.id
        JOIN partner as f ON e.partner_id = f.id
        JOIN produk_detail as g ON g.id = c.produk_detail_id
        JOIN produk as h ON g.produk_id = h.id
        JOIN partner as i ON a.pengantar = i.id
        WHERE a.id = $id
        GROUP BY b.id");
        $isPartner = Partner::where(['kategori_partner_id' => 3])->get();
        $pengantar = Partner::where('kategori_partner_id', 5)->get();
        $pembawa = Partner::where('kategori_partner_id', 5)->get();
        return view('admin.produksi.sewing.show', ['isPartner' => $isPartner, 'isSewing' =>$isSewing, 'pengantar' => $pengantar, 'isConfirm' => $isConfirm, 'pembawa' => $pembawa]);
    }

    public function destroy($id){
        try {
            DB::beginTransaction();
            Sewing::where('id', $id)->delete();
            SewingDetail::where('sewing_id', $id)->delete();
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function proccess(Request $request){
        // dd($request);
        try {
            DB::beginTransaction();
            $koreksi = $request->qty_jadi - $request->jumlah_cutting ;
            $flag = 1;

            if ($koreksi >= 0 && $koreksi <= 1) {
                $flag = 1;
            } elseif ($koreksi > 1) {
                $flag = 4;
            } elseif ($koreksi < 0 && $koreksi >= -2) {
                $flag = 2;
            } elseif ($koreksi < -2) {
                $flag = 3;
            }

            SewingDetail::where('id', $request->sewing_detail_id)->update([
                'koreksi' => $koreksi,
                'tanggal_selesai' => $request->tanggal_selesai,
                'total_harga' => $request->qty_jadi * $request->harga_jahit,
                'qty_jadi' => $request->qty_jadi,
                'status' => 1,
                'pembawa' => $request->pembawa_id,
            ]);

            // history
            $cutting = DB::select("SELECT a.cutting_detail_id, c.material_id, c.material_detail_id, b.harga AS harga_cutting, b.jumlah_jadi as jumlah_cutting, a.harga as harga_jahit, b.tanggal_selesai
            FROM sewing_detail AS a
            JOIN cutting_detail AS b ON a.cutting_detail_id = b.id
            JOIN pengadaan_detail AS c ON b.pengadaan_detail_id = c.id
            WHERE a.deleted_at IS NULL AND a.id = $request->sewing_detail_id
            GROUP BY a.id");
            $cutting = $cutting[0];

            // History Keluar dari Cutting
            HistoryMaterial::create([
                'material_id' => $cutting->material_id,
                'material_detail_id' => $cutting->material_detail_id,
                'tanggal_history' => $cutting->tanggal_selesai,
                'tipe_history' => 1,
                'jenis_history_id' => 3,
                'detail_id_dari_jenis' => $cutting->cutting_detail_id,
                'harga' => $cutting->harga_cutting,
                'qty' => $cutting->jumlah_cutting,
                'keterangan' => "Cutting",
                'note' => "Keluar Cutting",
                'user_id' => auth()->user()->id,
            ]);

            // History Masuk Jahit
            HistoryMaterial::create([
                'material_id' => $cutting->material_id,
                'material_detail_id' => $cutting->material_detail_id,
                'tanggal_history' => $request->tanggal_diserahkan,
                'tipe_history' => 0,
                'jenis_history_id' => 4,
                'detail_id_dari_jenis' => $request->sewing_detail_id,
                'harga' => $cutting->harga_jahit,
                'qty' => $request->qty_jadi,
                'keterangan' => "Jahit",
                'note' => "Masuk Jahit",
                'user_id' => auth()->user()->id,
            ]);

            // End HIstory

            $checkSewing = DB::select('SELECT b.* FROM (SELECT a.id FROM sewing AS a
            JOIN sewing_detail AS b ON a.id = b.sewing_id AND b.deleted_at IS NULL
            WHERE b.id = '.$request->sewing_detail_id.'
            GROUP BY a.id ) AS a
            JOIN sewing_detail AS b ON b.sewing_id = a.id
              WHERE b.deleted_at IS NULL
            GROUP BY b.id');
            $id = $checkSewing[0]->sewing_id;
            $status = 1;
            foreach($checkSewing as $cut){
                if($cut->status == 0) {
                    $status = 0;
                }
            }

            if($status == 1){
                Sewing::where(['id' => $id])->update(['status' => 1]);
            }else{
                Sewing::where(['id' => $id])->update(['status' => 2]);
            }

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }


}
