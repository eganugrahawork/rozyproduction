<?php

namespace App\Http\Controllers\Admin\Produksi\Cutting;

use Carbon\Carbon;
use App\Models\Cutting;
use App\Models\Partner;
use App\Models\Sequence;
use App\Models\Pengadaan;
use App\Models\MaterialStok;
use Illuminate\Http\Request;
use App\Models\CuttingDetail;
use App\Models\JenisMaterial;
use App\Models\HistoryMaterial;
use App\Models\PengadaanDetail;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CuttingController extends Controller
{
    public function index(){
        return view('admin.produksi.cutting.index');
    }

    public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.nama LIKE '%" . $search . "%' OR a.keterangan LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('cutting')->where('deleted_at', 'is null')->get()->count();
            $lists = DB::select("SELECT a.id,DATE(a.tanggal) as tanggal, a.kode, c.kode AS kode_pengadaan, e.nama AS partner,
            GROUP_CONCAT(CONCAT(f.nama, '-', FORMAT(d.qty,2), j.simbol, ' => ', g.nama, '-', h.nama ) SEPARATOR '<br>') AS detail,
            SUM(d.qty) AS jumlah_material, a.status, DATE(b.tanggal_selesai) as tanggal_selesai
            FROM cutting AS a
            JOIN cutting_detail AS b ON a.id = b.cutting_id AND b.deleted_at is null
            JOIN pengadaan AS c ON a.pengadaan_id = c.id
            JOIN pengadaan_detail AS d ON c.id = d.pengadaan_id AND b.pengadaan_detail_id = d.id
            JOIN partner AS e ON a.partner_id = e.id
            JOIN material AS f ON d.material_id = f.id
            JOIN produk AS g ON b.produk_id = g.id
            JOIN produk_detail AS h ON g.id = h.produk_id AND b.produk_detail_id = h.id
            JOIN uom AS j ON f.uom_id = j.id
            WHERE a.deleted_at is null $search
            GROUP BY a.id order by a.tanggal desc  limit $limit offset $offset");
            $totalRecords = count($lists);

            return DataTables::of($lists)
                ->addIndexColumn()->addColumn('status', function($model){
                    if($model->status == 0){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-800 fs-8">'.$model->tanggal.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-warning fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Dalam Proses</div>
                        </div>
                        </div>';
                    }elseif($model->status == 2){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-primary fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Menunggu Proses</div>
                        </div>

                        </div>';
                    }elseif($model->status == 1){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal_selesai.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-success fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Selesai</div>
                        </div>
                        <div class="timeline-item">
														<div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal.'</div>
														<div class="timeline-badge">
															<i class="fa fa-genderless text-warning fs-5"></i>
														</div>
														<div class="timeline-content d-flex">
															<span class="fw-bolder text-gray-800 ps-3">Proses</span>
														</div>
													</div>
                        </div>';
                    }

                    return $status;
                })
                ->addColumn('action', function ($model) {
                    $action =
                        '  <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                            <span class="svg-icon svg-icon-5 m-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none">
                                    <path
                                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                        fill="black" />
                                </svg>
                            </span></a>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-8 w-125px py-4"
                            data-kt-menu="true">
                    ';

        if($model->status == 0 ){
            $action .= '    <div class="menu-item px-3">
                <a href="/admin/produksi/cutting/show/' .
                $model->id .'/1" class="menu-link px-3">Proses</a>
            </div>
            <div class="menu-item px-3">
                <a href="/admin/produksi/cutting/' .
                $model->id .'" class="menu-link px-3">Edit</a>
            </div>
            <div class="menu-item px-3">
                <button type="button" class="menu-link px-3 btn btn-transparent fs-8" onclick="deleteData(' .
                            $model->id .
                            ', ' .
                            "'" .
                            $model->partner .
                            "'" .
                            ')">Delete</button>
            </div>
            ';
        }elseif($model->status == 2){
            $action .= ' <div class="menu-item px-3">
            <a href="/admin/produksi/cutting/show/' .
            $model->id .'/1" class="menu-link px-3">Proses</a>
        </div>';
        }


        $action .= '
        <div class="menu-item px-3">
        <a href="/admin/produksi/cutting/show/' .
        $model->id .'/0" class="menu-link px-3">Info</a>
</div>  </div>';
                    return $action;
                })
                ->rawColumns(['action', 'status', 'detail'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function create(){
        $isSequence = Sequence::where(['tab' => 'Cutting'])->first();
        $isPartner = Partner::where(['kategori_partner_id' => 2])->get();
        $pengantar = Partner::where('kategori_partner_id', 5)->get();
        $isPengadaan = DB::select("SELECT * FROM (
            SELECT a.id, a.kode , a.tanggal, b.id AS pd_id, c.id AS cd_id FROM pengadaan AS a
            LEFT JOIN pengadaan_detail AS b ON a.id = b.pengadaan_id AND b.deleted_at IS NULL
            LEFT JOIN cutting_detail AS c ON b.id = c.pengadaan_detail_id AND c.deleted_at IS NULL
            WHERE jenis_material_id = 1 AND a.status = 1
            GROUP BY b.id
            ) AS a
            WHERE cd_id IS NULL
            GROUP BY id
            ");

        $isSequence = $isSequence->first . $isSequence->middle + 1 . $isSequence->last;
        return view('admin.produksi.cutting.create', ['isPartner' => $isPartner, 'isSequence' =>$isSequence, 'isPengadaan' => $isPengadaan, 'pengantar' => $pengantar]);
    }

    public function store(Request $request){
                // Validasi jika memasukan pengadaan detail id yang sama belum ada (06-10-2024)
        // Jangan lupa check status ke pengadaan detail untuk mengubah statusnya menjadi dalam proses cutting.
        // dd($request);
        try {
            DB::beginTransaction();
            $cuttingId = Cutting::create([
                'kode' => $request->kode,
                'pengadaan_id' => $request->pengadaan_id,
                'partner_id' => $request->partner_id,
                'pengantar' => $request->pengantar_id,
                'tanggal' => $request->tanggal,
                'keterangan' => $request->keterangan,
                'status' => 0,
                'pembuat' => auth()->user()->id
            ])->id;

            foreach($request->pengadaan_detail_id as $i => $value){
                CuttingDetail::create([
                    'cutting_id'=> $cuttingId,
                    'pengadaan_detail_id'=> $request->pengadaan_detail_id[$i],
                    'produk_id'=> $request->produk_id[$i],
                    'produk_detail_id'=> $request->varian_id[$i],
                    'harga'=> str_replace(',', '',$request->price[$i]),
                    'est_jadi'=> $request->est_jadi[$i],
                    'est_sisa'=> $request->est_sisa[$i],
                    'status'=> 0
                ]);
            }

            $isSequence = Sequence::where(['tab' => 'Cutting'])->first();
            $SeqUpdate = $isSequence->middle + 1;
            Sequence::where(['tab' => 'Cutting'])->update(['middle' => $SeqUpdate]);

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function getDetailForm($id){
        $pengadaanDetail =  DB::select('SELECT * FROM (
            SELECT
                   a.id, a.pengadaan_id, a.material_id, a.material_detail_id, FORMAT(a.qty,2) AS qty, a.price, b.nama, c.warna, d.simbol AS uom,   e.id AS cutting_detail_id
                    FROM pengadaan_detail AS a
                    JOIN material AS b ON a.material_id = b.id
                    JOIN material_detail AS c ON a.material_detail_id = c.id
                    JOIN uom AS d ON b.uom_id = d.id
                    LEFT JOIN cutting_detail AS e ON a.id = e.pengadaan_detail_id AND e.deleted_at IS NULL
                    WHERE a.pengadaan_id = '.$id.'
                     AND a.deleted_at IS NULL AND a.status = 0
                     GROUP BY a.id
                     ) AS its WHERE cutting_detail_id IS NULL');

        return response()->json(['pengadaanDetail' => $pengadaanDetail]);
    }

    public function edit($id){
        $isCutting = DB::select("SELECT a.id, a.kode, a.partner_id, a.pengadaan_id, a.tanggal, b.pengadaan_detail_id, b.produk_id, b.produk_detail_id, FORMAT(b.est_jadi,2) as est_jadi, format(b.harga,2) as harga, format(b.est_sisa,2) as est_sisa, c.qty as qty_yard, b.id as cutting_detail_id, a.pengantar
         FROM cutting AS a
        JOIN cutting_detail AS b ON a.id = b.cutting_id AND b.deleted_at IS NULL
        JOIN pengadaan_detail as c ON b.pengadaan_detail_id = c.id
        WHERE a.id = $id
        GROUP BY b.id");
        $isPartner = Partner::where(['kategori_partner_id' => 2])->get();
        $isPengadaan = Pengadaan::where(['jenis_material_id' => 1, 'status' => 1])->get();
        $pengantar = Partner::where('kategori_partner_id', 5)->get();
        $isPengadaanDetail = DB::select("SELECT
        a.id, a.pengadaan_id, a.material_id, a.material_detail_id, FORMAT(a.qty,2) as qty, a.price, b.nama, c.warna, d.simbol AS uom
        FROM pengadaan_detail AS a
        JOIN material AS b ON a.material_id = b.id
        JOIN material_detail AS c ON a.material_detail_id = c.id
        JOIN uom AS d ON b.uom_id = d.id
        WHERE a.pengadaan_id = ". $isCutting[0]->pengadaan_id ." AND a.deleted_at IS NULL AND a.status = 0");

        return view('admin.produksi.cutting.edit', ['isPartner' => $isPartner, 'isCutting' =>$isCutting, 'isPengadaan' => $isPengadaan, 'isPengadaanDetail' => $isPengadaanDetail, 'pengantar' => $pengantar]);
    }

    public function update(Request $request){
        // dd($request);
        try {
            DB::beginTransaction();
             Cutting::where(['id' => $request->id])->update([
                'kode'=> $request->kode,
                'pengadaan_id'=> $request->pengadaan_id,
                'partner_id'=> $request->partner_id,
                'pengantar'=> $request->pengantar_id,
                'tanggal'=> $request->tanggal,
                'keterangan'=> $request->keterangan,
                'pembuat' => auth()->user()->id,
            ]);

            if(isset($request->cutting_detail_id)){
                $inDb = CuttingDetail::where(['cutting_id' => $request->id])->count();
                $inReq = count($request->cutting_detail_id);
                if($inDb > $inReq){
                    CuttingDetail::where('cutting_id', $request->id)
                    ->whereNotIn('id', $request->cutting_detail_id)
                    ->delete();
                }
            }else{
                CuttingDetail::where('cutting_id', $request->id)->delete();
            }

            foreach($request->pengadaan_detail_id as $i => $value){
                if(isset($request->cutting_detail_id[$i])){
                    CuttingDetail::where(['id' => $request->cutting_detail_id[$i]])->update([
                        'pengadaan_detail_id'=> $request->pengadaan_detail_id[$i],
                        'produk_id'=> $request->produk_id[$i],
                        'produk_detail_id'=> $request->varian_id[$i],
                        'harga'=> str_replace(',', '',$request->price[$i]),
                        'est_jadi'=> $request->est_jadi[$i],
                        'est_sisa'=> $request->est_sisa[$i]
                ]);
                }else{
                    CuttingDetail::create([
                        'cutting_id'=> $request->id,
                        'pengadaan_detail_id'=> $request->pengadaan_detail_id[$i],
                        'produk_id'=> $request->produk_id[$i],
                        'produk_detail_id'=> $request->varian_id[$i],
                        'harga'=> str_replace(',', '',$request->price[$i]),
                        'est_jadi'=> $request->est_jadi[$i],
                        'est_sisa'=> $request->est_sisa[$i],
                        'status'=> 0
                    ]);
                }
            }

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function getProduk($id){
        $isProduk = DB::select('SELECT b.*, a.qty  FROM `pengadaan_detail` AS a
        JOIN produk AS b ON a.material_id = b.material_id
        WHERE a.id = '.$id.'
        GROUP BY b.id');

        return response()->json(['isProduk' => $isProduk]);
    }

    public function getVarian($id){
        $isVarian = DB::select('SELECT a.*, b.number_cutting_require FROM produk_detail AS a JOIN produk AS b ON a.produk_id = b.id WHERE produk_id =
        '.$id.' GROUP BY a.id');

        return response()->json(['isVarian' => $isVarian]);
    }

    public function destroy($id){
        try {
            DB::beginTransaction();
            Cutting::where('id', $id)->delete();
            CuttingDetail::where('cutting_id', $id)->delete();
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function show($id, $isConfirm){

        $isCutting = DB::select("SELECT a.id, a.kode, a.partner_id, a.pengadaan_id, DATE(a.tanggal) as tanggal, b.pengadaan_detail_id, b.produk_id, b.produk_detail_id, FORMAT(b.est_jadi,2) as est_jadi, format(b.harga,2) as harga, CONCAT(format(b.est_sisa,2), f.simbol) as est_sisa, b.jumlah_jadi, c.qty as qty_yard, b.id as cutting_detail_id, FORMAT(b.koreksi,2) as koreksi,
       CONCAT(FORMAT(c.qty,2), f.simbol, ' : ', d.nama, ' (' , e.warna,')') AS pengadaan,
        CONCAT(g.nama,' (', FORMAT(g.number_cutting_require,2), f.simbol,')') AS produk, h.nama AS detail_produk, b.status, b.flag, DATE(b.tanggal_selesai) as tanggal_selesai, b.pembawa, up.nama as nama_pembawa
        FROM cutting AS a
       JOIN cutting_detail AS b ON a.id = b.cutting_id AND b.deleted_at IS NULL
       JOIN pengadaan_detail as c ON b.pengadaan_detail_id = c.id
       JOIN material AS d ON d.id = c.material_id
       JOIN material_detail AS e ON e.id = c.material_detail_id
       JOIN uom AS f ON d.uom_id = f.id
       JOIN produk AS g ON b.produk_id = g.id
       JOIN produk_detail AS h ON b.produk_detail_id = h.id
       LEFT JOIN partner as up ON b.pembawa = up.id
       WHERE a.id = $id
       GROUP BY b.id");
       $isPartner = Partner::where(['kategori_partner_id' => 2])->get();
       $isPengadaan = Pengadaan::where(['jenis_material_id' => 1, 'status' => 1])->get();
       $pembawa = Partner::where('kategori_partner_id', 5)->get();
       $isPengadaanDetail = DB::select("SELECT
       a.id, a.pengadaan_id, a.material_id, a.material_detail_id, FORMAT(a.qty,2) as qty, a.price, b.nama, c.warna, d.simbol AS uom
       FROM pengadaan_detail AS a
       JOIN material AS b ON a.material_id = b.id
       JOIN material_detail AS c ON a.material_detail_id = c.id
       JOIN uom AS d ON b.uom_id = d.id
       WHERE a.pengadaan_id = ". $isCutting[0]->pengadaan_id ." AND a.deleted_at IS NULL AND a.status = 0");
        if($isConfirm == 1){
            return view('admin.produksi.cutting.show', ['isPartner' => $isPartner, 'isCutting' =>$isCutting, 'isPengadaan' => $isPengadaan, 'isPengadaanDetail' => $isPengadaanDetail, 'isConfirm' => $isConfirm, 'pembawa' => $pembawa]);
        }else{
            return view('admin.produksi.cutting.show', ['isPartner' => $isPartner, 'isCutting' =>$isCutting, 'isPengadaan' => $isPengadaan, 'isPengadaanDetail' => $isPengadaanDetail, 'isConfirm' => $isConfirm,'pembawa' => $pembawa]);
        }
    }

    public function storeHasilCutting(Request $request){
        // dd($request);
        try {
            DB::beginTransaction();
            $koreksi = $request->jumlah_akhir - $request->est_jadi_modal ;
            $flag = 1;

            if ($koreksi >= 0 && $koreksi <= 1) {
                $flag = 1;
            } elseif ($koreksi > 1) {
                $flag = 4;
            } elseif ($koreksi < 0 && $koreksi >= -2) {
                $flag = 2;
            } elseif ($koreksi < -2) {
                $flag = 3;
            }

            CuttingDetail::where('id', $request->id_cutting_detail)->update([
                'koreksi' => $koreksi,
                'jumlah_jadi' => $request->jumlah_akhir,
                'tanggal_selesai' => $request->tanggal_selesai,
                'status' => 1,
                'pembawa' => $request->pembawa_id,
                'flag' => $flag,
            ]);

            // History
            $peng = PengadaanDetail::where(['id' => $request->pengadaan_detail_id])->first();
            // dd($peng);
            $stok = MaterialStok::where( ['material_id' => $peng->material_id,
            'material_detail_id' => $peng->material_detail_id])->first();
            HistoryMaterial::create([
                'material_id' => $peng->material_id,
                'material_detail_id' => $peng->material_detail_id,
                'tanggal_history' => $request->tanggal_diserahkan,
                'tipe_history' => 1,
                'jenis_history_id' => 2,
                'detail_id_dari_jenis' => $request->pengadaan_detail_id,
                'harga' => $peng->price,
                'stok_sebelum' => $stok->stok_akhir,
                'qty' => $peng->qty,
                'stok_sesudah' => $stok->stok_akhir - $peng->qty,
                'keterangan' => "Pengadaan",
                'note' => "Keluar Pengadaan, Masuk Cutting $request->kode",
                'user_id' => auth()->user()->id,
            ]);

            HistoryMaterial::create([
                'material_id' => $peng->material_id,
                'material_detail_id' => $peng->material_detail_id,
                'tanggal_history' => $request->tanggal_diserahkan,
                'tipe_history' => 0,
                'jenis_history_id' => 3,
                'detail_id_dari_jenis' => $request->id_cutting_detail,
                'harga' => str_replace(',', '',$request->price),
                'qty' => $request->jumlah_akhir,
                'keterangan' => "Cutting",
                'note' => "Masuk Cutting",
                'user_id' => auth()->user()->id,
            ]);

          DB::select('UPDATE material_stok SET stok_keluar = stok_keluar +'.$peng->qty.', stok_akhir = stok_akhir - '.$peng->qty.', updated_at = "'.Carbon::now()->format('Y-m-d H:i:s') .'" WHERE material_id = '.$peng->material_id.' AND material_detail_id = '.$peng->material_detail_id);
            // End History

            $checkCutting = DB::select('SELECT b.* FROM (SELECT a.id FROM cutting AS a
            JOIN cutting_detail AS b ON a.id = b.cutting_id AND b.deleted_at IS NULL
            WHERE b.id = '.$request->id_cutting_detail.'
            GROUP BY a.id ) AS a
            JOIN cutting_detail AS b ON b.cutting_id = a.id
              WHERE b.deleted_at IS NULL
            GROUP BY b.id');
            $id = $checkCutting[0]->cutting_id;
            $status = 1;
            foreach($checkCutting as $cut){
                if($cut->status == 0) {
                    $status = 0;
                }
            }

            if($status == 1){
                Cutting::where(['id' => $id])->update(['status' => 1]);
            }else{
                Cutting::where(['id' => $id])->update(['status' => 2]);
            }

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }
}
