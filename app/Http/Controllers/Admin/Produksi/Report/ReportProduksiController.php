<?php

namespace App\Http\Controllers\Admin\Produksi\Report;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
class ReportProduksiController extends Controller
{
    public function index(){
        $jumlahPengadaan = DB::select("SELECT SUM(a.qty * a.price) as total, SUM(a.total) as total_2 from (
                                    SELECT b.* FROM pengadaan AS a
                                    JOIN pengadaan_detail AS b ON a.id = b.pengadaan_id AND b.deleted_at IS NULL
                                    WHERE a.deleted_at IS null and a.status = 1
                                    GROUP BY b.id
                                    ) AS a ");
        return view('admin.produksi.report.index', ['jumlahPengadaan'=>$jumlahPengadaan[0]->total]);
    }

    public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.kode LIKE '%" . $search . "%' OR a.nama LIKE '%" . $search . "%' OR b.kode LIKE '%" . $search . "%' OR b.nama LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('material')->count();
            $lists = DB::select("SELECT a.id, DATE(a.tanggal) as tanggal, CONCAT(a.kode,' - ',i.nama) AS kode_pengadaan,
GROUP_CONCAT('<li>',j.nama,'-',k.warna,'(',FORMAT(b.qty,2),l.simbol,')','</li>' SEPARATOR '') AS material, SUM(b.total) as total,
                    CASE
                            WHEN d.status IS NULL THEN '-'
                        WHEN MIN(d.status) <> 1 THEN 'Dalam proses'
                        ELSE 'Selesai'
                    END AS cutting,
                    DATE(MAX(c.tanggal_selesai)) as tanggal_cutting,
                    CASE
                                WHEN f.status IS NULL THEN '-'
                            WHEN MIN(f.status) <> 1 THEN 'Dalam proses'
                            ELSE 'Selesai'
                        END AS sewing,
                         DATE(MAX(e.tanggal_selesai)) as tanggal_sewing,
                    CASE
                        WHEN h.status IS NULL THEN '-'
                        WHEN MIN(h.status) <> 1 THEN 'Dalam proses'
                        ELSE 'Selesai'
                    END AS finishing,
                     DATE(MAX(g.tanggal_selesai)) as tanggal_finishing
                FROM pengadaan AS a
                JOIN pengadaan_detail AS b ON a.id = b.pengadaan_id AND b.deleted_at IS null
                LEFT JOIN cutting_detail AS c ON b.id = c.pengadaan_detail_id AND c.deleted_at IS null
                LEFT JOIN cutting AS d ON d.id = c.cutting_id AND d.deleted_at IS NULL
                LEFT JOIN sewing_detail AS e ON c.id = e.cutting_detail_id AND e.deleted_at IS NULL
                LEFT JOIN sewing AS f ON e.sewing_id = f.id AND f.deleted_at IS NULL
                LEFT JOIN finishing_details AS g ON e.id = g.sewing_detail_id AND g.deleted_at IS NULL
                LEFT JOIN finishing AS h ON g.finishing_id = h.id AND h.deleted_at IS NULL
                JOIN partner AS i ON a.partner_id = i.id
                JOIN material AS j ON b.material_id = j.id
                JOIN material_detail AS k ON b.material_detail_id = k.id
                JOIN uom AS l ON j.uom_id = l.id
                WHERE a.`status` = 1 AND a.deleted_at IS null
                GROUP BY a.id");
            $totalRecords = count($lists);

            return DataTables::of($lists)
                ->addIndexColumn()
                ->addColumn('action', function ($model) {
                    $action =
                        '  <a href="/admin/procurement/pengadaan/show/' .
        $model->id .'/0" class="btn btn-sm btn-light btn-active-light-primary">Detail</a>';
                    return $action;
                })
                ->addColumn('status', function ($model) {
                            $status = '	<div class="timeline-label">
                                                    <div class="timeline-item">
														<div class="timeline-label fw-bolder text-gray-800 fs-8">'.$model->tanggal_cutting.'</div>
														<div class="timeline-badge">
															<i class="fa fa-genderless text-warning fs-1"></i>
														</div>
														<div class="timeline-content d-flex">
															<span class="fw-bolder text-gray-800 ps-3">Cutting '.$model->cutting.'</span>
														</div>
													</div>
													<div class="timeline-item">
														<div class="timeline-label fw-bolder text-gray-800 fs-8 p-2">'.$model->tanggal_sewing.'</div>
														<div class="timeline-badge">
															<i class="fa fa-genderless text-success fs-1"></i>
														</div>
														<div class="timeline-content d-flex">
															<span class="fw-bolder text-gray-800 ps-3">Jahit '.$model->sewing.'</span>
														</div>
													</div>
                                                    <div class="timeline-item">
														<div class="timeline-label fw-bolder text-gray-800 fs-8">'.$model->tanggal_finishing.'</div>
														<div class="timeline-badge">
															<i class="fa fa-genderless text-primary fs-1"></i>
														</div>
													    <div class="timeline-content d-flex">
															<span class="fw-bolder text-gray-800 ps-3">Finishing '.$model->finishing.'</span>
														</div>
													</div>

                                                    <div>';

                    return $status;
                })
                ->rawColumns(['action','material', 'status'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function show($id){
       $stokDetail = DB::select("SELECT c.id,b.nama AS produk, c.warna AS produk_detail,
        FORMAT(a.stok_awal,2) AS stok_awal,
        FORMAT(a.stok_masuk,2) AS stok_masuk, FORMAT(a.stok_keluar,2) AS stok_keluar,
        FORMAT(a.stok_akhir,2) AS stok_akhir
        FROM material_stok AS a
        JOIN material AS b ON a.material_id = b.id
        JOIN material_detail AS c ON a.material_detail_id = c.id
        WHERE b.id = $id
        GROUP BY a.id");

        $jumlahStok = DB::select("SELECT b.id,a.nama as jenis, b.nama, FORMAT(SUM(c.stok_akhir),2) AS stok_akhir,CONCAT(d.nama,'(',d.simbol,')') as uom FROM jenis_material AS a
        JOIN material AS b ON a.id = b.jenis_material_id AND b.deleted_at IS NULL
        JOIN material_stok AS c ON b.id = c.material_id
        JOIN uom as d ON b.uom_id = d.id
        where b.id = $id
        GROUP BY b.id");

        return view("admin.inventory.stokmaterial.show", ['stokDetail' => $stokDetail, 'jumlahStok' => $jumlahStok[0]]);
    }
}
