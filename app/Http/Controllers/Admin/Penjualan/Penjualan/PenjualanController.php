<?php

namespace App\Http\Controllers\Admin\Penjualan\Penjualan;

use Carbon\Carbon;
use App\Models\Partner;
use App\Models\Sequence;
use App\Models\Penjualan;
use App\Models\Warehouse;
use App\Models\ProdukStok;
use App\Models\ProdukDetail;
use Illuminate\Http\Request;
use App\Models\HistoryProduk;
use App\Models\PenjualanDetail;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PenjualanController extends Controller
{
    public function index(){
        return view('admin.penjualan.penjualan.index');
    }

    public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.nama LIKE '%" . $search . "%' OR a.keterangan LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table("penjualans")->where('deleted_at', 'is null')->get()->count();
            $lists = DB::select("SELECT a.id, DATE(a.tanggal) as tanggal, CONCAT(d.nama,' : ',c.nama) AS media, a.kode, a.keterangan, a.status,
GROUP_CONCAT('<li>',e.nama,'-',f.nama,'</li>' SEPARATOR'<br>') AS produk, FORMAT(SUM(b.qty),0) AS qty, FORMAT(SUM(b.total_harga),0) AS total, DATE(a.tanggal_pic) as tanggal_pic
FROM penjualans AS a
JOIN penjualan_details AS b ON a.id = b.penjualan_id AND b.deleted_at IS null
JOIN media_penjualans AS c ON a.media_penjualan_id = c.id
JOIN kategori_media_penjualans AS d ON c.kategori_media_penjualan_id = d.id
JOIN produk AS e ON b.produk_id = e.id
JOIN produk_detail AS f ON b.produk_detail_id = f.id
 WHERE a.deleted_at is null $search group by a.id order by a.created_at desc limit $limit offset $offset");
            $totalRecords = count($lists);
           
            return DataTables::of($lists)
                ->addIndexColumn()->addColumn('status', function($model){
                    if($model->status == 0){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-800 fs-8">'.$model->tanggal.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-warning fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Menunggu Persetujuan</div>
                        </div>
                        </div>';
                    }elseif($model->status == 2){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
														<div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal_pic.'</div>
														<div class="timeline-badge">
															<i class="fa fa-genderless text-danger fs-5"></i>
														</div>
														<div class="timeline-content d-flex">
															<span class="fw-bolder text-gray-800 ps-3">Ditolak/Selesai</span>
														</div>
													</div>
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-warning fs-5"></i>
                            </div>
                            <div class="fw-mormal timeline-content text-muted ps-3">Menunggu</div>
                        </div>
                        </div>';
                    }elseif($model->status == 1){
                        $status = '<div class="timeline-label">
                        <div class="timeline-item">
                            <div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal_pic.'</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-primary fs-5"></i>
                            </div>
                            <div class="fw-bolder timeline-content text-gray-800 ps-3">Disetujui</div>
                        </div>
                        <div class="timeline-item">
														<div class="timeline-label fw-bolder text-gray-600 fs-8">'.$model->tanggal.'</div>
														<div class="timeline-badge">
															<i class="fa fa-genderless text-warning fs-5"></i>
														</div>
														<div class="timeline-content d-flex">
															<span class="fw-normal text-gray-800 ps-3">Menunggu</span>
														</div>
													</div>
                        </div>';
                    }

                    return $status;
                })
                ->addColumn('action', function ($model) {
                    $action =
                        '  <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                            <span class="svg-icon svg-icon-5 m-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none">
                                    <path
                                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                        fill="black" />
                                </svg>
                            </span></a>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-8 w-125px py-4"
                            data-kt-menu="true">
                    ';

        if($model->status == 0 ){
            $action .= '    <div class="menu-item px-3">
                <a href="/admin/penjualan/show/' .
                $model->id .'/1" class="menu-link px-3">Approve</a>
            </div>
            <div class="menu-item px-3">
                <a href="/admin/penjualan/' .
                $model->id .'" class="menu-link px-3">Edit</a>
            </div>
            <div class="menu-item px-3">
                <button type="button" class="menu-link px-3 btn btn-transparent fs-8" onclick="deleteData(' .
                            $model->id .
                            ', ' .
                            "'" .
                            $model->kode .
                            "'" .
                            ')">Delete</button>
            </div>
            ';
        }

        $action .= '
        <div class="menu-item px-3">
        <a href="/admin/penjualan/show/' .
        $model->id .'/0" class="menu-link px-3">Info</a>
</div>  </div>';
                    return $action;
                })
                ->rawColumns(['action', 'status', 'produk'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function create(){
        $isSequence = Sequence::where(['tab' => 'Penjualan'])->first();
        $isMedia = DB::select('SELECT CONCAT(a.nama, " : ", b.nama) AS media, CONCAT(a.id, ",",b.id) AS id
        FROM kategori_media_penjualans AS a
        JOIN media_penjualans AS b ON a.id = b.kategori_media_penjualan_id AND b.deleted_at IS NULL 
        WHERE a.deleted_at IS NULL
        GROUP BY b.id ORDER BY b.id desc');
        $shipper = Partner::where('kategori_partner_id', 5)->get();

        $isSequence = $isSequence->first . $isSequence->middle + 1 . $isSequence->last;
        return view('admin.penjualan.penjualan.create', ['isMedia' => $isMedia, 'isSequence' =>$isSequence, 'shipper' => $shipper]);
    }

    public function getDetailForm($id){
        $id = explode(",", $id);
        $kategori_media_id = $id[0];
        $media_id = $id[1];

        $produk = DB::select('SELECT a.id, a.nama, b.harga_jual, b.id AS harga_produk_id  FROM produk AS a
                            JOIN harga_produks AS b ON b.produk_id = a.id AND b.`status` = 1
                            JOIN media_penjualans AS c ON b.media_penjualan_id = c.id
                            JOIN kategori_media_penjualans AS d ON c.kategori_media_penjualan_id = d.id
                            WHERE b.kategori_media_penjualan_id = '.$kategori_media_id.' AND b.media_penjualan_id = '.$media_id.'
                            GROUP BY a.id');
        return response()->json(['produk' => $produk]);
    }

    public function getVarian($id){
        $varian = ProdukDetail::where('produk_id', $id)->get();
        return response()->json(['varian' => $varian]);
    }
    public function getStok($produk_id, $produk_detail_id){
        $ps = ProdukStok::where(['produk_id'=> $produk_id, 'produk_detail_id'=>$produk_detail_id])->first();
        return response()->json(['ps' => $ps->stok_akhir]);
    }

    public function store(Request $request){
        // BELUM ADA VALIDASI STOK
        try {
            DB::beginTransaction();
            $media_id = explode(",", $request->media_id);
            $penjualanId = Penjualan::create([
                'kode' => $request->kode,
                'media_penjualan_id' => $media_id[1],
                'tanggal' => $request->tanggal,
                'shipper_id' => $request->shipper_id,
                'keterangan' => $request->keterangan,
                'pembuat' => auth()->user()->id,
                ])->id;

            foreach($request->produk_id as $i => $value){
                $data = explode(",", $request->produk_id[$i]);
               $penjualanDetailId = PenjualanDetail::create([
                    'penjualan_id' => $penjualanId,
                    'produk_id' => $data[0],
                    'produk_detail_id' => $request->varian_id[$i],
                    'harga_produk_id' => $data[2],
                    'harga' => $data[1],
                    'qty' => $request->qty[$i],
                    'total_harga' => str_replace(',', '',$request->total[$i])
                ]);
            }
            $isSequence = Sequence::where(['tab' => 'Penjualan'])->first();
            $SeqUpdate = $isSequence->middle + 1;
            Sequence::where(['tab' => 'Penjualan'])->update(['middle' => $SeqUpdate]);
            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function edit($id){
        $isSequence = Sequence::where(['tab' => 'Penjualan'])->first();
        $isMedia = DB::select('SELECT CONCAT(a.nama, " : ", b.nama) AS media, CONCAT(a.id, ",",b.id) AS id
                        FROM kategori_media_penjualans AS a
                        JOIN media_penjualans AS b ON a.id = b.kategori_media_penjualan_id AND b.deleted_at IS NULL 
                        WHERE a.deleted_at IS NULL
                        GROUP BY b.id ORDER BY b.id desc');
        $shipper = Partner::where('kategori_partner_id', 5)->get();

        $isPenjualan = DB::select('SELECT a.id, a.tanggal, a.kode, a.keterangan, a.shipper_id, 
            b.id AS penjualan_detail_id, b.produk_id,b.produk_detail_id, FORMAT(b.qty,0) as qty, CONCAT(d.id,",",c.id) AS media_id,
            CONCAT(e.id,",",b.harga,",",b.harga_produk_id) AS produk_id, b.harga, d.id as kategori_media_penjualan_id, c.id as media_penjualan_id
            FROM penjualans AS a
            JOIN penjualan_details AS b ON a.id = b.penjualan_id AND b.deleted_at IS null
            JOIN media_penjualans AS c ON a.media_penjualan_id = c.id
            JOIN kategori_media_penjualans AS d ON c.kategori_media_penjualan_id = d.id
            JOIN produk AS e ON b.produk_id = e.id
            JOIN produk_detail AS f ON b.produk_detail_id = f.id
            WHERE a.id = '.$id.'
            GROUP BY b.id');

            $produk = DB::select('SELECT a.id, a.nama, b.harga_jual, b.id AS harga_produk_id  FROM produk AS a
                            JOIN harga_produks AS b ON b.produk_id = a.id AND b.`status` = 1
                            JOIN media_penjualans AS c ON b.media_penjualan_id = c.id
                            JOIN kategori_media_penjualans AS d ON c.kategori_media_penjualan_id = d.id
                            WHERE b.kategori_media_penjualan_id = '.$isPenjualan[0]->kategori_media_penjualan_id.' AND b.media_penjualan_id = '.$isPenjualan[0]->media_penjualan_id.'
                            GROUP BY a.id');
        return view('admin.penjualan.penjualan.edit', ['isMedia' => $isMedia, 'shipper' => $shipper, 'isPenjualan' => $isPenjualan, 'produk' => $produk]);
    }

    public function update(Request $request){
        // dd($request);
        try {
            DB::beginTransaction();
            $media_id = explode(",", $request->media_id);
             Penjualan::where(['id' => $request->id])->update([
                'kode' => $request->kode,
                'media_penjualan_id' => $media_id[1],
                'tanggal' => $request->tanggal,
                'shipper_id' => $request->shipper_id,
                'keterangan' => $request->keterangan,
                'pembuat' => auth()->user()->id,
            ]);
            
            if(isset($request->penjualan_detail_id)){
                $inDb = PenjualanDetail::where(['penjualan_id' => $request->id])->count();
                $inReq = count($request->penjualan_detail_id);
                if($inDb > $inReq){
                    PenjualanDetail::where('penjualan_id', $request->id)
                    ->whereNotIn('id', $request->penjualan_detail_id)
                    ->delete();
                }
            }else{
                PenjualanDetail::where('penjualan_id', $request->id)->delete();
            }
            
            foreach($request->produk_id as $i => $value){
                $data = explode(",", $request->produk_id[$i]);
           
                if(isset($request->penjualan_detail_id[$i])){
                    PenjualanDetail::where(['id' => $request->penjualan_detail_id[$i]])->update([
                    'produk_id' => $data[0],
                    'produk_detail_id' => $request->varian_id[$i],
                    'harga_produk_id' => $data[2],
                    'harga' => $data[1],
                    'qty' => $request->qty[$i],
                    'total_harga' => str_replace(',', '',$request->total[$i])
                ]);
                }else{
                    PenjualanDetail::create([
                        'penjualan_id' => $request->id,
                        'produk_id' => $data[0],
                        'produk_detail_id' => $request->varian_id[$i],
                        'harga_produk_id' => $data[2],
                        'harga' => $data[1],
                        'qty' => $request->qty[$i],
                        'total_harga' => str_replace(',', '',$request->total[$i])
                    ]);
                }
            }

            DB::commit();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function show($id,$isConfirm){
        $isSequence = Sequence::where(['tab' => 'Penjualan'])->first();
        $isMedia = DB::select('SELECT CONCAT(a.nama, " : ", b.nama) AS media, CONCAT(a.id, ",",b.id) AS id
                        FROM kategori_media_penjualans AS a
                        JOIN media_penjualans AS b ON a.id = b.kategori_media_penjualan_id AND b.deleted_at IS NULL 
                        WHERE a.deleted_at IS NULL
                        GROUP BY b.id ORDER BY b.id desc');
        $shipper = Partner::where('kategori_partner_id', 5)->get();

        $isPenjualan = DB::select('SELECT a.id, a.tanggal, a.kode, a.keterangan, a.shipper_id, 
            b.id AS penjualan_detail_id, b.produk_id,b.produk_detail_id, FORMAT(b.qty,0) as qty, CONCAT(d.id,",",c.id) AS media_id, date(a.tanggal_pic) as tanggal_pic, g.name as pic,
            CONCAT(e.id,",",b.harga,",",b.harga_produk_id) AS produk_id, b.harga, d.id as kategori_media_penjualan_id, c.id as media_penjualan_id
            FROM penjualans AS a
            JOIN penjualan_details AS b ON a.id = b.penjualan_id AND b.deleted_at IS null
            JOIN media_penjualans AS c ON a.media_penjualan_id = c.id
            JOIN kategori_media_penjualans AS d ON c.kategori_media_penjualan_id = d.id
            JOIN produk AS e ON b.produk_id = e.id
            JOIN produk_detail AS f ON b.produk_detail_id = f.id
            LEFT JOIN users as g ON a.pic_id = g.id
            WHERE a.id = '.$id.'
            GROUP BY b.id');

            $produk = DB::select('SELECT a.id, a.nama, b.harga_jual, b.id AS harga_produk_id  FROM produk AS a
                            JOIN harga_produks AS b ON b.produk_id = a.id AND b.`status` = 1
                            JOIN media_penjualans AS c ON b.media_penjualan_id = c.id
                            JOIN kategori_media_penjualans AS d ON c.kategori_media_penjualan_id = d.id
                            WHERE b.kategori_media_penjualan_id = '.$isPenjualan[0]->kategori_media_penjualan_id.' AND b.media_penjualan_id = '.$isPenjualan[0]->media_penjualan_id.'
                            GROUP BY a.id');
        return view('admin.penjualan.penjualan.show', ['isMedia' => $isMedia, 'shipper' => $shipper, 'isPenjualan' => $isPenjualan, 'produk' => $produk, 'isConfirm' => $isConfirm]);
    }

    public function confirm(Request $request,$value){
        // dd($request);
        try {
            DB::beginTransaction();
            if($value == 1){
                Penjualan::where(['id' => $request->id])->update(['status' => 1,
                'tanggal_pic' => $request->tanggal_pic,
                'pic_id' => auth()->user()->id]);
                foreach($request->penjualan_detail_id as $i => $val){
                    $detail = PenjualanDetail::where( ['id' => $request->penjualan_detail_id[$i]])->first();

                    $stokProduk = ProdukStok::where(['produk_id' => $detail->produk_id, 'produk_detail_id' => $detail->produk_detail_id])->first();

                    HistoryProduk::create([
                        'produk_id' => $detail->produk_id,
                        'produk_detail_id' => $detail->produk_detail_id,
                        'tanggal_history' => Carbon::now(),
                        'tipe_history' => 1,
                        'jenis_history_id' => 7,
                        'detail_id_dari_jenis' => $request->penjualan_detail_id[$i],
                        'harga' => $detail->harga, // Sebenarnya ini bisa pakai rumus diambil harga rata rata.
                        'stok_sebelum' => $stokProduk->stok_akhir,
                        'qty' => $request->qty[$i],
                        'stok_sesudah' => $stokProduk->stok_akhir - $request->qty[$i],
                        'keterangan' => "Penjualan",
                        'note' => "Penjualan",
                        'user_id' => auth()->user()->id,
                    ]);

                  DB::select('UPDATE produk_stok SET stok_keluar = stok_keluar +'.$request->qty[$i].', stok_akhir = stok_akhir - '.$request->qty[$i].', updated_at = "'.Carbon::now()->format('Y-m-d H:i:s') .'" WHERE produk_id = '.$detail->produk_id.' AND produk_detail_id = '.$detail->produk_detail_id);


                }

            }else{
                Penjualan::where(['id' => $request->id])->update(['status' => 2,
                'tanggal_pic' => $request->tanggal_pic,
                'pic_id' => auth()->user()->id]);
            }

            DB::commit();
            return response()->json(['success' => true]);
          } catch (\GuzzleHttp\Exception\RequestException $e) {
                DB::rollback();
              $response = $e->getResponse();
              $responseData = json_decode($response->getBody(), true);
              if ($responseData['message']) {
                  return response()->json(['errors' => $responseData['message']]);
              } else {
                  return response()->json(['errors' => 'Gagal memanggil API !']);
              }
          }
    }

}
