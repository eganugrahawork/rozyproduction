<?php

namespace App\Http\Controllers\Admin\Inventory\StokMaterial;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;


class StokMaterialController extends Controller
{
    public function index(){
        $sumJenis = DB::select("SELECT a.nama, FORMAT(SUM(c.stok_akhir),2) AS stok_akhir ,
 (SELECT
        FORMAT(IFNULL(SUM(CASE
        WHEN tipe_history = 0 THEN (qty * harga)
        WHEN tipe_history = 1 THEN - (qty * harga)
        END),0),2)
        FROM history_materials AS hs
        JOIN material AS mat ON hs.material_id = mat.id
        JOIN jenis_material as jm ON jm.id = mat.jenis_material_id
        where hs.jenis_history_id IN (1,2)
        AND jm.id = a.id
		  ) as nilai

FROM jenis_material AS a
JOIN material AS b ON a.id = b.jenis_material_id AND b.deleted_at IS NULL
JOIN material_stok AS c ON b.id = c.material_id
GROUP BY a.id");
        return view('admin.inventory.stokmaterial.index', ['sumJenis' =>$sumJenis]);
    }

    public function lists(Request $request)
    {
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.kode LIKE '%" . $search . "%' OR a.nama LIKE '%" . $search . "%' OR b.kode LIKE '%" . $search . "%' OR b.nama LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('material')->count();
            $lists = DB::select("SELECT b.id, b.nama, FORMAT(SUM(a.stok_awal),2) AS stok_awal,
            FORMAT(SUM(a.stok_masuk),2) AS stok_masuk, FORMAT(SUM(a.stok_keluar),2) AS stok_keluar,
            FORMAT(SUM(a.stok_akhir),2) AS stok_akhir,
            CONCAT(c.simbol,'(',c.nama,')') as uom,
            (SELECT
        FORMAT(IFNULL(SUM(CASE
        WHEN tipe_history = 0 THEN (qty * harga)
        WHEN tipe_history = 1 THEN - (qty * harga)
        END),0),2)
        FROM history_materials
        where jenis_history_id IN (1,2)
        AND material_id = b.id) as nilai,
        MAX(b.updated_at) as updated_at
            FROM material_stok AS a
            JOIN material AS b ON a.material_id = b.id
            JOIN uom as c on b.uom_id = c.id
            GROUP BY b.id");
            $totalRecords = count($lists);

            return DataTables::of($lists)
                ->addIndexColumn()
                ->addColumn('action', function ($model) {
                    $action =
                        '  <a href="/admin/inventory/stok-material/' .
        $model->id .'" class="btn btn-sm btn-light btn-active-light-primary">Detail</a>';
                    return $action;
                })
                ->rawColumns(['action'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function show($id){
       $stokDetail = DB::select("SELECT c.id,b.nama AS produk, c.warna AS produk_detail,
        FORMAT(a.stok_awal,2) AS stok_awal,
        FORMAT(a.stok_masuk,2) AS stok_masuk, FORMAT(a.stok_keluar,2) AS stok_keluar,
        FORMAT(a.stok_akhir,2) AS stok_akhir,
        MAX(a.updated_at) as updated_at
        FROM material_stok AS a
        JOIN material AS b ON a.material_id = b.id
        JOIN material_detail AS c ON a.material_detail_id = c.id
        WHERE b.id = $id
        GROUP BY a.id");

        $jumlahStok = DB::select("SELECT b.id,a.nama as jenis, CONCAT(b.kode,'-',b.nama) as nama, FORMAT(SUM(c.stok_akhir),2) AS stok_akhir,d.simbol as uom FROM jenis_material AS a
        JOIN material AS b ON a.id = b.jenis_material_id AND b.deleted_at IS NULL
        JOIN material_stok AS c ON b.id = c.material_id
        JOIN uom as d ON b.uom_id = d.id
        where b.id = $id
        GROUP BY b.id");

        $nilai = DB::select('SELECT
        FORMAT(IFNULL(SUM(CASE
        WHEN tipe_history = 0 THEN (qty * harga)
        WHEN tipe_history = 1 THEN - (qty * harga)
        END),0),2) AS total

        FROM history_materials
        where jenis_history_id IN (1,2)
        AND material_id = '.$id);

        return view("admin.inventory.stokmaterial.show", ['stokDetail' => $stokDetail, 'jumlahStok' => $jumlahStok[0], 'nilai' => $nilai]);
    }

    public function history($id){
        $jumlahStok = DB::select("SELECT a.id,a.nama as material, FORMAT(SUM(b.stok_akhir),2)  AS stok_akhir, FORMAT(SUM(b.stok_masuk),2) as stok_masuk, FORMAT(SUM(b.stok_keluar),2) as stok_keluar, c.simbol, FORMAT(SUM(b.stok_awal),2) as stok_awal
        FROM material AS a
        JOIN material_stok AS b ON a.id = b.material_id
        join uom as c on a.uom_id = c.id
        WHERE a.id = $id
        GROUP BY a.id");

        $nilai = DB::select('SELECT
			FORMAT(IFNULL(SUM(CASE
         	WHEN tipe_history = 0 THEN (qty * harga)
            WHEN tipe_history = 1 THEN - (qty * harga)

      END),0),2) AS total,
       FORMAT(IFNULL(SUM(CASE
         	WHEN tipe_history = 0 AND jenis_history_id <> 1 THEN (qty * harga)
      END),0),2) AS jumlah_masuk,
		FORMAT(IFNULL(SUM(CASE
         	WHEN tipe_history = 1 THEN (qty * harga)
      END),0),2) AS jumlah_keluar,
      	FORMAT(IFNULL(SUM(CASE
         	WHEN tipe_history = 0 AND jenis_history_id = 1 THEN (qty * harga)
      END),0),2) AS jumlah_awal
FROM history_materials AS a
where a.jenis_history_id IN (1,2)
AND a.material_id = '.$id);

        return view('admin.inventory.stokmaterial.history', ['jumlahStok' =>$jumlahStok, 'nilai' => $nilai, 'id' =>$id]);
    }

    public function lists_history(Request $request, $id)
    {
        // dd($id);
        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.kode LIKE '%" . $search . "%' OR a.nama LIKE '%" . $search . "%' OR b.kode LIKE '%" . $search . "%' OR b.nama LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('history_materials')
                                    ->where('material_id', $id)
                                    ->whereIn('jenis_history_id', [1, 2])
                                    ->count();
            $lists = DB::select("SELECT
                a.id,CONCAT(b.nama, '-', c.warna) AS material,a.material_id, a.note,
                CASE
                        WHEN a.tipe_history = 0 THEN CONCAT('".'<span class="svg-icon svg-icon-success svg-icon-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path d="M13 9.59998V21C13 21.6 12.6 22 12 22C11.4 22 11 21.6 11 21V9.59998H13Z" fill="black"/>
                        <path opacity="0.3" d="M4 9.60002H20L12.7 2.3C12.3 1.9 11.7 1.9 11.3 2.3L4 9.60002Z" fill="black"/>
                        </svg></span>'." ',a.keterangan)
                        WHEN a.tipe_history = 1 THEN CONCAT('".
                        '<span class="svg-icon svg-icon-danger svg-icon-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path d="M13 14.4V3C13 2.4 12.6 2 12 2C11.4 2 11 2.4 11 3V14.4H13Z" fill="black"/>
                        <path opacity="0.3" d="M4 14.4H20L12.7 21.7C12.3 22.1 11.7 22.1 11.3 21.7L4 14.4Z" fill="black"/>
                        </svg></span>'."',a.keterangan)
                    END AS jenis,
                    a.tipe_history,
                DATE(a.tanggal_history) AS tanggal,
                FORMAT(a.stok_sebelum,0) as stok_sebelum, FORMAT(a.qty,0) as qty, FORMAT(a.stok_sesudah,0) as stok_sesudah,


                  FORMAT((SELECT
                        SUM(CASE
                            WHEN tipe_history = 0 THEN qty
                            WHEN tipe_history = 1 THEN -qty
                            ELSE 0
                        END)
                    FROM history_materials
                    WHERE material_id = a.material_id AND jenis_history_id IN (1, 2)
                    AND tanggal_history BETWEEN
                        (SELECT MIN(tanggal_history) FROM history_materials)
                        AND a.tanggal_history),0) AS stok_akhir,
                        FORMAT(a.harga * a.qty,2) as harga_material,
                         FORMAT(a.harga,2) as harga
                    FROM history_materials AS a
                    JOIN material AS b ON a.material_id = b.id
                    JOIN material_detail AS c ON a.material_detail_id = c.id
                    WHERE b.id = $id AND a.jenis_history_id IN (1, 2)
                    GROUP BY a.id ORDER BY a.tanggal_history DESC
            ");
            $totalRecords = count($lists);
            $stok_a = 0;
            return DataTables::of($lists)
                ->addIndexColumn()
                ->rawColumns(['jenis'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }

    public function detailHistory($id){
        $jumlahStok = DB::select("SELECT CONCAT(b.nama, '-', c.warna) AS nama, FORMAT(a.stok_akhir,0) AS stok_akhir,
FORMAT(a.stok_keluar,0) AS stok_keluar,  b.id,
        FORMAT(a.stok_masuk,0) AS stok_masuk,   FORMAT(a.stok_awal,0) AS stok_awal,
		  FORMAT(SUM(a.stok_akhir * IFNULL(d.harga,0)),0) AS nilai_stok_akhir
        FROM material_stok AS a
        JOIN material AS b ON a.material_id = b.id
        JOIN material_detail AS c ON a.material_detail_id = c.id
        LEFT JOIN history_materials AS d ON b.id = d.material_id AND c.id = d.material_detail_id AND d.deleted_at  IS null
		  WHERE a.material_detail_id = $id AND d.jenis_history_id IN (1, 2)
        GROUP BY a.id");

        $nilai = DB::select('SELECT
        FORMAT(IFNULL(SUM(CASE
        WHEN tipe_history = 0 THEN (qty * harga)
        WHEN tipe_history = 1 THEN - (qty * harga)

        END),0),2) AS total,
        FORMAT(IFNULL(SUM(CASE
        WHEN tipe_history = 0 AND jenis_history_id <> 1 THEN (qty * harga)
        END),0),2) AS jumlah_masuk,
        FORMAT(IFNULL(SUM(CASE
        WHEN tipe_history = 1 THEN (qty * harga)
        END),0),2) AS jumlah_keluar,
        FORMAT(IFNULL(SUM(CASE
        WHEN tipe_history = 0 AND jenis_history_id = 1 THEN (qty * harga)
        END),0),2) AS jumlah_awal
        FROM history_materials AS a
        WHERE a.material_detail_id = '.$id.' AND jenis_history_id IN (1, 2)');

        return view("admin.inventory.stokmaterial.historydetail", ['jumlahStok' => $jumlahStok,'id'=>$id, 'nilai'=>$nilai]);
    }

    public function lists_history_detail(Request $request, $id)
    {

        try {
            $limit = $request->input('length');
            $offset = $request->input('start');
            $search = $request->input('search.value');
            if ($search) {
                $search = "AND a.kode LIKE '%" . $search . "%' OR a.nama LIKE '%" . $search . "%' OR b.kode LIKE '%" . $search . "%' OR b.nama LIKE '%" . $search . "%' ";
            } else {
                $search = '';
            }
            $totalFilteredRecords = DB::table('history_materials')
                                        ->where('material_detail_id', $id)
                                        ->whereIn('jenis_history_id', [1, 2])
                                        ->count();
            $lists = DB::select("SELECT a.id,CONCAT(b.nama, '-', c.warna) AS material,a.material_id,
                CASE
                        WHEN a.tipe_history = 0 THEN CONCAT('".'<span class="svg-icon svg-icon-success svg-icon-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path d="M13 9.59998V21C13 21.6 12.6 22 12 22C11.4 22 11 21.6 11 21V9.59998H13Z" fill="black"/>
                        <path opacity="0.3" d="M4 9.60002H20L12.7 2.3C12.3 1.9 11.7 1.9 11.3 2.3L4 9.60002Z" fill="black"/>
                        </svg></span>'." ',a.keterangan)
                        WHEN a.tipe_history = 1 THEN CONCAT('".
                        '<span class="svg-icon svg-icon-danger svg-icon-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
<path d="M13 14.4V3C13 2.4 12.6 2 12 2C11.4 2 11 2.4 11 3V14.4H13Z" fill="black"/>
<path opacity="0.3" d="M4 14.4H20L12.7 21.7C12.3 22.1 11.7 22.1 11.3 21.7L4 14.4Z" fill="black"/>
</svg></span>'."',a.keterangan)
                        ELSE '-'
                    END AS jenis,
                    a.tipe_history, a.note,
                DATE(a.tanggal_history) AS tanggal,
                 FORMAT(a.stok_sebelum,0) as stok_sebelum, FORMAT(a.qty,0) as qty, FORMAT(a.stok_sesudah,0) as stok_sesudah,
                 FORMAT(a.harga * a.qty,2) as jumlah,
                  FORMAT(a.harga,2) as harga
                FROM history_materials AS a
                JOIN material AS b ON a.material_id = b.id
                JOIN material_detail AS c ON a.material_detail_id = c.id
                JOIN (
                    SELECT aa.material_id, SUM(aa.stok_awal) AS stok_awal
                    FROM material_stok AS aa
                    GROUP BY aa.material_id
                ) AS d ON d.material_id = b.id
                WHERE a.material_detail_id = $id AND a.jenis_history_id IN (1, 2)
                GROUP BY a.id ORDER BY a.tanggal_history DESC
            ");
            $totalRecords = count($lists);
            $stok_a = 0;
            return DataTables::of($lists)
                ->addIndexColumn()
                ->rawColumns(['jenis'])
                ->with('recordsTotal', $totalRecords)
                ->with('recordsFiltered', $totalFilteredRecords)
                ->setOffset((int) $offset)
                ->make(true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response = $e->getResponse();
            $responseData = json_decode($response->getBody(), true);
            if ($responseData['message']) {
                return response()->json(['errors' => $responseData['message']]);
            } else {
                return response()->json(['errors' => 'Gagal memanggil API !']);
            }
        }
    }
}
