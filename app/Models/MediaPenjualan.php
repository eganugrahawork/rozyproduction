<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class MediaPenjualan extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table ='media_penjualans';
    protected $guarded=['id'];
}
