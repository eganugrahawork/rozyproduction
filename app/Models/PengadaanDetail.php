<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class PengadaanDetail extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table ='pengadaan_detail';
    protected $guarded=['id'];
}
