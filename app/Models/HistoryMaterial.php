<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class HistoryMaterial extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table ='history_materials';
    protected $guarded=['id'];
}
