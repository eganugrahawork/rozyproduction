<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class MaterialStok extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table ='material_stok';
    protected $guarded=['id'];
}
