<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class KategoriPartner extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table ='kategori_partner';
    protected $guarded=['id'];
}
