<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class HistoryProduk extends Model
{
    use HasFactory;
    use HasFactory;
    use SoftDeletes;
    protected $table ='history_produks';
    protected $guarded=['id'];
}
