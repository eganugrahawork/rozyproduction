<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class JenisHistory extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table ='jenis_histories';
    protected $guarded=['id'];
}
