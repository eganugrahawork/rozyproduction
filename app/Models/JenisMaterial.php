<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class JenisMaterial extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table ='jenis_material';
    protected $guarded=['id'];

}
