<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\Masterdata\Coa\CoaController;
// Masterdata
use App\Http\Controllers\Admin\Masterdata\Uom\UomController;
use App\Http\Controllers\Admin\Dashboard\DashboardController;
use App\Http\Controllers\Admin\Produksi\Sewing\SewingController;
use App\Http\Controllers\Admin\Masterdata\Produk\ProdukController;
use App\Http\Controllers\Admin\Produksi\Cutting\CuttingController;
use App\Http\Controllers\Admin\Masterdata\Partner\PartnerController;
use App\Http\Controllers\Admin\Masterdata\Material\MaterialController;
use App\Http\Controllers\Admin\Produksi\Finishing\FinishingController;

// Procurement
use App\Http\Controllers\Admin\Penjualan\Penjualan\PenjualanController;
use App\Http\Controllers\Admin\Masterdata\Pricing\HargaProdukController;

// Produksi
use App\Http\Controllers\Admin\Masterdata\Warehouse\WarehouseController;
use App\Http\Controllers\Admin\Produksi\Report\ReportProduksiController;
use App\Http\Controllers\Admin\Inventory\StokProduk\StokProdukController;
use App\Http\Controllers\Admin\Procurement\Pengadaan\PengadaanController;


// Inventory
use App\Http\Controllers\Admin\Masterdata\Produk\KategoriProdukController;
use App\Http\Controllers\Admin\Masterdata\Material\JenisMaterialController;
use App\Http\Controllers\Admin\Masterdata\Partner\KategoriPartnerController;
use App\Http\Controllers\Admin\Inventory\StokMaterial\StokMaterialController;
use App\Http\Controllers\Admin\Procurement\Report\ReportProcurementController;
use App\Http\Controllers\Admin\Masterdata\MediaPenjualan\MediaPenjualanController;
use App\Http\Controllers\Admin\Masterdata\MediaPenjualan\KategoriMediaPenjualanController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('newauth.login');
})->middleware('guest')->name('login_baru');

Route::middleware(['auth'])->controller(DashboardController::class)->group(function () {
    Route::get('/admin/dashboard', 'index')->name('dashboard');
});

// Masterdata
Route::middleware(['auth'])->controller(MaterialController::class)->group(function () {
    Route::get('/admin/masterdata/material', 'index')->name('masterdata.material');
    Route::get('/admin/masterdata/material/lists', 'lists');
    Route::get('/admin/masterdata/material/create', 'create')->name('masterdata.material');
    Route::post('/admin/masterdata/material', 'store');
    Route::get('/admin/masterdata/material/edit/{id}', 'edit')->name('masterdata.material');
    Route::get('/admin/masterdata/material/show/{id}', 'show')->name('masterdata.material');
    Route::put('/admin/masterdata/material/{id}', 'update');
    Route::delete('/admin/masterdata/material/{id}', 'destroy');
    Route::get('/admin/masterdata/material/check-code', 'check_code');
});

Route::middleware(['auth'])->controller(JenisMaterialController::class)->group(function () {
    Route::get('/admin/masterdata/jenis-material', 'index')->name('masterdata.material');
    Route::get('/admin/masterdata/jenis-material/lists', 'lists');
    Route::get('/admin/masterdata/jenis-material/create', 'create');
    Route::post('/admin/masterdata/jenis-material/store', 'store');
    Route::get('/admin/masterdata/jenis-material/edit/{id}', 'edit');
    Route::put('/admin/masterdata/jenis-material/{id}', 'update');
    Route::delete('/admin/masterdata/jenis-material/{id}', 'destroy');

});

Route::middleware(['auth'])->controller(KategoriPartnerController::class)->group(function () {
    Route::get('/admin/masterdata/kategori-partner', 'index');
    Route::get('/admin/masterdata/kategori-partner/lists', 'lists');
    Route::get('/admin/masterdata/kategori-partner/create', 'create');
    Route::post('/admin/masterdata/kategori-partner', 'store');
    Route::get('/admin/masterdata/kategori-partner/{id}', 'edit');
    Route::put('/admin/masterdata/kategori-partner/{id}', 'update');
    Route::delete('/admin/masterdata/kategori-partner/{id}', 'destroy');
});

Route::middleware(['auth'])->controller(PartnerController::class)->group(function () {
    Route::get('/admin/masterdata/partner', 'index')->name('masterdata.partner');
    Route::get('/admin/masterdata/partner/lists', 'lists');
    Route::get('/admin/masterdata/partner/create', 'create')->name('masterdata.partner');
    Route::post('/admin/masterdata/partner', 'store');
    Route::get('/admin/masterdata/partner/edit/{id}', 'edit')->name('masterdata.partner');
    Route::put('/admin/masterdata/partner/{id}', 'update');
    Route::delete('/admin/masterdata/partner/{id}', 'destroy');
    Route::get('/admin/masterdata/partner/check-code', 'check_code');
});

Route::middleware(['auth'])->controller(ProdukController::class)->group(function () {
    Route::get('/admin/masterdata/produk', 'index')->name('masterdata.produk');
    Route::get('/admin/masterdata/produk/lists', 'lists');
    Route::get('/admin/masterdata/produk/create', 'create')->name('masterdata.produk');
    Route::post('/admin/masterdata/produk', 'store');
    Route::get('/admin/masterdata/produk/edit/{id}', 'edit')->name('masterdata.produk');
    Route::get('/admin/masterdata/produk/show/{id}', 'show')->name('masterdata.produk');
    Route::put('/admin/masterdata/produk/{id}', 'update');
    Route::delete('/admin/masterdata/produk/{id}', 'destroy');
    Route::get('/admin/masterdata/produk/check-code', 'check_code');
});

Route::middleware(['auth'])->controller(KategoriProdukController::class)->group(function () {
    Route::get('/admin/masterdata/kategori-produk', 'index');
    Route::get('/admin/masterdata/kategori-produk/lists', 'lists');
    Route::get('/admin/masterdata/kategori-produk/create', 'create');
    Route::post('/admin/masterdata/kategori-produk', 'store');
    Route::get('/admin/masterdata/kategori-produk/{id}', 'edit');
    Route::put('/admin/masterdata/kategori-produk/{id}', 'update');
    Route::delete('/admin/masterdata/kategori-produk/{id}', 'destroy');
});

Route::middleware(['auth'])->controller(UomController::class)->group(function () {
    Route::get('/admin/masterdata/uom', 'index')->name('masterdata.uom');
    Route::get('/admin/masterdata/uom/lists', 'lists');
    Route::get('/admin/masterdata/uom/create', 'create')->name('masterdata.uom');
    Route::post('/admin/masterdata/uom', 'store');
    Route::get('/admin/masterdata/uom/edit/{id}', 'edit')->name('masterdata.uom');
    Route::put('/admin/masterdata/uom/{id}', 'update');
    Route::delete('/admin/masterdata/uom/{id}', 'destroy');
});


Route::middleware(['auth'])->controller(CoaController::class)->group(function () {
    Route::get('/admin/masterdata/coa', 'index')->name('masterdata.coa');
    Route::get('/admin/masterdata/coa/lists', 'lists');
    Route::get('/admin/masterdata/coa/create', 'create')->name('masterdata.coa');
    Route::post('/admin/masterdata/coa', 'store');
    Route::get('/admin/masterdata/coa/edit/{id}', 'edit')->name('masterdata.coa');
    Route::put('/admin/masterdata/coa/{id}', 'update');
    Route::delete('/admin/masterdata/coa/{id}', 'destroy');
    Route::get('/admin/masterdata/coa/check-unique', 'checkUnique');
});

Route::middleware(['auth'])->controller(WarehouseController::class)->group(function () {
    Route::get('/admin/masterdata/warehouse', 'index')->name('masterdata.warehouse');
    Route::get('/admin/masterdata/warehouse/lists', 'lists');
    Route::get('/admin/masterdata/warehouse/create', 'create')->name('masterdata.warehouse');
    Route::post('/admin/masterdata/warehouse', 'store');
    Route::get('/admin/masterdata/warehouse/edit/{id}', 'edit')->name('masterdata.warehouse');
    Route::put('/admin/masterdata/warehouse/{id}', 'update');
    Route::delete('/admin/masterdata/warehouse/{id}', 'destroy');
    Route::get('/admin/masterdata/warehouse/check-unique', 'checkUnique');
});

Route::middleware(['auth'])->controller(KategoriMediaPenjualanController::class)->group(function () {
    Route::get('/admin/masterdata/kategori-media-penjualan', 'index');
    Route::get('/admin/masterdata/kategori-media-penjualan/lists', 'lists');
    Route::get('/admin/masterdata/kategori-media-penjualan/create', 'create');
    Route::post('/admin/masterdata/kategori-media-penjualan', 'store');
    Route::get('/admin/masterdata/kategori-media-penjualan/{id}', 'edit');
    Route::put('/admin/masterdata/kategori-media-penjualan/{id}', 'update');
    Route::delete('/admin/masterdata/kategori-media-penjualan/{id}', 'destroy');
});

Route::middleware(['auth'])->controller(MediaPenjualanController::class)->group(function () {
    Route::get('/admin/masterdata/media-penjualan', 'index')->name('masterdata.media_penjualan');
    Route::get('/admin/masterdata/media-penjualan/lists', 'lists');
    Route::get('/admin/masterdata/media-penjualan/create', 'create')->name('masterdata.media_penjualan');
    Route::post('/admin/masterdata/media-penjualan', 'store');
    Route::get('/admin/masterdata/media-penjualan/edit/{id}', 'edit')->name('masterdata.media_penjualan');
    Route::put('/admin/masterdata/media-penjualan/{id}', 'update');
    Route::delete('/admin/masterdata/media-penjualan/{id}', 'destroy');
    Route::get('/admin/masterdata/media-penjualan/check-unique', 'checkUnique');
});

Route::middleware(['auth'])->controller(HargaProdukController::class)->group(function () {
    Route::get('/admin/masterdata/pricing', 'index')->name('masterdata.pricing');
    Route::get('/admin/masterdata/pricing/get-media/{id}', 'get_media');
    Route::get('/admin/masterdata/pricing/lists', 'lists');
    Route::get('/admin/masterdata/pricing/create', 'create')->name('masterdata.pricing');
    Route::post('/admin/masterdata/pricing', 'store');
    Route::get('/admin/masterdata/pricing/edit/{id}', 'edit')->name('masterdata.pricing');
    Route::put('/admin/masterdata/pricing/{id}', 'update');
    Route::delete('/admin/masterdata/pricing/{id}', 'destroy');
    Route::get('/admin/masterdata/pricing/check-unique', 'checkUnique');
});
// End Masterdata


// Procurement
Route::middleware(['auth'])->controller(PengadaanController::class)->group(function () {
    Route::get('/admin/procurement/pengadaan', 'index')->name('procurement.pengadaan');
    Route::get('/admin/procurement/pengadaan/lists', 'lists');
    Route::get('/admin/procurement/pengadaan/create', 'create')->name('procurement.pengadaan');
    Route::post('/admin/procurement/pengadaan', 'store');
    Route::post('/admin/procurement/pengadaan/jumlah-pengadaan', 'jumlahPengadaan');
    Route::get('/admin/procurement/pengadaan/{id}', 'edit')->name('procurement.pengadaan');
    Route::get('/admin/procurement/pengadaan/show/{id}/{isConfirm}', 'show')->name('procurement.pengadaan');;
    Route::get('/admin/procurement/pengadaan/get-partner/{id}', 'getDataPartner');
    Route::put('/admin/procurement/pengadaan/{id}', 'update');
    Route::put('/admin/procurement/pengadaan/confirm/{value}', 'confirm')->name('procurement.pengadaan');;
    Route::get('/admin/procurement/pengadaan/get-detail-form/{id}', 'getDetailForm');
    Route::get('/admin/procurement/pengadaan/get-detail-material/{id}', 'getDetailMaterial');
    Route::delete('/admin/procurement/pengadaan/{id}', 'destroy');
});
Route::middleware(['auth'])->controller(ReportProcurementController::class)->group(function () {
    Route::get('/admin/procurement/report', 'index')->name('procurement.report');
    Route::get('/admin/procurement/report/lists', 'lists');
    Route::get('/admin/procurement/report/{id}', 'show')->name('procurement.report');
});
// End Procurement

// Produksi
Route::middleware(['auth'])->controller(CuttingController::class)->group(function () {
    Route::get('/admin/produksi/cutting', 'index')->name('produksi.cutting');
    Route::get('/admin/produksi/cutting/lists', 'lists');
    Route::get('/admin/produksi/cutting/create', 'create')->name('produksi.cutting');
    Route::post('/admin/produksi/cutting', 'store');
    Route::post('/admin/produksi/storehasilcutting', 'storeHasilCutting');
    Route::get('/admin/produksi/cutting/{id}', 'edit')->name('produksi.cutting');
    Route::get('/admin/produksi/cutting/show/{id}/{isConfirm}', 'show')->name('produksi.cutting');
    Route::get('/admin/produksi/cutting/get-partner/{id}', 'getDataPartner');
    Route::put('/admin/produksi/cutting/{id}', 'update');
    Route::put('/admin/produksi/cutting/confirm/{value}', 'confirm')->name('produksi.cutting');
    Route::get('/admin/produksi/cutting/get-detail-form/{id}', 'getDetailForm');
    Route::get('/admin/produksi/cutting/get-produk/{id}', 'getProduk');
    Route::get('/admin/produksi/cutting/get-varian/{id}', 'getVarian');
    Route::delete('/admin/produksi/cutting/{id}', 'destroy');
});
Route::middleware(['auth'])->controller(SewingController::class)->group(function () {
    Route::get('/admin/produksi/jahit', 'index')->name('produksi.jahit');
    Route::get('/admin/produksi/jahit/lists', 'lists');
    Route::get('/admin/produksi/jahit/create', 'create')->name('produksi.jahit');
    Route::post('/admin/produksi/jahit', 'store');
    Route::get('/admin/produksi/jahit/get-detail-form', 'getDetailForm');
    Route::get('/admin/produksi/jahit/{id}', 'edit')->name('produksi.jahit');
    Route::get('/admin/produksi/jahit/show/{id}/{isConfirm}', 'show')->name('produksi.jahit');
    Route::get('/admin/produksi/jahit/get-partner/{id}', 'getDataPartner');
    Route::put('/admin/produksi/jahit/{id}', 'update');
    Route::post('/admin/produksi/jahit/proccess', 'proccess')->name('produksi.jahit');
    Route::delete('/admin/produksi/jahit/{id}', 'destroy');
});
Route::middleware(['auth'])->controller(FinishingController::class)->group(function () {
    Route::get('/admin/produksi/finishing', 'index')->name('produksi.finishing');
    Route::get('/admin/produksi/finishing/lists', 'lists');
    Route::get('/admin/produksi/finishing/create', 'create')->name('produksi.finishing');
    Route::post('/admin/produksi/finishing', 'store');
    Route::get('/admin/produksi/finishing/get-detail-form', 'getDetailForm');
    Route::get('/admin/produksi/finishing/{id}', 'edit')->name('produksi.finishing');
    Route::get('/admin/produksi/finishing/show/{id}/{isConfirm}', 'show')->name('produksi.finishing');
    Route::get('/admin/produksi/finishing/get-partner/{id}', 'getDataPartner');
    Route::put('/admin/produksi/finishing/{id}', 'update');
    Route::post('/admin/produksi/finishing/proccess', 'proccess')->name('produksi.finishing');
    Route::delete('/admin/produksi/finishing/{id}', 'destroy');
});
Route::middleware(['auth'])->controller(ReportProduksiController::class)->group(function () {
    Route::get('/admin/produksi/report', 'index')->name('produksi.report');
    Route::get('/admin/produksi/report/lists', 'lists');
    Route::get('/admin/produksi/report/{id}', 'show')->name('produksi.report');
});
// End Produksi


// Inventory
Route::middleware(['auth'])->controller(StokProdukController::class)->group(function () {
    Route::get('/admin/inventory/stok-produk', 'index')->name('inventory.produkstok');
    Route::get('/admin/inventory/stok-produk/lists', 'lists');
    Route::get('/admin/inventory/stok-produk/{id}', 'show')->name('inventory.produkstok');
    Route::get('/admin/inventory/stok-produk/history/{id}', 'historyProduk')->name('inventory.produkstok');
    Route::get('/admin/inventory/stok-produk/listshistory/{id}', 'lists_history');
    Route::get('/admin/inventory/stok-produk/detail-history/{id}', 'detailHistory')->name('inventory.produkstok');
    Route::get('/admin/inventory/stok-produk/listshistorydetail/{id}', 'lists_history_detail');
});
Route::middleware(['auth'])->controller(StokMaterialController::class)->group(function () {
    Route::get('/admin/inventory/stok-material', 'index')->name('inventory.materialstok');
    Route::get('/admin/inventory/stok-material/lists', 'lists')->name('inventory.materialstok');
    Route::get('/admin/inventory/stok-material/{id}', 'show')->name('inventory.materialstok');
    Route::get('/admin/inventory/stok-material/history/{id}', 'history')->name('inventory.materialstok');
    Route::get('/admin/inventory/stok-material/listshistory/{id}', 'lists_history')->name('inventory.materialstok');
    Route::get('/admin/inventory/stok-material/detail-history/{id}', 'detailHistory')->name('inventory.materialstok');
    Route::get('/admin/inventory/stok-material/listshistorydetail/{id}', 'lists_history_detail');
});
// End Inventory


// Penjualan
Route::middleware(['auth'])->controller(PenjualanController::class)->group(function () {
    Route::get('/admin/penjualan', 'index')->name('penjualan.penjualan');
    Route::get('/admin/penjualan/lists', 'lists');
    Route::get('/admin/penjualan/create', 'create')->name('penjualan.penjualan');
    Route::post('/admin/penjualan', 'store');
    Route::get('/admin/penjualan/{id}', 'edit')->name('penjualan.penjualan');
    Route::get('/admin/penjualan/show/{id}/{isConfirm}', 'show')->name('penjualan.penjualan');;
    Route::get('/admin/penjualan/get-partner/{id}', 'getDataPartner');
    Route::put('/admin/penjualan/{id}', 'update');
    Route::put('/admin/penjualan/confirm/{value}', 'confirm')->name('penjualan.penjualan');;
    Route::get('/admin/penjualan/get-detail-form/{id}', 'getDetailForm');
    Route::get('/admin/penjualan/get-varian/{id}', 'getVarian');
    Route::get('/admin/penjualan/get-stok/{produk_id}/{produk_detail_id}', 'getStok');
    Route::delete('/admin/penjualan/{id}', 'destroy');
});
// End Penjualan

Auth::routes();

